
package com.sertic.applikalo.externalprograms.modules.solgas.soap.gen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ZsdsCorecequipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ZsdsCorecequipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Medidor" type="{urn:sap-com:document:sap:soap:functions:mc-style}char18"/>
 *         &lt;element name="FechaCorte" type="{urn:sap-com:document:sap:soap:functions:mc-style}date10"/>
 *         &lt;element name="FechaReconexion" type="{urn:sap-com:document:sap:soap:functions:mc-style}date10"/>
 *         &lt;element name="FechaNoacceso" type="{urn:sap-com:document:sap:soap:functions:mc-style}date10"/>
 *         &lt;element name="FechaSuspension" type="{urn:sap-com:document:sap:soap:functions:mc-style}date10"/>
 *         &lt;element name="RespAcceso" type="{urn:sap-com:document:sap:soap:functions:mc-style}char20"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZsdsCorecequipo", propOrder = {
    "medidor",
    "fechaCorte",
    "fechaReconexion",
    "fechaNoacceso",
    "fechaSuspension",
    "respAcceso"
})
public class ZsdsCorecequipo {

    @XmlElement(name = "Medidor", required = true)
    protected String medidor;
    @XmlElement(name = "FechaCorte", required = true)
    protected String fechaCorte;
    @XmlElement(name = "FechaReconexion", required = true)
    protected String fechaReconexion;
    @XmlElement(name = "FechaNoacceso", required = true)
    protected String fechaNoacceso;
    @XmlElement(name = "FechaSuspension", required = true)
    protected String fechaSuspension;
    @XmlElement(name = "RespAcceso", required = true)
    protected String respAcceso;

    /**
     * Obtiene el valor de la propiedad medidor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedidor() {
        return medidor;
    }

    /**
     * Define el valor de la propiedad medidor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedidor(String value) {
        this.medidor = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaCorte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaCorte() {
        return fechaCorte;
    }

    /**
     * Define el valor de la propiedad fechaCorte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaCorte(String value) {
        this.fechaCorte = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaReconexion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaReconexion() {
        return fechaReconexion;
    }

    /**
     * Define el valor de la propiedad fechaReconexion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaReconexion(String value) {
        this.fechaReconexion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaNoacceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaNoacceso() {
        return fechaNoacceso;
    }

    /**
     * Define el valor de la propiedad fechaNoacceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaNoacceso(String value) {
        this.fechaNoacceso = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaSuspension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaSuspension() {
        return fechaSuspension;
    }

    /**
     * Define el valor de la propiedad fechaSuspension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaSuspension(String value) {
        this.fechaSuspension = value;
    }

    /**
     * Obtiene el valor de la propiedad respAcceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRespAcceso() {
        return respAcceso;
    }

    /**
     * Define el valor de la propiedad respAcceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRespAcceso(String value) {
        this.respAcceso = value;
    }

}
