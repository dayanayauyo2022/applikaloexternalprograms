
package com.sertic.applikalo.externalprograms.modules.solgas.soap.gen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EtReturn" type="{urn:sap-com:document:sap:soap:functions:mc-style}ZsdttReturnCanalizadoWs"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "etReturn"
})
@XmlRootElement(name = "ZsdfIngresoMedicionesResponse")
public class ZsdfIngresoMedicionesResponse {

    @XmlElement(name = "EtReturn", required = true)
    protected ZsdttReturnCanalizadoWs etReturn;

    /**
     * Obtiene el valor de la propiedad etReturn.
     * 
     * @return
     *     possible object is
     *     {@link ZsdttReturnCanalizadoWs }
     *     
     */
    public ZsdttReturnCanalizadoWs getEtReturn() {
        return etReturn;
    }

    /**
     * Define el valor de la propiedad etReturn.
     * 
     * @param value
     *     allowed object is
     *     {@link ZsdttReturnCanalizadoWs }
     *     
     */
    public void setEtReturn(ZsdttReturnCanalizadoWs value) {
        this.etReturn = value;
    }

}
