package com.seratic.applikalo.externalprograms.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

@Configuration
public class ConfiguracionCron implements SchedulingConfigurer {

    @Value("${pool.connection.crone}")
    private Integer poolSize;

    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(poolSize);
        threadPoolTaskScheduler.setThreadNamePrefix("my-scheduled-task-pool-");
        threadPoolTaskScheduler.initialize();
        threadPoolTaskScheduler.setWaitForTasksToCompleteOnShutdown(false);
        scheduledTaskRegistrar.setTaskScheduler(threadPoolTaskScheduler);
    }
}
