//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.18 a las 03:11:44 PM COT 
//
package com.seratic.applikalo.externalprograms.modules.solgas.soap.gen;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Clase Java para corteyreconexion complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="corteyreconexion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="medidor" type="{http://www.seratic.com/applikalo/externalprograms/modules/solgas/soap/gen}medidor" minOccurs="0"/>
 *         &lt;element name="tipos" type="{http://www.seratic.com/applikalo/externalprograms/modules/solgas/soap/gen}tipos" minOccurs="0"/>
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "corteyreconexion", propOrder = {
    "id",
    "medidor",
    "tipos",
    "fecha"
})
@JsonTypeName(value = "corteyreconexion")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Corteyreconexion {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected Integer id;
    @JsonIgnoreProperties({"caudal", "modelo", "marca", "activomedidor", "presionfabrica", "edificio", "ultimalectura", "ultimoconsumo", "consumo3Meses", "consumopromedio", "interior", "cliente"})
    protected Medidor medidor;
    protected Tipos tipos;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fecha;

    /**
     * Obtiene el valor de la propiedad id.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad medidor.
     *
     * @return possible object is {@link Medidor }
     *
     */
    public Medidor getMedidor() {
        return medidor;
    }

    /**
     * Define el valor de la propiedad medidor.
     *
     * @param value allowed object is {@link Medidor }
     *
     */
    public void setMedidor(Medidor value) {
        this.medidor = value;
    }

    /**
     * Obtiene el valor de la propiedad tipos.
     *
     * @return possible object is {@link Tipos }
     *
     */
    public Tipos getTipos() {
        return tipos;
    }

    /**
     * Define el valor de la propiedad tipos.
     *
     * @param value allowed object is {@link Tipos }
     *
     */
    public void setTipos(Tipos value) {
        this.tipos = value;
    }

    /**
     * Obtiene el valor de la propiedad fecha.
     *
     * @return possible object is {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getFecha() {
        return fecha;
    }

    /**
     * Define el valor de la propiedad fecha.
     *
     * @param value allowed object is {@link XMLGregorianCalendar }
     *
     */
    public void setFecha(XMLGregorianCalendar value) {
        this.fecha = value;
    }

}
