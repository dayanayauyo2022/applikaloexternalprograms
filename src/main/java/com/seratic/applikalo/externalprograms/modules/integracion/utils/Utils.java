package com.seratic.applikalo.externalprograms.modules.integracion.utils;

import com.fasterxml.jackson.databind.JsonNode;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.text.Collator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.seratic.applikalo.externalprograms.modules.integracion.Constants.*;

public class Utils {

    /*obtener ambiente a partir de una url del apiread*/
    public static final String getAmbiente(String urlApiRead) {
        try {
            if (urlApiRead.equals(URL_API_READ_DESA)) {
                return AMBIENTE_DESA;
            } else if (urlApiRead.equals(URL_API_READ_ED)) {
                return AMBIENTE_ED;
            } else if (urlApiRead.equals(URL_API_READ_SERATIC)) {
                return AMBIENTE_SERATIC;
            } else if (urlApiRead.equals(URL_API_READ_RELEASE)) {
                return AMBIENTE_RELEASE;
            } else if (urlApiRead.equals(URL_API_READ_TDP)) {
                return AMBIENTE_TDP;
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    public static final boolean verificarElementoEnListado(String elemento, List<String> listado) {
        boolean existe = false;
        for (String itemLista : listado) {
            if (itemLista.equals(elemento)) {
                existe = true;
            }
        }
        return existe;
    }

    /*quitar primer elemento a la ruta de un campo, ejemplo: cliente.usuario.nombres ->> usuario.nombres*/
    public static final String quitarPrimerElementoCampoRuta(String campoRuta) {
        try {

            String[] campoArray = campoRuta.split("\\.");
            String campoProcess = "";
            if (campoArray.length > 1) {
                for (int i = 1; i < campoArray.length; i++) {
                    if (i < campoArray.length - 1) {
                        campoProcess += campoArray[i] + ".";
                    } else {
                        campoProcess += campoArray[i];
                    }
                }
            } else {
                campoProcess = campoRuta;
            }
            return campoProcess;
        } catch (Exception e) {
            return campoRuta;
        }
    }

    /*Método para extraer el valor del campo correo de una estructura json recibiendo como parámetro la ruta del campo*/
    public static final JsonNode extraerCampoObjetos(JsonNode objetoBase, String rutaDeCampos, String campoSinRuta, boolean isRelacion) {
        try {
            String[] campoObjetoArray = rutaDeCampos.split("\\.");
            // solo enviar arreglo de la ruta de las tablas sin el campo
            String rutaTablas = "";
            Integer length = campoObjetoArray.length;
            if(!isRelacion){
                length--;
            }
            for (int i = 0; i < length; i++) {
                if (i == 0) {
                    rutaTablas = campoObjetoArray[i];
                } else {
                    rutaTablas += "." + campoObjetoArray[i];
                }
            }
            JsonNode resp = getValorJerarquiaObjetos(objetoBase, rutaTablas, campoSinRuta);
            return resp;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Método para obtener el valor de un campo en una estructura JSON
     * @param camposObjeto estructura JSON con campos del objeto base y sus relaciones
     * @param rutaCampos espera la ruta de tablas sin el campo del objeto al final de la ruta. Ejemplo: usuario.perfil
     * @param campoRetornar campo que se desea obtener de la estructura json. Ejemplo: nombre
     * @return
     */
    public static final JsonNode getValorJerarquiaObjetos(JsonNode camposObjeto, String rutaCampos, String campoRetornar) {
        try {
            String[] objetos = rutaCampos.split("\\.");
            JsonNode tablaRel = null;
            JsonNode campoResult = null;
            if (objetos.length == 1) {
                campoResult = camposObjeto.get(campoRetornar);
            } else {
                // iniciar en segunda posicion porque el primer elemento es el objeto base:
                for (int i = 1; i < objetos.length; i++) {
                    if (tablaRel != null) {
                        if (tablaRel.isObject()) {
                            tablaRel = tablaRel.get(objetos[i]);
                        } else if (tablaRel.isArray()) {
                            tablaRel = tablaRel.get(0).get(objetos[i]);
                        }
                    } else {
                        tablaRel = camposObjeto.get(objetos[i]);
                    }
                }
                if (tablaRel != null) {
                    if (tablaRel.isObject()) {
                        campoResult = tablaRel.get(campoRetornar);
                    } else if (tablaRel.isArray()) {
                        campoResult = tablaRel.get(0).get(campoRetornar);
                    }
                }
            }
            return campoResult;
        } catch (Exception e) {
            throw e;
        }
    }

    public static final String ejecutarScript(String script) {
        try {
            ScriptEngineManager manager = new ScriptEngineManager();
            ScriptEngine engine = manager.getEngineByName("JavaScript");
            Object expresion =  engine.eval(script);
            if (expresion != null) {
                return String.valueOf(expresion);
            } else {
                return "";
            }
        } catch (Exception e) {
            return "";
        }
    }

    public static boolean isNumeric(String cadena){
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe){
            return false;
        }
    }

    public static String getUrlWrite(String prefijo) {
        if (prefijo != null && !prefijo.isEmpty()) {
            if (prefijo.toUpperCase().trim().equals("DESA")) {
                return URL_API_WRITE_DESA;
            } else if (prefijo.toUpperCase().trim().equals("ED")) {
                return URL_API_WRITE_ED;
            } else if (prefijo.toUpperCase().trim().equals("DB")) {
                return URL_API_WRITE_TDP;
            } else if (prefijo.toUpperCase().trim().equals("REBETA")) {
                return URL_API_WRITE_BETA;
            } else{
                return "";
            }
        } else {
            return "";
        }
    }

    public static String getUrlRead(String prefijo) {
        if (prefijo != null && !prefijo.isEmpty()) {
            if (prefijo.toUpperCase().trim().equals("DESA")) {
                return URL_API_READ_DESA;
            } else if (prefijo.toUpperCase().trim().equals("ED")) {
                return URL_API_READ_ED;
            } else if (prefijo.toUpperCase().trim().equals("DB")) {
                return URL_API_READ_TDP;
            } else if (prefijo.toUpperCase().trim().equals("REBETA")) {
                return URL_API_READ_BETA;
            } else{
                return "";
            }
        } else {
            return "";
        }
    }

    public static String getUrlEmaReportes(String prefijo) {
        if (prefijo != null && !prefijo.isEmpty()) {
            if (prefijo.toUpperCase().trim().equals("DESA")) {
                return URL_API_EMAREPORTES_DESA;
            } else if (prefijo.toUpperCase().trim().equals("ED")) {
                return URL_API_EMAREPORTES_ED;
            } else if (prefijo.toUpperCase().trim().equals("DB")) {
                return URL_API_EMAREPORTES_TDP;
            } else if (prefijo.toUpperCase().trim().equals("REBETA")) {
                return URL_API_EMAREPORTES_BETA;
            } else{
                return "";
            }
        } else {
            return "";
        }
    }

    public static String urlBaseProgExternos(String prefijo) {
        if (prefijo.equals("rebeta")) {
            return "http://ema2progextrbeta.latinapps.co";
        } else {
            return "http://ema2progextbeta.latinapps.co";
        }
    }

    /*Método para verificar si dos cadenas son iguales sin tener en cuenta mayusculas, ni tildes. Retorna true si son iguales*/
    public static final boolean compareInsenstiveCad(String cad1, String cad2) {
        try {
            Collator insenstiveStringComparator = Collator.getInstance();
            insenstiveStringComparator.setStrength(Collator.PRIMARY);
            if (insenstiveStringComparator.compare(cad1, cad2) == 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /*generar llave primaria de objetos en mongo*/
    public static final String generarPkObjetoCronMongo(String key, String prefijo, Integer version) {
        try {
            return key + "|" + prefijo + "|" + version;
        } catch (Exception e) {
            return "";
        }
    }

    /*Método para verificar si un correo es valido retornando true o invalido retornando false*/
    public static final boolean isEmailValid(String email) {
        // Patrón para validar el email
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(email);
        if (mather.find() == true) {
            return true;
        } else {
            return false;
        }
    }
}
