package com.seratic.applikalo.externalprograms.modules.amauta.utils;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Esta clase es una utilidad que perimete crear los campos y filtros
 * para objetos de la tabla estudiante_curso en amauta
 *
 * @author Mario Jurado
 * @version 1.0
 * @since 2021-03-16
 */

public final class EstudianteCursoUtils {
    
    /**
     * Metodo estatico para retornar los filtros de un estudiante curso 
     * 
     * @param idEstudiante entero para identificar el estudiante por el que se filtra
     * el estudiante curso
     * @return List<JsonNode>, lista de filtros.
   */
    public static List<JsonNode> crearFiltrosEstudianteCurso(Integer idEstudiante){
        List<JsonNode> filtros = new ArrayList<>();
        filtros.add(CrearBodyUtils.obtenerFiltro("id", "=", "AND", "estudiante", idEstudiante.toString()));
        filtros.add(CrearBodyUtils.obtenerFiltro("activo", "=", "AND", "estudiante_curso", "'true'"));
        return filtros;
    }

    /**
     * Metodo estatico para retornar los campos de un estudiante curso 
     * 
     * @return List<JsonNode>, lista de filtros.
   */
    public static List<JsonNode> crearCamposEstudianteCurso(){
        List<JsonNode> campos = new ArrayList<>();
        campos.add(CrearBodyUtils.obtenerCampo("estudiante.id"));
        campos.add(CrearBodyUtils.obtenerCampo("avancetotal"));
        campos.add(CrearBodyUtils.obtenerCampo("activo"));
        campos.add(CrearBodyUtils.obtenerCampo("periodo.id"));
        return campos;
    }

    /**
     * Metodo estatico para retornar los filtros de los posibles estudiantes cursos 
     * que pueden ser usados para asignarse al feedback
     * 
     * @param fechaModificacion parametro por el cual se crea un filtro para validar 
     * registros mayores a esta fecha
     * @param avances lista de strings por el cual se filtran los avanestotal en un between
     * @param idsEstidiatesExcluir lista de strings con los ids que se deben excluir en el filtro
     * @param categorias string con las categorias que se van a filtrar 
     * @param grado grado al que pertenece el estudiante
     * @param periodo periodo al que pertenece el estudiante
     * @return List<JsonNode>, lista de filtros.
   */
    public static List<JsonNode> crearFiltrosEstudianteCursoAE(String fechaModificacion, List<String> avances, List<String> idsEstidiatesExcluir, String categorias, String grado, String periodo){
        List<JsonNode> filtros = new ArrayList<>();
        filtros.add(CrearBodyUtils.obtenerFiltro("fecha_modificacion", ">=", "AND", "estudiante_curso", "'" + fechaModificacion + "'"));
        filtros.add(CrearBodyUtils.obtenerFiltro("avancetotal", "BETWEEN", "AND", "estudiante_curso", CrearBodyUtils.obtnerCondicionBetween(avances)));
        filtros.add(CrearBodyUtils.obtenerFiltro("activo", "=", "AND", "estudiante_curso", "'true'"));
        filtros.add(CrearBodyUtils.obtenerFiltro("id", "NOT IN", "AND", "estudiante", CrearBodyUtils.obtnerCondicionIn(idsEstidiatesExcluir)));
        filtros.add(CrearBodyUtils.obtenerFiltro("categoria", "IN", "AND", "estudiante", categorias));
        filtros.add(CrearBodyUtils.obtenerFiltro("grado", "ILIKE", "AND", "estudiante", "'" + grado + "'"));
        filtros.add(CrearBodyUtils.obtenerFiltro("id", "=", "AND", "periodo", periodo));
        return filtros;
    }

    /**
     * Metodo estatico para retornar los campos de los posibles estudiantes cursos 
     * que pueden ser usados para asignarse al feedback
     * 
     * @return List<JsonNode>, lista de filtros.
   */
    public static List<JsonNode> crearCamposEstudianteCursoAE(){
        List<JsonNode> campos = new ArrayList<>();
        campos.add(CrearBodyUtils.obtenerCampo("estudiante.id"));
        campos.add(CrearBodyUtils.obtenerCampo("avancetotal"));
        campos.add(CrearBodyUtils.obtenerCampo("activo"));
        campos.add(CrearBodyUtils.obtenerCampo("fecha_creacion"));
        campos.add(CrearBodyUtils.obtenerCampo("fecha_modificacion"));
        campos.add(CrearBodyUtils.obtenerCampo("estudiante.categoria"));
        campos.add(CrearBodyUtils.obtenerCampo("estudiante.grado"));
        campos.add(CrearBodyUtils.obtenerCampo("curso.id"));
        campos.add(CrearBodyUtils.obtenerCampo("periodo.id"));
        return campos;
    }
}
