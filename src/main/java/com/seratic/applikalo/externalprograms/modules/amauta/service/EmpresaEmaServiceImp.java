package com.seratic.applikalo.externalprograms.modules.amauta.service;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class EmpresaEmaServiceImp implements EmpresaEmaService{

    private static final Log LOG = LogFactory.getLog(EmpresaEmaServiceImp.class);
    @Value("${amauta.enviocorreo-recordatorio.ema-data.url}")
    private String urlEmaData;
    @Value("${amauta.enviocorreo-recordatorio.empresa-id}")
    private Integer idempresa;
    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public JsonNode obtenerEmpresaById() {
        JsonNode retorno;
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        //headers.add("Authorization", "ed_717");
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity request = new HttpEntity(null, headers);
        JsonNode respose = restTemplate.exchange(urlEmaData + "empresa/findempresasbyid/" + idempresa, HttpMethod.GET, request, JsonNode.class).getBody();
        if (respose.size() > 0){
            return retorno = respose.get(0);
        } else {
            LOG.error("  EmpresaEmaServiceImp  ->  obtenerEmpresaById Fallo");
            return retorno = null;
        }
    }
}
