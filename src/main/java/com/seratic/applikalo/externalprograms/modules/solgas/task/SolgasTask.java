/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.modules.solgas.task;

import com.seratic.applikalo.externalprograms.modules.solgas.service.SolgasService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class SolgasTask {

    private static final Logger LOG = LoggerFactory.getLogger(SolgasTask.class);
    @Value("${solgas.programacion-automatica.schema}")
    private String schema;
    @Value("${solgas.programacion-automatica.id-publico}")
    private String idPublico;
    @Value("${solgas.programacion-automatica.idVersionApp}")
    private String versionApplicacion;
    private String urlApiWrite;
    @Value("${solgas.programacion-automatica.urlReader}")
    private String urlApiRead;
    @Value(value = "${solgas.insertSolgasSOAP.idEstsdoMeidorInsert}")
    private String idEstadoMedidor;

    @Autowired
    private SolgasService solgasService;

    @Scheduled(cron = "${solgas.programacion-automatica.cron}")
    public void programacionAutomatica() {
        try {
            solgasService.programacionAutomatica();
        } catch (Exception e) {
            LOG.info("====ERROR INESPERADO CRON PROGRAMACIONAUTOMATICA===== posible causa: ", e);
        }
    }

    @Scheduled(cron = "${solgas.insertSolgasSOAP.cron}")
    public void insertSolgasSOAP() {
        try {
            solgasService.insertMediciones(schema, idPublico, versionApplicacion, urlApiWrite, urlApiRead, Integer.valueOf(idEstadoMedidor));
        } catch (Exception e) {
            LOG.error("  SolgasTask  ->  insertSolgasSOAP  Fallo: ", e);
        }
    }
}
