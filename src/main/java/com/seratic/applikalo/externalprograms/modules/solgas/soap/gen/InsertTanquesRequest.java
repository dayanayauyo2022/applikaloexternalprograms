//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.18 a las 03:11:44 PM COT 
//
package com.seratic.applikalo.externalprograms.modules.solgas.soap.gen;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tanques" type="{http://www.seratic.com/applikalo/externalprograms/modules/solgas/soap/gen}tanque" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "token",
    "tanques"
})
@XmlRootElement(name = "insertTanquesRequest")
public class InsertTanquesRequest {

    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected List<Tanque> tanques;

    /**
     * Obtiene el valor de la propiedad token.
     *
     * @return possible object is {@link String }
     *
     */
    public String getToken() {
        return token;
    }

    /**
     * Define el valor de la propiedad token.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the tanques property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the tanques property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTanques().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Tanque }
     *
     *
     */
    public List<Tanque> getTanques() {
        if (tanques == null) {
            tanques = new ArrayList<Tanque>();
        }
        return this.tanques;
    }

}
