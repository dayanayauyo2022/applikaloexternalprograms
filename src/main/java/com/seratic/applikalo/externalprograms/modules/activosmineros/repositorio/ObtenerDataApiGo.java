package com.seratic.applikalo.externalprograms.modules.activosmineros.repositorio;

import com.fasterxml.jackson.databind.JsonNode;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.apiGo.Filtro;

public interface ObtenerDataApiGo {

    JsonNode getObjetosApiCamposPaginacion(String url, Filtro filtros);
}
