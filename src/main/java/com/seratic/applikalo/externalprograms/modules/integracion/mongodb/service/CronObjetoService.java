package com.seratic.applikalo.externalprograms.modules.integracion.mongodb.service;

import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.entity.CronObjeto;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.repository.CronObjetoRepository;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.integracion.utils.Utils.generarPkObjetoCronMongo;

@Service
public class CronObjetoService {
    @Autowired
    CronObjetoRepository cronObjetoRepository;

    public CronObjeto saveUpdateCronObjeto(CronObjeto cronObjeto) {
        return cronObjetoRepository.save(cronObjeto);
    }

    public boolean deleteCronObjeto(CronObjeto cronObjeto) {
        try {
            cronObjetoRepository.delete(cronObjeto);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public CronObjeto getCronObjetoById(String id) {
        return cronObjetoRepository.findById(id).orElse(null);
    }

    public List<CronObjeto> getAllCronObjeto() {
        return cronObjetoRepository.findAll();
    }

    public boolean findAndSaveCronObjeto(String key, DataToken dataToken, String idPublicoOwner, boolean mantenerSinTimeZone, String gmt) {
        try {
            String idCronObjeto = generarPkObjetoCronMongo(key, dataToken.getPrefijo(), dataToken.getVersionAplicacion());
            CronObjeto cronObjeto = getCronObjetoById(idCronObjeto);
            if (cronObjeto == null) {
                cronObjeto = new CronObjeto();
                cronObjeto.setId(idCronObjeto);
                cronObjeto.setIdPublicoUsuario(dataToken.getIdPublico());
                cronObjeto.setIdPublicoOwner(idPublicoOwner);
                cronObjeto.setPrefijo(dataToken.getPrefijo());
                cronObjeto.setVersion(dataToken.getVersionAplicacion());
                cronObjeto.setMantenerSinTimeZone(mantenerSinTimeZone);
                cronObjeto.setGmt(gmt);
                cronObjetoRepository.save(cronObjeto);
            } else {
                // actualizar campos en caso de que exista:
                cronObjeto.setIdPublicoUsuario(dataToken.getIdPublico());
                cronObjeto.setIdPublicoOwner(idPublicoOwner);
                cronObjeto.setPrefijo(dataToken.getPrefijo());
                cronObjeto.setVersion(dataToken.getVersionAplicacion());
                cronObjeto.setMantenerSinTimeZone(mantenerSinTimeZone);
                cronObjeto.setGmt(gmt);
                cronObjetoRepository.save(cronObjeto);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
