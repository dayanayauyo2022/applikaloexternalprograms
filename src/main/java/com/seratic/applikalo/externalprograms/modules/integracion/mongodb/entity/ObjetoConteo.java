package com.seratic.applikalo.externalprograms.modules.integracion.mongodb.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@Document(collection = "objeto_conteo")
public class ObjetoConteo implements Serializable {
    private static final long serialVersionUID = 2330289880871451994L;
    @Id
    private String id;
    private String idObjetoApi;
    private String nombre;
    private String prefijo;
    private Integer version;
    private List<String> contadores;
    private String urlApiWrite;
    private String urlApiRead;
    private String idPublicoUsuario;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public List<String> getContadores() {
        return contadores;
    }

    public void setContadores(List<String> contadores) {
        this.contadores = contadores;
    }

    public String getUrlApiWrite() {
        return urlApiWrite;
    }

    public void setUrlApiWrite(String urlApiWrite) {
        this.urlApiWrite = urlApiWrite;
    }

    public String getUrlApiRead() {
        return urlApiRead;
    }

    public void setUrlApiRead(String urlApiRead) {
        this.urlApiRead = urlApiRead;
    }

    public String getIdObjetoApi() {
        return idObjetoApi;
    }

    public void setIdObjetoApi(String idObjetoApi) {
        this.idObjetoApi = idObjetoApi;
    }

    public String getIdPublicoUsuario() {
        return idPublicoUsuario;
    }

    public void setIdPublicoUsuario(String idPublicoUsuario) {
        this.idPublicoUsuario = idPublicoUsuario;
    }
}
