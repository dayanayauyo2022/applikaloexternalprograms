package com.seratic.applikalo.externalprograms.modules.calculoriesgos.utils;

import java.util.ArrayList;
import java.util.List;

public class ConstantsRiesgos {

    public static final String TABLA_CHEQUEO_SINTOMAS = "chequeo_sintomas";
    public static final String NODO_RIESGOS = "chequeo_sintomas";

    /*campos de la tabla chequeo_sintomas*/
    public static final List<String> getListaCamposRiesgos() {
        List<String> listaCamposRiegos = new ArrayList<>();
        listaCamposRiegos.add("temperatura");
        listaCamposRiegos.add("sintomas");
        listaCamposRiegos.add("condicionescomorbilidad");
        listaCamposRiegos.add("rta_3");
        listaCamposRiegos.add("rta_5");
        listaCamposRiegos.add("rta_7");
        listaCamposRiegos.add("rta_9");
        return listaCamposRiegos;
    }

    public static final List<String> getSintomasRiesgoMedio() {
        List<String> lstSintomasRM = new ArrayList<>();
        lstSintomasRM.add("Diarrea");
        lstSintomasRM.add("Náuseas/Vómitos");
        lstSintomasRM.add("Irritabilidad y Confusión");
        lstSintomasRM.add("Dolor abdominal");
        lstSintomasRM.add("Otros");
        return lstSintomasRM;
    }

    public static final List<String> getSintomasRiesgoAlto() {
        List<String> lstSintomasRA = new ArrayList<>();
        lstSintomasRA.add("Tos");
        lstSintomasRA.add("Dolor garganta");
        lstSintomasRA.add("Congestión nasal");
        lstSintomasRA.add("Dificultad respiratoria");
        lstSintomasRA.add("Fiebre/Escalofrío");
        lstSintomasRA.add("Malestar general");
        lstSintomasRA.add("Cefalea");
        lstSintomasRA.add("Dolor muscular");
        lstSintomasRA.add("Dolar pecho");
        lstSintomasRA.add("Dolor articulaciones");
        return lstSintomasRA;
    }

    public static final List<String> getCondComorbilidadRA() {
        List<String> lstComorbilidad = new ArrayList<>();
        lstComorbilidad.add("Embarazo");
        lstComorbilidad.add("Enfermedad cardiovascular");
        lstComorbilidad.add("Diabetes");
        lstComorbilidad.add("Enfermedad hepática");
        lstComorbilidad.add("Enfermedad crónica neurológica o neuromuscular");
        lstComorbilidad.add("Post parto");
        lstComorbilidad.add("Inmunodeficiencia VIH");
        lstComorbilidad.add("Enfermedad renal");
        lstComorbilidad.add("Daño hepático");
        lstComorbilidad.add("Enfermedad pulmonar crónica");
        lstComorbilidad.add("Otros");
        return lstComorbilidad;
    }



}
