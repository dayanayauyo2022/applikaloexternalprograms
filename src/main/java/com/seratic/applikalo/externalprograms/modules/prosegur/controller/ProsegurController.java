/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.modules.prosegur.controller;

import com.seratic.applikalo.externalprograms.modules.prosegur.service.ProsegurService;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.AsignarSimKitePlatformRequestDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.ValidarSimBajaRequestDTO;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author ivang
 */
@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.POST})
@RequestMapping(value = "/prosegur")
public class ProsegurController {

    private static final Logger logger = LoggerFactory.getLogger(ProsegurController.class);
    @Autowired
    private ProsegurService prosegurService;

    @PostMapping(value = "/asignarSimKitePlatform", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> asignarSimKitePlatform(@RequestBody AsignarSimKitePlatformRequestDTO requestBody,
            @RequestAttribute("prefijo") String prefijo,
            @RequestAttribute("versionAplicacion") Integer versionAplicacion,
            @RequestAttribute("idPublico") String idPublico,
            @RequestAttribute("urlApiWrite") String urlApiWrite,
            @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            DataToken dataToken = new DataToken(prefijo, idPublico, versionAplicacion, urlApiWrite, urlApiRead);
            return new ResponseEntity<>(prosegurService.installSIM(requestBody.getObjeto(), dataToken), headers, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("asignarSimKitePlatform fallo", e);
            return new ResponseEntity<>(e, headers, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/validarSimBaja", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> validarSimBaja(@RequestBody ValidarSimBajaRequestDTO requestBody,
            @RequestAttribute("prefijo") String prefijo,
            @RequestAttribute("versionAplicacion") Integer versionAplicacion,
            @RequestAttribute("idPublico") String idPublico,
            @RequestAttribute("urlApiWrite") String urlApiWrite,
            @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            DataToken dataToken = new DataToken(prefijo, idPublico, versionAplicacion, urlApiWrite, urlApiRead);
            return new ResponseEntity<>(prosegurService.validateDownSIM(requestBody.getObjeto(), dataToken), headers, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("validarSimBaja fallo", e);
            return new ResponseEntity<>(e, headers, HttpStatus.BAD_REQUEST);
        }
    }
}
