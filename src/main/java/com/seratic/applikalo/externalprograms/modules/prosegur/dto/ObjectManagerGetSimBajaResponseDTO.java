/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.modules.prosegur.dto;

import java.util.List;

/**
 *
 * @author ivang
 */
public class ObjectManagerGetSimBajaResponseDTO {

    private boolean success;
    private List<SimBajaDownDTO> data;
    private int total;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<SimBajaDownDTO> getData() {
        return data;
    }

    public void setData(List<SimBajaDownDTO> data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
