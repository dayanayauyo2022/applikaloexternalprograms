package com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.VOs;

public class LocationVo {
    double radio;
    PointVo locationOne;
    PointVo locationTwo;
    String fieldToUpdate;
    
    public LocationVo() {
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    
    public PointVo getLocationOne() {
        return locationOne;
    }

    public void setLocationOne(PointVo locationOne) {
        this.locationOne = locationOne;
    }

    public PointVo getLocationTwo() {
        return locationTwo;
    }

    public void setLocationTwo(PointVo locationTwo) {
        this.locationTwo = locationTwo;
    }

    public String getFieldToUpdate() {
        return fieldToUpdate;
    }

    public void setFieldToUpdate(String fieldToUpdate) {
        this.fieldToUpdate = fieldToUpdate;
    }

    
}
