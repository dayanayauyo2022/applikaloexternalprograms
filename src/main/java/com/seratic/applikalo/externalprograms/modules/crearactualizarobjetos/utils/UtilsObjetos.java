package com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.VOs.LocationVo;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.utils.ConstantsObjetos.*;
import static com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.utils.FuncionesUtils.extraerVariables;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.FechasUtils.*;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.Utils.*;

public class UtilsObjetos {

    public static final String armarPeticionCrearObjeto(JsonNode camposObjeto, String nombreObjeto) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<JsonNode> listaObjetos = new ArrayList<>();
            JsonNode objetoNew = mapper.createObjectNode();
            ((ObjectNode) objetoNew).put(nombreObjeto, camposObjeto);
            listaObjetos.add(objetoNew);

            JsonNode objToSave = mapper.createObjectNode();
            ((ObjectNode) objToSave).putArray("objetos").addAll(listaObjetos);
            return objToSave.toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static final String armarPeticionActualizarObjeto(JsonNode camposObjeto, String nombreObjeto) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objToUpdate = mapper.createObjectNode();
            ((ObjectNode) objToUpdate).put(nombreObjeto, camposObjeto);
            return objToUpdate.toString();
        } catch (Exception e) {
            return "";
        }
    }

    /*Método para crear body de peticion getObjeto en el api go*/
    public static final String armarPeticionGetObjetoWfApi(String nombreObjeto, String idObjeto, String objetoRelacion, List<String> listaCampos, boolean isObjectWf, int page, int start, int pageSize) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();
            JsonNode filtro = mapper.createObjectNode();
            ((ObjectNode) filtro).put("campo", "id");
            ((ObjectNode) filtro).put("condicional", "=");
            ((ObjectNode) filtro).put("objeto", (objetoRelacion == null || objetoRelacion.isEmpty()) ? nombreObjeto : objetoRelacion);
            ((ObjectNode) filtro).put("parametro", idObjeto);
            filtros.add(filtro);

            List<JsonNode> camposOrdenArray = new ArrayList<>();
            JsonNode campoOrdenFecha = mapper.createObjectNode();
            ((ObjectNode) campoOrdenFecha).put("campo", "fecha_creacion");
            ((ObjectNode) campoOrdenFecha).put("ascendente", false);
            camposOrdenArray.add(campoOrdenFecha);

            ((ObjectNode) objetoPeticion).putArray("orden").addAll(camposOrdenArray);
            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", page);
            ((ObjectNode) objetoPeticion).put("start", start);
            ((ObjectNode) objetoPeticion).put("pagesize", pageSize);
            ((ObjectNode) objetoPeticion).put("objeto", isObjectWf ? "wf_" + nombreObjeto : nombreObjeto);

            List<JsonNode> campos = new ArrayList<>();

            for (String campoValor : listaCampos) {
                JsonNode nodoCampo = mapper.createObjectNode();
                ((ObjectNode) nodoCampo).put("campo", campoValor);
                campos.add(nodoCampo);
            }

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);

            return objetoPeticion.toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static final List<String> buscarCamposWf(JsonNode listaAtribsCampos, String nomObjetoBase) {
        List<String> listaCamposWf = new ArrayList<>();
        try {
            for (JsonNode atribCampo : listaAtribsCampos) {
                if (atribCampo.get("tipoConfiguracion") != null && !atribCampo.get("tipoConfiguracion").isNull()
                        && (atribCampo.get("tipoConfiguracion").get("id").asText().equals(TIPO_CONFIG_WF)
                        || atribCampo.get("tipoConfiguracion").get("id").asText().equals(TIPO_CONFIG_FUNCION))) {
                    // quitar primer elemento de la ruta del campo:
                    if (atribCampo.get("tipoConfiguracion").get("id").asText().equals(TIPO_CONFIG_WF)) {
                        String campoProcess = quitarPrimerElementoCampoRuta(atribCampo.get("valorConfigurar").asText());
                        if (!verificarElementoEnListado(campoProcess, listaCamposWf)) {
                            if (atribCampo.get("tipoCampo").get("id").asText().equals(TIPO_CAMPO_RELACION)) {
                                listaCamposWf.add(campoProcess + ".id");
                            } else {
                                listaCamposWf.add(campoProcess);
                            }
                        }
                    } // extraer campos wf del script
                    else {
                        if (atribCampo.get("valorFuncionJs") != null && !atribCampo.get("valorFuncionJs").asText().isEmpty()) {
                            String[] variablesLst = extraerVariables(atribCampo.get("valorFuncionJs").asText());
                            if (variablesLst != null && variablesLst.length > 0) {
                                for (int i = 0; i < variablesLst.length; i++) {
                                    if (variablesLst[i].startsWith("wf_" + nomObjetoBase)) {
                                        String campoProcesScript = quitarPrimerElementoCampoRuta(variablesLst[i]);
                                        if (!verificarElementoEnListado(campoProcesScript, listaCamposWf)) {
                                            listaCamposWf.add(campoProcesScript);
                                        }
                                    }

                                }

                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        return listaCamposWf;
    }

    public static final JsonNode obtenerValorCampoWf(JsonNode listaObjetoWf, String campoRuta, boolean isRelacion) {
        JsonNode valorRetornar = null;
        try {
            String campoProcess = quitarPrimerElementoCampoRuta(campoRuta);
            String[] campoArray = campoProcess.split("\\.");
            boolean encontrado = false;
            for (int i = 0; i < listaObjetoWf.size() && !encontrado; i++) {
                if (campoArray.length == 1) {
                    if (listaObjetoWf.get(i).get(campoArray[0]) != null && !listaObjetoWf.get(i).get(campoArray[0]).isNull()) {
                        valorRetornar = listaObjetoWf.get(i).get(campoArray[0]);
                        encontrado = true;
                    }
                } else {
                    String nomObjetoRel = campoArray[0];
                    if (listaObjetoWf.get(i).get(nomObjetoRel) != null && !listaObjetoWf.get(i).get(nomObjetoRel).isNull() && !listaObjetoWf.get(i).get(nomObjetoRel).isEmpty()) {
                        JsonNode objetoRel = listaObjetoWf.get(i).get(nomObjetoRel);
                        String campoBuscar;
                        if (isRelacion) {
                            campoBuscar = "id";
                        } else {
                            campoBuscar = campoArray[campoArray.length - 1];
                        }
                        valorRetornar = extraerCampoObjetos(objetoRel, campoProcess, campoBuscar, isRelacion);
                        encontrado = true;
                    }
                }
            }
        } catch (Exception e) {
        }
        return valorRetornar;
    }

    public static final String obtenerValorEstatico(String tipoCampo, String valorConfigurar, boolean mantenerSinTimeZone) {
        if (tipoCampo.equals(TIPO_CAMPO_FECHA) && valorConfigurar.trim().toUpperCase().equals(VALOR_FECHA_ACTUAL)) {
            return formatDateConSinTimeZoneSave(new Date(), mantenerSinTimeZone);
        } else if (tipoCampo.equals(TIPO_CAMPO_FECHA)
                && (isFechaColaborador(valorConfigurar, "2020-01-01") || isFechaColaborador(valorConfigurar, "2020-01-02"))) {
            // si es 2020-01-01 sumar 8 dias
            if (isFechaColaborador(valorConfigurar, "2020-01-01")) {
                Date fechaParamDate = new Date();
                Date fechaParamNew = sumarDiasAFecha(fechaParamDate, 8);
                String fechaResult = formatDateConSinTimeZoneSave(fechaParamNew, mantenerSinTimeZone);
                return fechaResult;
            } // si es 020-01-02 retornar vacio para limpiar el campo habilitadohasta
            else {
                return "";
            }
        } else {
            return valorConfigurar;
        }
    }

    public static final boolean isFechaColaborador(String fechaParam, String fechaReferencia) {
        try {
            Date fechaParamDate = getFechaByCadenaApiGenerico(fechaParam);
            Date fechaRefeDate = getFechaByCadenaSinHora(fechaReferencia);

            if (fechaParamDate.getYear() == fechaRefeDate.getYear()
                    && fechaParamDate.getMonth() == fechaRefeDate.getMonth()
                    && fechaParamDate.getDay() == fechaRefeDate.getDay()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static final String getDiferenciaEstados(String estadoInicial, String estadoFinal, JsonNode listaObjetoWf) {
        String diferencia = "";
        try {
            Date fechaInicio = null;
            Date fechaFin = null;
            boolean isFechaIni = false;
            for (int i = 0; i < listaObjetoWf.size() && !isFechaIni; i++) {
                if (listaObjetoWf.get(i).get("nodo").asText().trim().equals(estadoInicial.trim())) {
                    fechaInicio = getFechaByCadenaApi(listaObjetoWf.get(i).get("fecha_creacion").asText());
                    isFechaIni = true;
                }
            }
            boolean isFechaFin = false;
            for (int i = 0; i < listaObjetoWf.size() && !isFechaFin; i++) {
                if (listaObjetoWf.get(i).get("nodo").asText().trim().equals(estadoFinal.trim())) {
                    fechaFin = getFechaByCadenaApi(listaObjetoWf.get(i).get("fecha_creacion").asText());
                    isFechaFin = true;
                }
            }
            if (fechaInicio != null && fechaFin != null) {
                diferencia = getTiempoTranscurrido(fechaInicio, fechaFin);
            }
        } catch (Exception e) {
        }
        return diferencia;
    }

    public static final String generarKeyInsertObjeto(Integer versionApp) {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        String timesTampCadena = timestamp.toString().replaceAll(" ", "");
        return versionApp + "_" + timesTampCadena;
    }

    public static final String buildBodyDistanceRequest(LocationVo location) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode objectRequest = mapper.createObjectNode();

        JsonNode pointOne = mapper.createObjectNode();
        ((ObjectNode) pointOne).put("lat", location.getLocationOne().getLat());
        ((ObjectNode) pointOne).put("lng", location.getLocationOne().getLng());

        JsonNode pointTwo = mapper.createObjectNode();
        ((ObjectNode) pointTwo).put("lat", location.getLocationTwo().getLat());
        ((ObjectNode) pointTwo).put("lng", location.getLocationTwo().getLng());

        ((ObjectNode) objectRequest).set("pointOne", pointOne);
        ((ObjectNode) objectRequest).set("pointTwo", pointTwo);
        return objectRequest.toString();
    }
}
