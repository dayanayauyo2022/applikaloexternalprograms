package com.seratic.applikalo.externalprograms.modules.integracion.VOs.apiGo;

public class Where {
    String objeto;
    String campo;
    String condicional;
    String parametro;

    public Where() {
    }

    public Where(String objeto, String campo, String condicional, String parametro) {
        this.objeto = objeto;
        this.campo = campo;
        this.condicional = condicional;
        this.parametro = parametro;
    }

    public String getObjeto() {
        return objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getCondicional() {
        return condicional;
    }

    public void setCondicional(String condicional) {
        this.condicional = condicional;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }
}
