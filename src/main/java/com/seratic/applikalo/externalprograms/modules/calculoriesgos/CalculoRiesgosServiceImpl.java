package com.seratic.applikalo.externalprograms.modules.calculoriesgos;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.seratic.applikalo.externalprograms.modules.integracion.IntegracionRepository;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.RespuestaVO;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.calculoriesgos.utils.CalculoRiesgosUtils.*;
import static com.seratic.applikalo.externalprograms.modules.calculoriesgos.utils.ConstantsRiesgos.*;
import static com.seratic.applikalo.externalprograms.modules.security.utils.TokenManagerUtil.generarTokenGetObjeto;

@Service
public class CalculoRiesgosServiceImpl implements CalculoRiesgosService {
    private static final Logger logger = LoggerFactory.getLogger(CalculoRiesgosServiceImpl.class);

    @Autowired
    private IntegracionRepository integracionRepository;

    @Override
    public RespuestaVO calcularRiesgo(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
            JsonNode objetoBase = objetoRequestJson.get("objeto");
            String nombreObjeto = objetoRequestJson.get("nombreObjeto").asText();
            String idObjeto = objetoBase.get("id").asText();

            String bodyGetObjeto = armarObjRiesgosGetObjetoApi(nombreObjeto, idObjeto);
            DataToken dataTokenApiRead = new DataToken(prefijo, idPublico, versionApp, urlWrite, urlRead);
            String tokenEncriptApiRead = generarTokenGetObjeto(dataTokenApiRead);
            ResponseEntity<String> responseGetObjeto = integracionRepository.getObjetoApi(dataTokenApiRead, tokenEncriptApiRead, bodyGetObjeto);
            JsonNode nodoRespGetObjeto = mapper.readTree(responseGetObjeto.getBody());
            if (nodoRespGetObjeto.get("statusCode") != null && !nodoRespGetObjeto.get("statusCode").asText().equals("200")) {
                errList.addError("412", "Tabla wf", "No fue posible obtner nodo chequeo_sintomas: " + nombreObjeto);
                throw errList;
            }
            JsonNode listaNodosWf = nodoRespGetObjeto.get("data");
            if (listaNodosWf != null && listaNodosWf.size() > 0) {
                int llegoAChequeoSintoma = -1;
                for (int i = 0; i < listaNodosWf.size() && llegoAChequeoSintoma == -1; i++) {
                    if (listaNodosWf.get(i).get("nodo").asText().contains(NODO_RIESGOS)) {
                        llegoAChequeoSintoma = i;
                    }
                }
                if (llegoAChequeoSintoma != -1) {
                    JsonNode nodoWfChequeoSintomas = listaNodosWf.get(llegoAChequeoSintoma);
                    if (nodoWfChequeoSintomas.get(TABLA_CHEQUEO_SINTOMAS) != null && !nodoWfChequeoSintomas.get(TABLA_CHEQUEO_SINTOMAS).isNull()) {
                        JsonNode objChequeoSintomas = nodoWfChequeoSintomas.get(TABLA_CHEQUEO_SINTOMAS);
                        String valorRiesgo = getNivelRiesgo(objChequeoSintomas);
                        String bodyUpdateObject = armarObjRiesgoUpdateApiWrite(nombreObjeto, objetoBase, valorRiesgo);
                        ResponseEntity<String> respUpdateObject = integracionRepository.updateApi(dataTokenApiRead, bodyUpdateObject, true);
                        JsonNode nodoRespUpdate = mapper.readTree(respUpdateObject.getBody());
                        if (nodoRespUpdate.get("statusCode") != null && !nodoRespUpdate.get("statusCode").asText().equals("200")) {
                            errList.addError("412", "UPDATE API WRITE", "No fue posible actualizar contador de objeto: " + nombreObjeto);
                            throw errList;
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.info("error calculo riesgos: " + errList.getErrors().get(0).getMessage());
            errList.addError("412", "ERROR CALCULO RIESGOS", "No fue posible realizar calculo riesgos");
        }
        if (errList.getErrors().size() > 0) {
            logger.info("error calculo riesgos: " + errList.getErrors().get(0).getMessage());
            throw errList;
        } else {
            logger.info("****EXITO. CALCULO RIESGOS ***");
            RespuestaVO response = new RespuestaVO();
            response.setCode("200");
            response.setMessage("success");
            return response;
        }
    }

    public String getNivelRiesgo(JsonNode objChequeo) {
        String nivelRiesgo = "Bajo";
        try {
            Integer temperatura = -1;
            if (objChequeo.get("temperatura") != null && !objChequeo.get("temperatura").isNull()) {
                temperatura = objChequeo.get("temperatura").asInt();
            }
            List<String> sintomasLst = new ArrayList<>();
            if (objChequeo.get("sintomas") != null && !objChequeo.get("sintomas").isNull()) {
                for (JsonNode node : objChequeo.get("sintomas")) {
                    sintomasLst.add(node.asText());
                }
            }
            List<String> comorbilidadLst = new ArrayList<>();
            if (objChequeo.get("condicionescomorbilidad") != null && !objChequeo.get("condicionescomorbilidad").isNull()) {
                for (JsonNode node : objChequeo.get("condicionescomorbilidad")) {
                    comorbilidadLst.add(node.asText());
                }
            }
            String rta_3 = "no";
            if (objChequeo.get("rta_3") != null && !objChequeo.get("rta_3").isNull()) {
                for (JsonNode node : objChequeo.get("rta_3")) {
                    rta_3 = node.asText();
                }
            }
            String rta_5 = "no";
            if (objChequeo.get("rta_5") != null && !objChequeo.get("rta_5").isNull()) {
                for (JsonNode node : objChequeo.get("rta_5")) {
                    rta_5 = node.asText();
                }
            }
            String rta_7 = "no";
            if (objChequeo.get("rta_7") != null && !objChequeo.get("rta_7").isNull()) {
                for (JsonNode node : objChequeo.get("rta_7")) {
                    rta_7 = node.asText();
                }
            }
            String rta_9 = "no";
            if (objChequeo.get("rta_9") != null && !objChequeo.get("rta_9").isNull()) {
                for (JsonNode node : objChequeo.get("rta_9")) {
                    rta_9 = node.asText();
                }
            }
            // evaluar riesgo alto
            if ((temperatura >= 38 && verificarValorEnListado(sintomasLst, getSintomasRiesgoAlto())) ||
                    verificarValorEnListado(comorbilidadLst, getCondComorbilidadRA()) ||
                    rta_3.toUpperCase().equals("SI") || rta_5.toUpperCase().equals("SI") ||
                    rta_7.toUpperCase().equals("SI") || rta_9.toUpperCase().equals("SI")
            ) {
                nivelRiesgo = "Alto";
            }
            // evaluar riesgo medio
            else if (temperatura >= 38 && verificarValorEnListado(sintomasLst, getSintomasRiesgoMedio())) {
                nivelRiesgo = "Medio";
            }
        } catch (Exception e) {
        }
        return nivelRiesgo;
    }
}
