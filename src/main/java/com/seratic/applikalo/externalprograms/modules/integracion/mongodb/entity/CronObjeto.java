package com.seratic.applikalo.externalprograms.modules.integracion.mongodb.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.io.Serializable;

@Document(collection = "cron_objeto")
public class CronObjeto implements Serializable {
    private static final long serialVersionUID = 2330289880871451911L;

    @Id
    private String id;
    private String prefijo;
    private Integer version;
    private String idPublicoUsuario;
    private String idPublicoOwner;
    private boolean mantenerSinTimeZone;
    private String gmt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getIdPublicoUsuario() {
        return idPublicoUsuario;
    }

    public void setIdPublicoUsuario(String idPublicoUsuario) {
        this.idPublicoUsuario = idPublicoUsuario;
    }

    public String getIdPublicoOwner() {
        return idPublicoOwner;
    }

    public void setIdPublicoOwner(String idPublicoOwner) {
        this.idPublicoOwner = idPublicoOwner;
    }

    public boolean isMantenerSinTimeZone() {
        return mantenerSinTimeZone;
    }

    public void setMantenerSinTimeZone(boolean mantenerSinTimeZone) {
        this.mantenerSinTimeZone = mantenerSinTimeZone;
    }

    public String getGmt() {
        return gmt;
    }

    public void setGmt(String gmt) {
        this.gmt = gmt;
    }
}
