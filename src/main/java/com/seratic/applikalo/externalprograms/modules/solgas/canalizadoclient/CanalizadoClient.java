package com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class CanalizadoClient extends WebServiceGatewaySupport {

    public ZsdfIngresoMedicionesResponse zsdfIngresoMediciones(ZsdfIngresoMediciones ingresoMediciones) {
        return (ZsdfIngresoMedicionesResponse) getWebServiceTemplate().marshalSendAndReceive(ingresoMediciones);
    }

    public ZsdfCorteReconexionEquipoResponse zsdfCorteReconexionEquipo(ZsdfCorteReconexionEquipo corteReconexionEquipo) {
        return (ZsdfCorteReconexionEquipoResponse) getWebServiceTemplate().marshalSendAndReceive(corteReconexionEquipo);
    }
}
