package com.seratic.applikalo.externalprograms.modules.solgas.soap;

import com.seratic.applikalo.externalprograms.modules.security.TokenManager;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import com.seratic.applikalo.externalprograms.modules.solgas.soap.gen.Corteyreconexion;
import com.seratic.applikalo.externalprograms.modules.solgas.soap.gen.Edificio;
import com.seratic.applikalo.externalprograms.modules.solgas.soap.gen.InsertCorteReconexionesRequest;
import com.seratic.applikalo.externalprograms.modules.solgas.soap.gen.InsertCorteReconexionesResponse;
import com.seratic.applikalo.externalprograms.modules.solgas.soap.gen.InsertEdificiosRequest;
import com.seratic.applikalo.externalprograms.modules.solgas.soap.gen.InsertEdificiosResponse;
import com.seratic.applikalo.externalprograms.modules.solgas.soap.gen.InsertMedidoresRequest;
import com.seratic.applikalo.externalprograms.modules.solgas.soap.gen.InsertMedidoresResponse;
import com.seratic.applikalo.externalprograms.modules.solgas.soap.gen.InsertTanquesRequest;
import com.seratic.applikalo.externalprograms.modules.solgas.soap.gen.InsertTanquesResponse;
import com.seratic.applikalo.externalprograms.modules.solgas.soap.gen.Medidor;
import com.seratic.applikalo.externalprograms.modules.solgas.soap.gen.Tanque;
import com.seratic.applikalo.externalprograms.objectmanager.dto.ObjectManagerInsertRequest;
import com.seratic.applikalo.externalprograms.objectmanager.repository.ObjectManagerRepository;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class SolgasSoapEndpoint {

    private static final String NAMESPACE_URI = "http://www.seratic.com/applikalo/externalprograms/modules/solgas/soap/gen";
    private static final Logger logger = LoggerFactory.getLogger(SolgasSoapEndpoint.class);

    @Autowired
    private ObjectManagerRepository objectManagerRepository;

    @Value(value = "${solgas.programacion-automatica.urlReader}")
    private String urlAPIRead;
    @Value(value = "${solgas.programacion-automatica.urlWriter}")
    private String urlAPIWrite;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "insertEdificiosRequest")
    @ResponsePayload
    public InsertEdificiosResponse insertEdificios(@RequestPayload InsertEdificiosRequest request) throws Exception {
        InsertEdificiosResponse retorno = null;
        try {
            DataToken dataToken = TokenManager.getDataSourceInfo(request.getToken());
            dataToken.setUrlApiRead(urlAPIRead);
            dataToken.setUrlApiWrite(urlAPIWrite);
            ObjectManagerInsertRequest body = new ObjectManagerInsertRequest(request.getEdificios());
            List<Edificio> response = objectManagerRepository.insert(dataToken, body, false, Edificio.class);
            if (response != null) {
                retorno = new InsertEdificiosResponse();
                retorno.getEdificios().addAll(response);
            }
        } catch (Exception ex) {
            logger.error("insertEdificios fallo", ex);
            throw ex;
        }
        return retorno;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "insertTanquesRequest")
    @ResponsePayload
    public InsertTanquesResponse insertTanques(@RequestPayload InsertTanquesRequest request) throws Exception {
        InsertTanquesResponse retorno = null;
        try {
            DataToken dataToken = TokenManager.getDataSourceInfo(request.getToken());
            dataToken.setUrlApiRead(urlAPIRead);
            dataToken.setUrlApiWrite(urlAPIWrite);
            ObjectManagerInsertRequest body = new ObjectManagerInsertRequest(request.getTanques());
            List<Tanque> response = objectManagerRepository.insert(dataToken, body, false, Tanque.class);
            if (response != null) {
                retorno = new InsertTanquesResponse();
                retorno.getTanques().addAll(response);
            }
        } catch (Exception ex) {
            logger.error("insertTanques fallo", ex);
            throw ex;
        }
        return retorno;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "insertMedidoresRequest")
    @ResponsePayload
    public InsertMedidoresResponse insertMedidores(@RequestPayload InsertMedidoresRequest request) throws Exception {
        InsertMedidoresResponse retorno = null;
        try {
            DataToken dataToken = TokenManager.getDataSourceInfo(request.getToken());
            dataToken.setUrlApiRead(urlAPIRead);
            dataToken.setUrlApiWrite(urlAPIWrite);
            ObjectManagerInsertRequest body = new ObjectManagerInsertRequest(request.getMedidores());
            List<Medidor> response = objectManagerRepository.insert(dataToken, body, false, Medidor.class);
            if (response != null) {
                retorno = new InsertMedidoresResponse();
                retorno.getMedidores().addAll(response);
            }
        } catch (Exception ex) {
            logger.error("insertMedidores fallo", ex);
            throw ex;
        }
        return retorno;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "insertCorteReconexionesRequest")
    @ResponsePayload
    public InsertCorteReconexionesResponse insertCorteReconexiones(@RequestPayload InsertCorteReconexionesRequest request) throws Exception {
        InsertCorteReconexionesResponse retorno = null;
        try {
            DataToken dataToken = TokenManager.getDataSourceInfo(request.getToken());
            dataToken.setUrlApiRead(urlAPIRead);
            dataToken.setUrlApiWrite(urlAPIWrite);
            ObjectManagerInsertRequest body = new ObjectManagerInsertRequest(request.getCorteyreconexiones());
            List<Corteyreconexion> response = objectManagerRepository.insert(dataToken, body, false, Corteyreconexion.class);
            if (response != null) {
                retorno = new InsertCorteReconexionesResponse();
                retorno.getCorteyreconexiones().addAll(response);
            }
        } catch (Exception ex) {
            logger.error("insertCorteReconexiones fallo", ex);
            throw ex;
        }
        return retorno;
    }
}
