package com.seratic.applikalo.externalprograms.modules.entities;

public class Accion {
    private Integer id;
    private Componente componenteSiguiente;
    private TipoAccion tipoAccion;
    private String servicio;
    private String mensajeExitoso;
    private String mensajeErrado;
    private Boolean enabled;
    private String atributos;

    public Accion() {
        // default implementation ignored
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Componente getComponenteSiguiente() {
        return componenteSiguiente;
    }

    public void setComponenteSiguiente(Componente componenteSiguiente) {
        this.componenteSiguiente = componenteSiguiente;
    }

    public TipoAccion getTipoAccion() {
        return tipoAccion;
    }

    public void setTipoAccion(TipoAccion tipoAccion) {
        this.tipoAccion = tipoAccion;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getMensajeExitoso() {
        return mensajeExitoso;
    }

    public void setMensajeExitoso(String mensajeExitoso) {
        this.mensajeExitoso = mensajeExitoso;
    }

    public String getMensajeErrado() {
        return mensajeErrado;
    }

    public void setMensajeErrado(String mensajeErrado) {
        this.mensajeErrado = mensajeErrado;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getAtributos() {
        return atributos;
    }

    public void setAtributos(String atributos) {
        this.atributos = atributos;
    }
}
