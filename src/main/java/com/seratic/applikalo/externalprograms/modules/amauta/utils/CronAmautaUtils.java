package com.seratic.applikalo.externalprograms.modules.amauta.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.amauta.ConstantsAmauta.BODY_CRON_NOTIFI_CONFE;

public class CronAmautaUtils {

    private static final String COMENTARIO = "Tarea aprobada";
    public static final String AMBIENTE_DESA = "DESA";
    private static final String APRODADO = "Aprobado";
    private static final Integer TIPO_NODO_100 = 100;
    private static final Integer ID_ESTADO_FEEDFACK_APROBADO = 2;

    public static final JsonNode getBodyFeedEstudianteLstCron(String fechaInicio, String fechaFin, String nombreObjeto, boolean adminFiltro, boolean estadoFeedFiltro) {
        try {

            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();
            JsonNode filtro = mapper.createObjectNode();
            ((ObjectNode) filtro).put("campo", "fecha_creacion");
            ((ObjectNode) filtro).put("condicional", "BETWEEN");
            ((ObjectNode) filtro).put("objeto", nombreObjeto);
            ((ObjectNode) filtro).put("parametro", "'" + fechaInicio + "' AND '" + fechaFin + "'");
            filtros.add(filtro);

            if(adminFiltro){
                JsonNode filtroAdmin = mapper.createObjectNode();
                ((ObjectNode) filtroAdmin).put("campo", "id");
                ((ObjectNode) filtroAdmin).put("condicional", ">");
                ((ObjectNode) filtroAdmin).put("objeto", "usuario");
                ((ObjectNode) filtroAdmin).put("parametro", "0");
                filtros.add(filtroAdmin);
            }

            if(estadoFeedFiltro){
                JsonNode filtroAdmin = mapper.createObjectNode();
                ((ObjectNode) filtroAdmin).put("campo", "id");
                ((ObjectNode) filtroAdmin).put("condicional", "=");
                ((ObjectNode) filtroAdmin).put("objeto", "estado_feedback_estudiante");
                ((ObjectNode) filtroAdmin).put("parametro", 1);
                filtros.add(filtroAdmin);
            }

            List<JsonNode> camposOrdenArray = new ArrayList<>();
            JsonNode campoOrdenFecha = mapper.createObjectNode();
            ((ObjectNode) campoOrdenFecha).put("campo", "id");
            ((ObjectNode) campoOrdenFecha).put("ascendente", true);
            camposOrdenArray.add(campoOrdenFecha);

            ((ObjectNode) objetoPeticion).putArray("orden").addAll(camposOrdenArray);
            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 100000);
            ((ObjectNode) objetoPeticion).put("objeto", nombreObjeto);

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campoFechaCreacion = mapper.createObjectNode();
            ((ObjectNode) campoFechaCreacion).put("campo", "fecha_creacion");
            campos.add(campoFechaCreacion);

            JsonNode campoEstudLeccion = mapper.createObjectNode();
            ((ObjectNode) campoEstudLeccion).put("campo", "estudiante_leccion.id");
            campos.add(campoEstudLeccion);

            JsonNode campoEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoEstudiante).put("campo", "estudiante.id");
            campos.add(campoEstudiante);

            JsonNode campoAdmin = mapper.createObjectNode();
            ((ObjectNode) campoAdmin).put("campo", "usuario.id");
            campos.add(campoAdmin);

            JsonNode campoEstado = mapper.createObjectNode();
            ((ObjectNode) campoEstado).put("campo", "estado_feedback_estudiante.nombre");
            campos.add(campoEstado);

            JsonNode campoEvaluacionEscrita = mapper.createObjectNode();
            ((ObjectNode) campoEvaluacionEscrita).put("campo", "evaluacion_escrita.id");
            campos.add(campoEvaluacionEscrita);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion;
        } catch (Exception e) {
            throw e;
        }
    }

    public static final String getBodyInsertFeddbackEstudCron(String idEstudianteLeccion, String idEvaluacionEsrita, JsonNode admin) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<JsonNode> listaObjetos = new ArrayList<>();

            JsonNode objEstudianteLeccion = mapper.createObjectNode();
            ((ObjectNode) objEstudianteLeccion).put("id", idEstudianteLeccion);

            JsonNode objUsuario = mapper.createObjectNode();
            ((ObjectNode) objUsuario).put("id", admin.get("id").asText());

            JsonNode objEvaluacionEscrita = mapper.createObjectNode();
            ((ObjectNode) objEvaluacionEscrita).put("id", idEvaluacionEsrita);

            JsonNode camposObj = mapper.createObjectNode();
            ((ObjectNode) camposObj).set("estudiante_leccion", objEstudianteLeccion);
            ((ObjectNode) camposObj).set("usuario", objUsuario);
            ((ObjectNode) camposObj).set("evaluacion_escrita", objEvaluacionEscrita);
            ((ObjectNode) camposObj).put("comentariofeed", "");

            JsonNode objInsert = mapper.createObjectNode();
            ((ObjectNode) objInsert).put("feedback_estudiante", camposObj);
            listaObjetos.add(objInsert);


            JsonNode objToSave = mapper.createObjectNode();
            ((ObjectNode) objToSave).putArray("objetos").addAll(listaObjetos);
            return objToSave.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error insertregisterpost generando body insert feedback_estudiante", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static final JsonNode getEstudLeccionNotifiCron(String nombreObjeto, String idLeccion) {
        try {

            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            List<JsonNode> filtros = new ArrayList<>();
            JsonNode filtro2 = mapper.createObjectNode();
            ((ObjectNode) filtro2).put("campo", "activo");
            ((ObjectNode) filtro2).put("condicional", "LIKE");
            ((ObjectNode) filtro2).put("objeto", nombreObjeto);
            ((ObjectNode) filtro2).put("parametro", "'true'");
            filtros.add(filtro2);

            JsonNode filtro3 = mapper.createObjectNode();
            ((ObjectNode) filtro3).put("campo", "id");
            ((ObjectNode) filtro3).put("condicional", "=");
            ((ObjectNode) filtro3).put("objeto", nombreObjeto + ".leccion");
            ((ObjectNode) filtro3).put("parametro", idLeccion);
            filtros.add(filtro3);

            List<JsonNode> camposOrdenArray = new ArrayList<>();
            JsonNode campoOrdenFecha = mapper.createObjectNode();
            ((ObjectNode) campoOrdenFecha).put("campo", "id");
            ((ObjectNode) campoOrdenFecha).put("ascendente", true);
            camposOrdenArray.add(campoOrdenFecha);

            ((ObjectNode) objetoPeticion).putArray("orden").addAll(camposOrdenArray);
            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 100000);
            ((ObjectNode) objetoPeticion).put("objeto", nombreObjeto);

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campoActivo = mapper.createObjectNode();
            ((ObjectNode) campoActivo).put("campo", "activo");
            campos.add(campoActivo);

            JsonNode campoFechaLeccion = mapper.createObjectNode();
            ((ObjectNode) campoFechaLeccion).put("campo", "leccion.fecha");
            campos.add(campoFechaLeccion);

            JsonNode campoNombreLeccion = mapper.createObjectNode();
            ((ObjectNode) campoNombreLeccion).put("campo", "leccion.nombre");
            campos.add(campoNombreLeccion);

            JsonNode campoLinkConfeLeccion = mapper.createObjectNode();
            ((ObjectNode) campoLinkConfeLeccion).put("campo", "leccion.link_conferencia");
            campos.add(campoLinkConfeLeccion);

            JsonNode campoCorreo = mapper.createObjectNode();
            ((ObjectNode) campoCorreo).put("campo", "estudiante.usuario.correo");
            campos.add(campoCorreo);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion;
        } catch (Exception e) {
            throw e;
        }
    }

    public static final JsonNode getBodyLeccionesConfeCron(String fechaInicio, String fechaFin, String nombreObjeto, String nomTipoConferencia) {
        try {

            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();
            JsonNode filtro = mapper.createObjectNode();
            ((ObjectNode) filtro).put("campo", "fecha");
            ((ObjectNode) filtro).put("condicional", "BETWEEN");
            ((ObjectNode) filtro).put("objeto", nombreObjeto);
            ((ObjectNode) filtro).put("parametro", "'" + fechaInicio + "' AND '" + fechaFin + "'");
            filtros.add(filtro);

            JsonNode filtro3 = mapper.createObjectNode();
            ((ObjectNode) filtro3).put("campo", "nombre");
            ((ObjectNode) filtro3).put("condicional", "ILIKE");
            ((ObjectNode) filtro3).put("objeto", nombreObjeto + ".tipo_leccion");
            ((ObjectNode) filtro3).put("parametro", "'" + nomTipoConferencia + "'");
            filtros.add(filtro3);

            List<JsonNode> camposOrdenArray = new ArrayList<>();
            JsonNode campoOrdenFecha = mapper.createObjectNode();
            ((ObjectNode) campoOrdenFecha).put("campo", "id");
            ((ObjectNode) campoOrdenFecha).put("ascendente", true);
            camposOrdenArray.add(campoOrdenFecha);

            ((ObjectNode) objetoPeticion).putArray("orden").addAll(camposOrdenArray);
            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 100000);
            ((ObjectNode) objetoPeticion).put("objeto", nombreObjeto);

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campoNomTipoLecc = mapper.createObjectNode();
            ((ObjectNode) campoNomTipoLecc).put("campo", "tipo_leccion.nombre");
            campos.add(campoNomTipoLecc);

            JsonNode campoFechaLeccion = mapper.createObjectNode();
            ((ObjectNode) campoFechaLeccion).put("campo", "fecha");
            campos.add(campoFechaLeccion);

            JsonNode campoNombreLeccion = mapper.createObjectNode();
            ((ObjectNode) campoNombreLeccion).put("campo", "nombre");
            campos.add(campoNombreLeccion);

            JsonNode campoLinkConfeLeccion = mapper.createObjectNode();
            ((ObjectNode) campoLinkConfeLeccion).put("campo", "link_conferencia");
            campos.add(campoLinkConfeLeccion);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion;
        } catch (Exception e) {
            throw e;
        }
    }

    public static final String getBodyNotificarCorreo(List<String> destinatarios, String asunto, JsonNode camposObjeto, String nombreObjeto, Integer versionApp) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode destinatariosJson = mapper.valueToTree(destinatarios);
            JsonNode objetoPeticion = mapper.createObjectNode();
            ((ObjectNode) objetoPeticion).putArray("destinatarios").addAll(destinatariosJson);
            ((ObjectNode) objetoPeticion).put("asunto", asunto);
            ((ObjectNode) objetoPeticion).put("cuerpo", BODY_CRON_NOTIFI_CONFE);
            ((ObjectNode) objetoPeticion).set("camposObjeto", camposObjeto);
            ((ObjectNode) objetoPeticion).put("nombreObjeto", nombreObjeto);
            ((ObjectNode) objetoPeticion).put("versionApp", versionApp);
            return objetoPeticion.toString();
        } catch (Exception e) {
            throw e;
        }
    }

    public static final JsonNode getBodyFeedEstudAdimExist(String idEstudianteLeccion, String idUsuarioAdmin, String nombreObjeto) {
        try {

            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();
            JsonNode filtro = mapper.createObjectNode();
            ((ObjectNode) filtro).put("campo", "id");
            ((ObjectNode) filtro).put("condicional", "=");
            ((ObjectNode) filtro).put("conjuncion", "AND");
            ((ObjectNode) filtro).put("objeto", nombreObjeto + ".estudiante_leccion");
            ((ObjectNode) filtro).put("parametro", idEstudianteLeccion);
            filtros.add(filtro);

            JsonNode filtro2 = mapper.createObjectNode();
            ((ObjectNode) filtro2).put("campo", "id");
            ((ObjectNode) filtro2).put("condicional", "=");
            ((ObjectNode) filtro2).put("conjuncion", "AND");
            ((ObjectNode) filtro2).put("objeto", nombreObjeto + ".usuario");
            ((ObjectNode) filtro2).put("parametro", idUsuarioAdmin);
            filtros.add(filtro2);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1);
            ((ObjectNode) objetoPeticion).put("objeto", nombreObjeto);

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campoFechaCreacion = mapper.createObjectNode();
            ((ObjectNode) campoFechaCreacion).put("campo", "fecha_creacion");
            campos.add(campoFechaCreacion);

            JsonNode campoEstudLeccion = mapper.createObjectNode();
            ((ObjectNode) campoEstudLeccion).put("campo", "estudiante_leccion.id");
            campos.add(campoEstudLeccion);

            JsonNode campoEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoEstudiante).put("campo", "estudiante.id");
            campos.add(campoEstudiante);

            JsonNode campoAdmin = mapper.createObjectNode();
            ((ObjectNode) campoAdmin).put("campo", "usuario.id");
            campos.add(campoAdmin);

            JsonNode campoEstado = mapper.createObjectNode();
            ((ObjectNode) campoEstado).put("campo", "estado_feedback_estudiante.nombre");
            campos.add(campoEstado);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Este metodo genera el json del body para insertar el formulario de feedback 
     * del admin 
     * @param versionApp, valor para generar el key del formulario
     * @param feedbackEstudiantes, valor del feedback estudiante al cual se va asociar el registro nuevo
     * @param idUsuario, valor de id del usuario el cual inserta el feedback
     * @param username, valor para el username del usario
     * @return body del objeto a insertar formulario feedback1
    */
    public static final JsonNode getBodyInsertFeddbackFormAprobed(Integer versionApp, Integer feedbackEstudiante,Integer idUsuario, String username, String prefijo) {
        MyErrorListException errList = new MyErrorListException();
        try {
            Date fecha = new Date();

            ObjectMapper mapper = new ObjectMapper();
            List<JsonNode> listaObjetos = new ArrayList<>();

            JsonNode objFeedbackEstudiante = mapper.createObjectNode();
            ((ObjectNode) objFeedbackEstudiante).put("id", feedbackEstudiante);

            JsonNode objUsuario = mapper.createObjectNode();
            ((ObjectNode) objUsuario).put("username", username);

            JsonNode camposObj = mapper.createObjectNode();
            ((ObjectNode) camposObj).set("feedback_estudiante", objFeedbackEstudiante);
            ((ObjectNode) camposObj).set("usuario", objUsuario);
            
            ((ObjectNode) camposObj).put("key", versionApp + "|" + idUsuario + "|" + fecha.getTime());
            JsonNode objInsert = mapper.createObjectNode();

            if (prefijo.toUpperCase().equals(AMBIENTE_DESA)) {
                ((ObjectNode) camposObj).put("pregunta1", COMENTARIO);
                ((ObjectNode) objInsert).set("feedback1", camposObj);   
            } else {
                ((ObjectNode) camposObj).put("pregunta_1", COMENTARIO);
                ((ObjectNode) objInsert).set("refeedback", camposObj);
            }


            listaObjetos.add(objInsert);

            JsonNode objToSave = mapper.createObjectNode();
            ((ObjectNode) objToSave).putArray("objetos").addAll(listaObjetos);
            return objToSave;
        } catch (Exception e) {
            errList.addError("15.1", "Error generando body insert formulario feedback aprobado", "ex: " + e.getMessage());
            throw errList;
        }
    }

    /**
     * Este metodo genera el json del body para insertar el formulario wf_feedback_estudiante 
     * de estudiante de forma automatica  
     * @param idFeedbackEstudiantes, valor del feedback estudiante al cual se va asociar el registro nuevo
     * @param idUsuario, valor de id del usuario el cual inserta el feedback
     * @param username, valor para el username del usario
     * @param fechaActual, fecha actual para insertar en el registro
     * @return body del objeto a insertar wf_feedback_estudiante
    */
    public static final JsonNode getBodyInsertWFFeddbackEstudAprobed(Integer idFeedbackEstudiante,Integer idUsuario, String username, String fechaActual) {
        MyErrorListException errList = new MyErrorListException();
        try {

            Date fecha = new Date();

            ObjectMapper mapper = new ObjectMapper();
            List<JsonNode> listaObjetos = new ArrayList<>();

            JsonNode objFeedbackEstudiante = mapper.createObjectNode();
            ((ObjectNode) objFeedbackEstudiante).put("id", idFeedbackEstudiante);

            JsonNode objEstadoFeedbackEstudiante = mapper.createObjectNode();
            ((ObjectNode) objEstadoFeedbackEstudiante).put("nombre", APRODADO);

            JsonNode camposObj = mapper.createObjectNode();
            ((ObjectNode) camposObj).set("feedback_estudiante", objFeedbackEstudiante);
            ((ObjectNode) camposObj).set("estado_feedback_estudiante", objEstadoFeedbackEstudiante);
            ((ObjectNode) camposObj).put("nodo", APRODADO);
            ((ObjectNode) camposObj).put("tipo_nodo", TIPO_NODO_100);
            ((ObjectNode) camposObj).put("usuario", username);
            ((ObjectNode) camposObj).put("fecha_hora", fechaActual);
            ((ObjectNode) camposObj).put("pk_flujo_trabajo", idFeedbackEstudiante + "|" + username + "|" + fecha.getTime());
            ((ObjectNode) camposObj).put("tiempo_estado", "00:00:00.0");


            JsonNode objInsert = mapper.createObjectNode();
            ((ObjectNode) objInsert).set("wf_feedback_estudiante", camposObj);
            listaObjetos.add(objInsert);


            JsonNode objToSave = mapper.createObjectNode();
            ((ObjectNode) objToSave).putArray("objetos").addAll(listaObjetos);
            return objToSave;
        } catch (Exception e) {
            errList.addError("15.2", "Error generando body insert WF_feedback_estudiante", "ex: " + e.getMessage());
            throw errList;
        }
    }

    /**
     * Este metodo genera el json del body para actualizar el estado del feedback_estudiante 
     * de un estudiante de forma automatica  
     * @param idFeedbackEstudiantes, valor del feedback estudiante al cual se va asociar el registro nuevo
     * @param fechaActual, valor para generar el key del formulario
     * @return body del objeto a actualizar feedback_estudiante
    */
    public static final JsonNode getBodyUpdateStateFeedbackStudent(Integer idFeedBackEstudiante, String fechaActual) {
        MyErrorListException errList = new MyErrorListException();
        try {

            ObjectMapper mapper = new ObjectMapper();
            
            JsonNode objEstadoFeedBackEstudiante = mapper.createObjectNode();
            ((ObjectNode) objEstadoFeedBackEstudiante).put("id", ID_ESTADO_FEEDFACK_APROBADO);

            JsonNode objFeedbackEstudiante = mapper.createObjectNode();
            ((ObjectNode) objFeedbackEstudiante).put("id", idFeedBackEstudiante);
            ((ObjectNode) objFeedbackEstudiante).put("fecha_estado", fechaActual);
            ((ObjectNode) objFeedbackEstudiante).set("estado_feedback_estudiante", objEstadoFeedBackEstudiante);

            JsonNode objInsert = mapper.createObjectNode();
            ((ObjectNode) objInsert).set("feedback_estudiante", objFeedbackEstudiante);

            return objInsert;
        } catch (Exception e) {
            errList.addError("15.3", "Error  generando body Update feedback_estudiante", "ex: " + e.getMessage());
            throw errList;
        }
    }
}
