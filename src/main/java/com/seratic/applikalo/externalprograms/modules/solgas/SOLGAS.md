## /solgas/calcularPromedio

Servicio POST. Programa externo para el calculo de promedio de consumo de los ultimos meses a partir de la fecha actual. 
Se consultan las ultimas 13 lecturas de medidor, ordenadas descendentemente por fecha de creación, se toma el registro inmediatamente anterior 
al actual y se procede a restar la lectura actual con el dato mencionado anteriormente, de esta manera se tiene el consumo actual. 
Para el calculo del promedio se usa la siguiente formula (primera - ultima)/(numero - 1)