package com.seratic.applikalo.externalprograms.modules.amauta;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.amauta.VOs.AsignarLeccionesResponse;
import com.seratic.applikalo.externalprograms.modules.amauta.VOs.EstudianteCursoVO;
import com.seratic.applikalo.externalprograms.modules.amauta.VOs.LeccionVO;
import com.seratic.applikalo.externalprograms.modules.amauta.utils.EstudianteCursoUtils;
import com.seratic.applikalo.externalprograms.modules.integracion.IntegracionRepository;
import com.seratic.applikalo.externalprograms.modules.integracion.IntegrationService;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.RespuestaVO;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.entity.CronObjeto;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.service.CronObjetoService;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.service.ProgExternoErrorService;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.amauta.ConstantsAmauta.*;
import static com.seratic.applikalo.externalprograms.modules.amauta.utils.AmautaUtils.*;
import static com.seratic.applikalo.externalprograms.modules.amauta.utils.AsignarEvaluacionUtils.*;
import static com.seratic.applikalo.externalprograms.modules.amauta.utils.AvanceFeedBackUtils.*;
import static com.seratic.applikalo.externalprograms.modules.amauta.utils.AvanceLeccionUtils.*;
import static com.seratic.applikalo.externalprograms.modules.amauta.utils.CronAmautaUtils.*;
import static com.seratic.applikalo.externalprograms.modules.conteoactividades.utils.ConteoActividadesUtils.generarPkObjetoMongo;
import static com.seratic.applikalo.externalprograms.modules.integracion.Constants.*;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.FechasUtils.*;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.NumberUtils.formatearDecimales;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.Utils.*;
import static com.seratic.applikalo.externalprograms.modules.security.utils.TokenManagerUtil.generarTokenGetObjeto;

@Service
public class AmautaServiceImpl implements AmautaService {
    private static final Logger logger = LoggerFactory.getLogger(AmautaServiceImpl.class);

    @Autowired
    IntegrationService integrationService;

    @Autowired
    ProgExternoErrorService progExternoErrorService;

    @Autowired
    CronObjetoService cronObjetoService;

    @Autowired
    IntegracionRepository integracionRepository;

    @Override
    public AsignarLeccionesResponse asignarLecciones(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        String nombreEstadoConfirmado = "Confirmado";
        try {
            if (prefijo != null && !prefijo.isEmpty() && versionApp != null && idPublico != null && !idPublico.isEmpty()) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
                JsonNode objetoBase = objetoRequestJson.get("objeto");
                String idEstudianteCurso = objetoBase.get("id").asText();
                String nombreObjeto = objetoRequestJson.get("nombreObjeto").asText();
                String ultimoNodo = objetoRequestJson.get("ultimoNodo").asText();
                boolean mantenerSinTimeZone = true;
                if (objetoRequestJson.get("mantenerSinTimeZone") != null && !objetoRequestJson.get("mantenerSinTimeZone").isNull()) {
                    mantenerSinTimeZone = objetoRequestJson.get("mantenerSinTimeZone").asBoolean();
                }
                String gmt = objetoRequestJson.get("gmt") != null && !objetoRequestJson.get("gmt").isNull() ? objetoRequestJson.get("gmt").asText() : GMT_TIPO + "-5";

                String idPublicoPrincipal;
                if (objetoRequestJson.get("idPublicoOwner") != null) {
                    idPublicoPrincipal = objetoRequestJson.get("idPublicoOwner").asText();
                } else {
                    idPublicoPrincipal = idPublico;
                }

                AsignarLeccionesResponse out = new AsignarLeccionesResponse();
                DataToken dataToken = new DataToken(prefijo, idPublico, versionApp, urlWrite, urlRead);
                String tokenEncript = generarTokenGetObjeto(dataToken);
                JsonNode estudianteCurso = null;
                boolean isDuplicado = false;
                try {
                    String bodyEstudianteLeccion = getBodyEstudianteCursoById(idEstudianteCurso);
                    estudianteCurso = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyEstudianteLeccion, "estudiante_leccion").get(0);
                    EstudianteCursoVO estudianteCursoVO = new EstudianteCursoVO();
                    if (estudianteCurso != null && estudianteCurso.get("estudiante") != null && estudianteCurso.get("estudiante").get("id") != null &&
                            estudianteCurso.get("curso") != null && estudianteCurso.get("curso").get("id") != null &&
                            estudianteCurso.get("periodo") != null && estudianteCurso.get("periodo").get("id") != null) {

                        // buscar lecciones para el estudiante:
                        String bodyLecciones = getBodyLecciones(estudianteCurso.get("curso").get("id").asText());
                        JsonNode lecciones = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyLecciones, "leccion");
                        if (lecciones != null && lecciones.size() > 0) {
                            // buscar si ya esta procesado en estudiante_curso
                            String idEstudiante = estudianteCurso.get("estudiante").get("id").asText();
                            String idCurso = estudianteCurso.get("curso").get("id").asText();
                            String bodyYaExiste = getBodyEstudianteCursoExiste(idEstudiante, idCurso, idEstudianteCurso);
                            JsonNode estudCursoExiste = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyYaExiste, "estudiante_curso");
                            if (estudCursoExiste != null && estudCursoExiste.size() > 0) {
                                // verificar si es periodo diferente:
                                if (!estudianteCurso.get("periodo").get("id").asText().equals(estudCursoExiste.get(0).get("periodo").get("id").asText())) {
                                    // desactivar lecciones existentes y estudCurso:
                                    desactivarEstudLeccionAndCurso(estudCursoExiste.get(0), dataToken, tokenEncript, true);
                                } else {
                                    isDuplicado = true;
                                    String dni = estudianteCurso.get("estudiante").get("dni") != null ? estudianteCurso.get("estudiante").get("dni").asText() : "-";
                                    String curso = estudianteCurso.get("curso").get("nombre") != null ? estudianteCurso.get("curso").get("nombre").asText() : "-";
                                    String periodo = estudianteCurso.get("periodo").get("nombre") != null ? estudianteCurso.get("periodo").get("nombre").asText() : "-";
                                    errList.addError("2.2", "Error estudiante_leccion", "estudiante " + dni + " ya se encuentra registrado para el curso " + curso +
                                            " en el período " + periodo);
                                    throw errList;
                                }
                            }
                            // se desactiva sus lecciones anteriores, por si existen:
                            desactivarEstudLeccionAndCurso(estudianteCurso, dataToken, tokenEncript, false);
                            // insertar nuevas lecciones
                            String bodyInsertLecciones = getBodyInsertEstudLecciones(estudianteCurso.get("estudiante").get("id").asText(), lecciones);
                            JsonNode respInsertLecciones = integrationService.insertarObjetoApi(dataToken, bodyInsertLecciones, "estudiante_leccion");
                            if (respInsertLecciones != null && respInsertLecciones.size() == lecciones.size()) {
                                List<LeccionVO> leccionVOList = getLeccionesVOLst(respInsertLecciones);
                                estudianteCursoVO.setEstudiante_leccionLst(leccionVOList);
                            } else {
                                errList.addError("2.1", "Error estudiante_leccion", "error insertando estudiante_leccion");
                                throw errList;
                            }
                        } else {
                            estudianteCursoVO.setMensaje("Curso sin lecciones");
                        }

                        estudianteCursoVO.setIdentificador(estudianteCurso.get("id").asText());
                        estudianteCursoVO.setEstado(true);
                        out.setEstudianteCurso(estudianteCursoVO);
                        // verificar si esta desactivado para ponerlo como activo
                        if (!estudianteCurso.get("activo").asBoolean()) {
                            String bodyUpdateDesactivarEC = getBodyActivarEstudCurso(estudianteCurso, true);
                            integrationService.actualizarObjetoApi(dataToken, bodyUpdateDesactivarEC, "estudiante_curso", true);
                        }
                        try {
                            // eliminar progExternoError en mongo par si existe:
                            progExternoErrorService.findAndDeleteProgExternoError(KEY_PROG_ASIGNAR_LECCIONES, prefijo, versionApp, nombreObjeto, idEstudianteCurso);

                            // tratar de guardar registro en mongo para ejecucion de cron de notificar por correo
                            // se guardan registros por cada ambiente y version aplicacion.
                            cronObjetoService.findAndSaveCronObjeto(KEY_CRON_ASIGNAR_LECCIONES, dataToken, idPublicoPrincipal, mantenerSinTimeZone, gmt);

                            // poner en estado confirmado:
                            String bodyActualizarEstado = getBodyEstadObjeto(idEstudianteCurso, nombreObjeto, nombreEstadoConfirmado);
                            integrationService.actualizarObjetoApi(dataToken, bodyActualizarEstado, "estudiante_curso", true);

                            // insertar wf:
                            String bodyInsertWf = armarObjetoWf(nombreObjeto, idEstudianteCurso, nombreEstadoConfirmado, mantenerSinTimeZone);
                            integrationService.insertarObjetoApi(dataToken, bodyInsertWf, "estudiante_curso");
                        } catch (Exception e) {
                            logger.info("***** Error Asignar lecciones insertar estado Confirmado. idEstudainteLeccion: " + idEstudianteCurso + " Ex: ", e);
                        }
                    } else {
                        errList.addError("2.0", "Error estudiante_curso", "verifique que existan las relaciones estudiante, curso y periodo. Son obligatorias");
                        throw errList;
                    }
                } catch (Exception e) {
                    logger.info("***** Error Asignar lecciones. idEstudainteLeccion: " + idEstudianteCurso + " Ex: ", e);
                    EstudianteCursoVO estudianteCursoVOEx = new EstudianteCursoVO();
                    estudianteCursoVOEx.setMensaje("no procesado");
                    estudianteCursoVOEx.setEstado(false);
                    estudianteCursoVOEx.setIdentificador(idEstudianteCurso);
                    estudianteCursoVOEx.setCodigoError("400");
                    if (((MyErrorListException) e).getErrors() != null && ((MyErrorListException) e).getErrors().size() > 0) {
                        estudianteCursoVOEx.setErrores(((MyErrorListException) e).getErrors());
                    }
                    if (estudianteCurso != null) {
                        try {
                            if (!isDuplicado) {
                                // desactivarEstudLeccionAndCurso(estudianteCurso, dataToken, tokenEncript, true);
                            } else {
                                // si es duplicado solo desactivar el registro estudiante_curso
                                String bodyUpdateDesactivarEC = getBodyActivarEstudCurso(estudianteCurso, false);
                                integrationService.actualizarObjetoApi(dataToken, bodyUpdateDesactivarEC, "estudiante_curso", true);
                            }
                        } catch (Exception ex) {
                            logger.info("***** Error Asignar lecciones catch. idEstudainteLeccion: " + idEstudianteCurso + " Ex: ", ex);
                        }
                    }
                    out.setEstudianteCurso(estudianteCursoVOEx);
                    // buscar progExternoError, si existe incrementar num de ejecuciones de lo contrario crearlo
                    String idProgExternoError = generarPkObjetoMongo(KEY_PROG_ASIGNAR_LECCIONES, prefijo, versionApp, nombreObjeto, idEstudianteCurso);
                    String url = "/amauta/asignarLecciones";
                    progExternoErrorService.findAndSaveUpdateProgExternoError(idProgExternoError, url, TIPO_PETICION_POST, objetoRequest, dataToken, estudianteCursoVOEx.getErrores());
                }
                return out;
            } else {
                errList.addError("1.1", "faltan precondiciones", "los parametros prefijo y versionApp son obligatorios");
                throw errList;
            }
        } catch (Exception e) {
            if (((MyErrorListException) e).getErrors() != null && ((MyErrorListException) e).getErrors().size() > 0) {
                errList.addErrorList(((MyErrorListException) e).getErrors());
            } else {
                errList.addError("10.2", "Error proceso de asignar lecciones:", e.getMessage());
            }
            throw errList;
        }
    }

    public void desactivarEstudLeccionAndCurso(JsonNode estudianteCurso, DataToken dataToken, String tokenEncript, boolean desactivarEstudianteCurso) {
        MyErrorListException errList = new MyErrorListException();
        try {
            String bodyLeccionesExist = getBodyLeccionesExist(estudianteCurso.get("estudiante").get("id").asText(), estudianteCurso.get("curso").get("id").asText());
            JsonNode estudLeccionesExist = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyLeccionesExist, "estudiante_leccion");
            if (estudLeccionesExist != null && estudLeccionesExist.size() > 0) {
                // desactivar estudiante_leccion y estudiante_curso:
                for (JsonNode estudLeccExist : estudLeccionesExist) {
                    if (estudLeccExist.get("leccion") != null && estudLeccExist.get("leccion").get("id") != null) {
                        String bodyUpdateDesactivar = getBodyUpadateLeccionExist(estudLeccExist);
                        integrationService.actualizarObjetoApi(dataToken, bodyUpdateDesactivar, "estudiante_leccion", true);
                    }
                }
            }
            if (desactivarEstudianteCurso) {
                String bodyUpdateDesactivarEC = getBodyActivarEstudCurso(estudianteCurso, false);
                integrationService.actualizarObjetoApi(dataToken, bodyUpdateDesactivarEC, "estudiante_curso", true);
            }
        } catch (Exception e) {
            errList.addError("10.1", "error desactivando insertregisterpost update", "no fue posible desactivar estudiante_leccion y estudiante_curso");
            throw errList;
        }
    }

    @Override
    public RespuestaVO calcularAvanceCurso(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        RespuestaVO response = new RespuestaVO();
        response.setCode("200");
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
            JsonNode objetoBase = objetoRequestJson.get("objeto");
            String idObjeto = objetoBase.get("id").asText();

            if (objetoBase.get("procesado") == null || (objetoBase.get("procesado") != null && objetoBase.get("procesado").asBoolean() == false)) {
                // trear estudiante y curso:
                String bodyGetObjeto = getBodyEstudLecciones(idObjeto);
                DataToken dataToken = new DataToken(prefijo, idPublico, versionApp, urlWrite, urlRead);
                String tokenEncript = generarTokenGetObjeto(dataToken);
                JsonNode objetoBaseLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyGetObjeto, "estudiante_leccion");
                if (objetoBaseLst != null && objetoBaseLst.size() > 0) {
                    JsonNode objBaseConRel = objetoBaseLst.get(0);
                    if (!compareInsenstiveCad(objBaseConRel.get("leccion").get("tipo_leccion").get("nombre").asText(), TIPO_LECCION_CONFE) &&
                            !compareInsenstiveCad(objBaseConRel.get("leccion").get("tipo_leccion").get("nombre").asText(), TIPO_LECCION_ENCUESTA)) {
                        if (objBaseConRel.get("estudiante") != null && objBaseConRel.get("leccion") != null &&
                                objBaseConRel.get("leccion").get("modulo") != null && objBaseConRel.get("leccion").get("modulo").get("curso") != null) {
                            String idEstudiante = objBaseConRel.get("estudiante").get("id").asText();
                            String idCurso = objBaseConRel.get("leccion").get("modulo").get("curso").get("id").asText();

                            // obtener lecciones del estudiante y curso y luego filtrar lecciones q no sean conferencia ni encuesta:
                            String bodyLstEstudLecciones = getBodyEstudiantesLecciones(idEstudiante, idCurso);
                            JsonNode lstEstudianteLeccion = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyLstEstudLecciones, "estudiante_leccion");
                            List<JsonNode> lstEstudLeccionFiltradas = new ArrayList<>();
                            Integer contProcesados = 0;
                            if (lstEstudianteLeccion != null && lstEstudianteLeccion.size() > 0) {
                                for (JsonNode estudianteLeccion : lstEstudianteLeccion) {
                                    if (estudianteLeccion.get("leccion") != null && estudianteLeccion.get("leccion").get("modulo") != null &&
                                            estudianteLeccion.get("leccion").get("modulo").get("curso") != null) {
                                        if (!compareInsenstiveCad(estudianteLeccion.get("leccion").get("tipo_leccion").get("nombre").asText(), TIPO_LECCION_CONFE) &&
                                                !compareInsenstiveCad(estudianteLeccion.get("leccion").get("tipo_leccion").get("nombre").asText(), TIPO_LECCION_ENCUESTA)) {
                                            JsonNode node= estudianteLeccion.get("estado_estudiante_leccion");
                                            if (node!=null){
                                                JsonNode nombre=node.get("nombre");
                                                if (nombre!=null){
                                                    String lessonStatus = nombre.asText();
                                                    if (lessonStatus!=null && lessonStatus.equals(ESTADO_FINALIZADO) || lessonStatus.equals(ESTADO_DESAPROBADO) || lessonStatus.equals(ESTADO_APROBADO)) {
                                                        contProcesados++;
                                                    }
                                                }
                                            }
                                            lstEstudLeccionFiltradas.add(estudianteLeccion);
                                        }
                                    }
                                }
                            }
                            if (lstEstudLeccionFiltradas.size() > 0) {
                                // calcular avance:
                                Integer total = lstEstudLeccionFiltradas.size();
                                Double avanceLeccion = Double.valueOf((float) (contProcesados * 80) / total);

                                // buscar estudiante_curso para actualizar
                                String bodyEstudianteCurso = getBodyEstudianteCurso(idEstudiante, idCurso);
                                JsonNode estudianteCursoLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyEstudianteCurso, "estudiante_curso");
                                if (estudianteCursoLst != null && estudianteCursoLst.size() > 0) {
                                    String idEstudianteCurso = estudianteCursoLst.get(0).get("id").asText();
                                    Double avanceFeed = (double) 0;
                                    if (estudianteCursoLst.get(0).get("avance_feed") != null) {
                                        avanceFeed = estudianteCursoLst.get(0).get("avance_feed").asDouble();
                                    }
                                    Double avanceRefeed = (double) 0;
                                    if (estudianteCursoLst.get(0).get("avance_refeed") != null) {
                                        avanceRefeed = estudianteCursoLst.get(0).get("avance_refeed").asDouble();
                                    }

                                    Double avanceTotalTemp = formatearDecimales((avanceRefeed + avanceLeccion + avanceFeed), 1);
                                    String avanceTotal = avanceTotalTemp + "%";
                                    String bodyUpdateEstudCurso = getBodyActualizarAvance(idEstudianteCurso, avanceTotal, avanceLeccion, TIPO_AVANCE_LECCION);
                                    integrationService.actualizarObjetoApi(dataToken, bodyUpdateEstudCurso, "estudiante_curso", true);
                                    // poner estudiante_leccion como procesado:
                                    String bodyUpdateEstudLeccion = getBodyUpdateEstudianteLeccion(objBaseConRel.get("id").asText());
                                    integrationService.actualizarObjetoApi(dataToken, bodyUpdateEstudLeccion, "estudiante_leccion", true);
                                    response.setMessage("success");
                                    return response;
                                } else {
                                    errList.addError("10.1", "error getObjeto estudiante_curso", "no fue posible obtener estudiante_curso. estudiante_leccion: " + idObjeto);
                                    throw errList;
                                }
                            } else {
                                errList.addError("10.1", "error getObjeto estudiante_leccion", "no fue posible obtener lista de estudiante_leccion. estudiante_leccion: " + idObjeto);
                                throw errList;
                            }
                        } else {
                            errList.addError("10.1", "error obtener estudiante y curso", "no fue posible obtener estudiante y curso a partir de estudiante_leccion: " + idObjeto);
                            throw errList;
                        }
                    } else {
                        response.setMessage("Este tipo de lección no se tiene en cuenta para el calculo del avanceCurso");
                        return response;
                    }
                } else {
                    errList.addError("10.1", "error obtener estudiante_leccion", "no fue posible obtener estudiante_leccion actual id: " + idObjeto);
                    throw errList;
                }
            } else {
                response.setMessage("success. el avance para la leccion ya estaba calculado. estudiante_leccion: " + idObjeto);
                return response;
            }
        } catch (Exception e) {
            if (((MyErrorListException) e).getErrors() != null && ((MyErrorListException) e).getErrors().size() > 0) {
                errList.addErrorList(((MyErrorListException) e).getErrors());
            } else {
                errList.addError("10.1", "error ws calcularAvanceCurso", "no fue posible calcular avance " + e.getMessage());
            }
            throw errList;
        }
    }

    @Override
    public RespuestaVO calcularAvanceCursoFeedBack(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        RespuestaVO response = new RespuestaVO();
        response.setCode("200");
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
            JsonNode objetoBase = objetoRequestJson.get("objeto");
            String idObjeto = objetoBase.get("id").asText();
            String ultimoNodo = objetoRequestJson.get("ultimoNodo").asText();
            DataToken dataToken = new DataToken(prefijo, idPublico, versionApp, urlWrite, urlRead);
            String tokenEncript = generarTokenGetObjeto(dataToken);

            // buscar estudiante:
            String bodyFeedBackEstudiante = getBodyFeedBackEstudiante(idObjeto);
            JsonNode respLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyFeedBackEstudiante, "feedback_estudiante");
            if (respLst != null && respLst.size() > 0 && respLst.get(0).get("estudiante") != null) {
                JsonNode feedBackEstudiante = respLst.get(0);
                String idEstudiante = feedBackEstudiante.get("estudiante").get("id").asText();
                String idCurso = feedBackEstudiante.get("estudiante_leccion").get("leccion").get("modulo").get("curso").get("id").asText();

                // buscar curso:
                String bodyEstudianteCurso = getBodyEstudianteCurso(idEstudiante, idCurso);
                JsonNode respEstudCursoLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyEstudianteCurso, "estudiante_curso");
                if (respEstudCursoLst != null && respEstudCursoLst.size() > 0 && respEstudCursoLst.get(0).get("curso") != null) {
                    JsonNode estudianteCurso = respEstudCursoLst.get(0);

                    // buscar lecciones de tipo evaluacion escrita del curso:
                    String bodyLeccionesCursoAvance = getBodyLeccionesCursoAvance(NOMBRE_TIPO_LECCION_FEEDBACK, idCurso);
                    JsonNode leccionesLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyLeccionesCursoAvance, "leccion");
                    if (leccionesLst != null && leccionesLst.size() > 0) {
                        Integer totalLecciones = leccionesLst.size();
                        // buscar registros feedback_estudiante asignados al estudiante y curso:
                        String bodyLstFeedEstud = getBodyLstFeedEstudiante(idEstudiante, idCurso);
                        JsonNode feedEstudianteLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyLstFeedEstud, "feedback_estudiante");
                        if (feedEstudianteLst != null && feedBackEstudiante.size() > 0) {
                            // buscar primer estado en wf_feedback_estudiante:
                            String bodyWfFeedEstud = getBodyWfFeedEstudPrimerEstado(idObjeto);
                            JsonNode respWfFeedEstud = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyWfFeedEstud, "wf_feedback_estudiante");
                            if (respWfFeedEstud != null && respWfFeedEstud.size() > 0) {
                                String nombrePrimerNodo = respWfFeedEstud.get(0).get("nodo").asText();
                                List<String> idLeccionFeedLst = new ArrayList<>();
                                int contAvanceFeed = 0;
                                for (JsonNode nodoFeedBack : feedEstudianteLst) {
                                    if (nodoFeedBack.get("estado_feedback_estudiante") != null && nodoFeedBack.get("estudiante_leccion") != null) {
                                        String nombreEstadoNodoFeed;
                                        if (nodoFeedBack.get("id").asText().equals(idObjeto)) {
                                            nombreEstadoNodoFeed = ultimoNodo;
                                        } else {
                                            nombreEstadoNodoFeed = nodoFeedBack.get("estado_feedback_estudiante").get("nombre").asText();
                                        }
                                        String idLeccionFeed = nodoFeedBack.get("estudiante_leccion").get("leccion").get("id").asText();
                                        if (!nombreEstadoNodoFeed.equals(nombrePrimerNodo) && !verificarElementoEnListado(idLeccionFeed, idLeccionFeedLst)) {
                                            idLeccionFeedLst.add(idLeccionFeed);
                                            contAvanceFeed++;
                                        }
                                    }
                                }
                                Double avanceFeedBack = Double.valueOf((float) (contAvanceFeed * 10) / totalLecciones);

                                Double avanceLeccion = (double) 0;
                                if (estudianteCurso.get("avance_leccion") != null) {
                                    avanceLeccion = estudianteCurso.get("avance_leccion").asDouble();
                                }
                                Double avanceRefeed = (double) 0;
                                if (estudianteCurso.get("avance_refeed") != null) {
                                    avanceRefeed = estudianteCurso.get("avance_refeed").asDouble();
                                }

                                Double avanceTotalTemp = formatearDecimales((avanceRefeed + avanceLeccion + avanceFeedBack), 1);
                                String avanceTotal = avanceTotalTemp + "%";
                                // actualizar estudinte_curso:
                                String bodyUpdateEstudCurso = getBodyActualizarAvance(estudianteCurso.get("id").asText(), avanceTotal, avanceFeedBack, TIPO_AVANCE_FEEDBACK);
                                integrationService.actualizarObjetoApi(dataToken, bodyUpdateEstudCurso, "estudiante_curso", true);
                            } else {
                                errList.addError("1.1", "faltan precondiciones", "error buscando lista de wf_feedback_estudiante. id feedback_estudiante=" + idObjeto);
                                throw errList;
                            }
                        } else {
                            errList.addError("1.1", "faltan precondiciones", "error buscando lista de feedback_estudiante. id feedback_estudiante=" + idObjeto);
                            throw errList;
                        }
                    } else {
                        response.setMessage("No se encontraron lecciones para calcuar avance");
                    }
                } else {
                    errList.addError("1.1", "faltan precondiciones", "error buscando estudiante_curso. id feedback_estudiante=" + idObjeto);
                    throw errList;
                }
            } else {
                errList.addError("1.1", "faltan precondiciones", "error buscando registro feedback_estudiante id=" + idObjeto);
                throw errList;
            }

            return response;
        } catch (Exception e) {
            if (((MyErrorListException) e).getErrors() != null && ((MyErrorListException) e).getErrors().size() > 0) {
                errList.addErrorList(((MyErrorListException) e).getErrors());
            } else {
                errList.addError("10.1", "error ws calcularAvanceCursoFeedBack", "no fue posible calcular avance " + e.getMessage());
            }
            throw errList;
        }
    }

    @Override
    public RespuestaVO asignarEvaluacionTareas(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        RespuestaVO response = new RespuestaVO();
        response.setCode("200");
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
            JsonNode objetoBase = objetoRequestJson.get("objeto");
            String idObjeto = objetoBase.get("id").asText();
            String idPublicoPrincipal;
            if (objetoRequestJson.get("idPublicoOwner") != null) {
                idPublicoPrincipal = objetoRequestJson.get("idPublicoOwner").asText();
            } else {
                idPublicoPrincipal = idPublico;
            }
            boolean mantenerSinTimeZone = true;
            if (objetoRequestJson.get("mantenerSinTimeZone") != null && !objetoRequestJson.get("mantenerSinTimeZone").isNull()) {
                mantenerSinTimeZone = objetoRequestJson.get("mantenerSinTimeZone").asBoolean();
            }
            String gmt = objetoRequestJson.get("gmt") != null && !objetoRequestJson.get("gmt").isNull() ? objetoRequestJson.get("gmt").asText() : GMT_TIPO + "-5";

            String nomRelEvaluacionEscrita;
            nomRelEvaluacionEscrita = "evaluacion_escrita";

            DataToken dataToken = new DataToken(prefijo, idPublicoPrincipal, versionApp, urlWrite, urlRead);
            String tokenEncript = generarTokenGetObjeto(dataToken);

            // buscar estudiante_leccion con sus relaciones:
            String bodyEstudianteLeccion = getBodyEstudianteLeccionAE(idObjeto);
            JsonNode respLstEstudLeccion = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyEstudianteLeccion, "estudiante_leccion");
            if (respLstEstudLeccion != null && respLstEstudLeccion.size() > 0 && respLstEstudLeccion.get(0).get("estudiante") != null &&
                    respLstEstudLeccion.get(0).get("leccion") != null && respLstEstudLeccion.get(0).get("leccion").get("tipo_leccion") != null) {
                JsonNode miLeccion = respLstEstudLeccion.get(0).get("leccion");
                if (compareInsenstiveCad(miLeccion.get("tipo_leccion").get("nombre").asText(), NOMBRE_TIPO_LECCION_FEEDBACK)) {
                    JsonNode miEstudiante = respLstEstudLeccion.get(0).get("estudiante");
                    if (miEstudiante.get("categoria") != null && miEstudiante.get("grado") != null) {
                        
                        String bodyEstudianteCurso = getBodyEstudianteCurso(EstudianteCursoUtils.crearFiltrosEstudianteCurso(miEstudiante.get("id").asInt()), EstudianteCursoUtils.crearCamposEstudianteCurso());
                        JsonNode respLstEstudCurso = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyEstudianteCurso, "estudiante_curso");

                        String rango = respLstEstudCurso.get(0).get("avancetotal").asText().replace("%", "");
                        Double rangoDouble = Double.valueOf(rango);
                        List<String> avances = new ArrayList<>();
                        List<String> idsEstudianteExcluir = new ArrayList<>();
                
                        idsEstudianteExcluir.add(miEstudiante.get("id").asText());

                        Date fechaActual = new Date();
                        Date fechaModificacionEstCur = sumarDiasAFecha(fechaActual, -7);
                        String fechaModificacionEstCurCad = formatDateSinHora(fechaModificacionEstCur);
                            
                        if( rangoDouble>= 0 && rangoDouble <= 20){
                            avances.add("0%");
                            avances.add("20%");
                        }else{
                            avances.add("21%");
                            avances.add("80%");
                        }
 
                        String nivelesAcademicos = getNivelesAcademicosAsignar(miEstudiante.get("categoria").asText());
                        // obtener posibles estudiantes a asignar:
                        String bodyEstudiantesPosiblesNew = getBodyEstudianteCurso(EstudianteCursoUtils.crearFiltrosEstudianteCursoAE(fechaModificacionEstCurCad, avances, idsEstudianteExcluir, nivelesAcademicos, miEstudiante.get("grado").asText(), respLstEstudCurso.get(0).get("periodo").get("id").asText()), EstudianteCursoUtils.crearCamposEstudianteCursoAE());
                        JsonNode estudiantesPosiblesLstNew = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyEstudiantesPosiblesNew, "estudiante_curso");
                        // String bodyEstudiantesPosibles = getBodyPosiblesEstudiantesAE(miEstudiante.get("id").asText(), nivelesAcademicos, miEstudiante.get("grado").asText());
                        // JsonNode estudiantesPosiblesLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyEstudiantesPosibles, "estudiante");
                        List<String> idsEstudiantesRegistrar = new ArrayList<>();
                        JsonNode usuarioAdmin = null;
                        if (estudiantesPosiblesLstNew != null && estudiantesPosiblesLstNew.size() > NUM_ESTUDIANTES_ASIGNAR) {
                            // dar prioridad a los estudiantes que no tienen asignación:
                            // obtener feedback_estudiante registrados:
                            String bodyFeedRegistrados = getBodyFeedbackRegistradosAE(miLeccion.get("id").asText());
                            JsonNode feedRegistradosLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyFeedRegistrados, "feedback_estudiante");

                            // filtrar estudiantes q no han sido registrados:
                            for (JsonNode estudiantePosibleNew : estudiantesPosiblesLstNew) {
                                JsonNode estudiantePosible = estudiantePosibleNew.get("estudiante");
                                if (!verificarEstudianteEnListaFeed(feedRegistradosLst, estudiantePosible.get("id").asText()) &&
                                        idsEstudiantesRegistrar.size() < NUM_ESTUDIANTES_ASIGNAR) {
                                    idsEstudiantesRegistrar.add(estudiantePosible.get("id").asText());
                                }
                            }
                            if (idsEstudiantesRegistrar.size() < NUM_ESTUDIANTES_ASIGNAR) {
                                for (JsonNode estudiantePosibleNew : estudiantesPosiblesLstNew) {
                                    JsonNode estudiantePosible = estudiantePosibleNew.get("estudiante");
                                    if (!verificarElementoEnListado(estudiantePosible.get("id").asText(), idsEstudiantesRegistrar) &&
                                            idsEstudiantesRegistrar.size() < NUM_ESTUDIANTES_ASIGNAR) {
                                        idsEstudiantesRegistrar.add(estudiantePosible.get("id").asText());
                                    }
                                }
                            }
                        } else if (estudiantesPosiblesLstNew != null && estudiantesPosiblesLstNew.size() == NUM_ESTUDIANTES_ASIGNAR) {
                            // asignar a los 5 estudiantes que llegan
                            for (JsonNode estudiantePosibleNew : estudiantesPosiblesLstNew) {
                                JsonNode estudiantePosible = estudiantePosibleNew.get("estudiante");
                                idsEstudiantesRegistrar.add(estudiantePosible.get("id").asText());
                            }
                        } else {
                            // consultar adminamauta
                            String bodyAdminAmauta = getBodyAdminAmautaAE();
                            JsonNode respAdminAmauta = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyAdminAmauta, "usuairo");
                            usuarioAdmin = respAdminAmauta != null && respAdminAmauta.size() > 0 ? respAdminAmauta.get(0) : null;
                            if (estudiantesPosiblesLstNew != null && estudiantesPosiblesLstNew.size() > 0) {
                                for (JsonNode estudiantePosibleNew : estudiantesPosiblesLstNew) {
                                    JsonNode estudiantePosible = estudiantePosibleNew.get("estudiante");
                                    idsEstudiantesRegistrar.add(estudiantePosible.get("id").asText());
                                }
                            }
                        }
                        // buscar evaluacionescrita para relacionarla:
                        String bodyEvalEscrita = getBodyEvalEscrita(idObjeto, nomRelEvaluacionEscrita);
                        JsonNode evalEscritaLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyEvalEscrita, nomRelEvaluacionEscrita);
                        JsonNode miEvalEscrita = null;
                        if (evalEscritaLst != null && evalEscritaLst.size() > 0) {
                            miEvalEscrita = mapper.createObjectNode();
                            ((ObjectNode) miEvalEscrita).put("id", evalEscritaLst.get(0).get("id").asText());
                        }

                        // armar peticion para insertar en feedback_estudiante:
                        String bodyInsertFeddbackEstudAE = getBodyInsertFeddbackEstudAE(idObjeto, idsEstudiantesRegistrar, usuarioAdmin, miEvalEscrita, nomRelEvaluacionEscrita);
                        JsonNode respInsertFeedEstudAE = integrationService.insertarObjetoApi(dataToken, bodyInsertFeddbackEstudAE, "estudiante_leccion");
                        if (respInsertFeedEstudAE == null || (respInsertFeedEstudAE != null && respInsertFeedEstudAE.size() == 0)) {
                            errList.addError("10.1", "error insert feedback_estudiante", "ocurrió un error al insertar en feedback_estudiante. idEstudainteLeccion:" + idObjeto);
                            throw errList;
                        }
                        // tratar de guardar registro en mongo para ejecucion de cron de asignar feedback al admin.
                        // se guardan registros por cada ambiente y version aplicacion.
                        cronObjetoService.findAndSaveCronObjeto(KEY_CRON_ASIGNAR_LECCIONES, dataToken, idPublicoPrincipal, mantenerSinTimeZone, gmt);
                    } else {
                        errList.addError("10.1", "faltan precondiciones asignarEvaluacionTareas", "el estudiante debe tener grado y categoria. idEstudainteLeccion:" + idObjeto);
                        throw errList;
                    }
                } else {
                    errList.addError("10.1", "faltan precondiciones asignarEvaluacionTareas", "tipo evaluacion debe ser " + NOMBRE_TIPO_LECCION_FEEDBACK + ". idEstudainteLeccion:" + idObjeto);
                    throw errList;
                }
            } else {
                errList.addError("10.1", "faltan precondiciones asignarEvaluacionTareas", "no se encontró el estudiante en la bd go. idEstudainteLeccion:" + idObjeto);
                throw errList;
            }
            return response;
        } catch (Exception e) {
            if (((MyErrorListException) e).getErrors() != null && ((MyErrorListException) e).getErrors().size() > 0) {
                errList.addErrorList(((MyErrorListException) e).getErrors());
            } else {
                errList.addError("10.1", "error ws asignarEvaluacionTareas", "ex: " + e.getMessage());
            }
            throw errList;
        }
    }

    @Override
    public boolean asignarFeedAdminCron() {
        final String nombreObjetoFeed = "feedback_estudiante";
        try {
            List<CronObjeto> cronObjetoList = cronObjetoService.getAllCronObjeto();
            if (cronObjetoList != null && cronObjetoList.size() > 0) {
                for (CronObjeto cronObjeto : cronObjetoList) {
                    if (cronObjeto.getId().startsWith(KEY_CRON_ASIGNAR_LECCIONES)) {
                        try {
                            DataToken dataToken = new DataToken();
                            dataToken.setUrlApiRead(getUrlRead(cronObjeto.getPrefijo()));
                            dataToken.setUrlApiWrite(getUrlWrite(cronObjeto.getPrefijo()));
                            dataToken.setIdPublico(cronObjeto.getIdPublicoUsuario());
                            dataToken.setVersionAplicacion(cronObjeto.getVersion());
                            dataToken.setPrefijo(cronObjeto.getPrefijo());
                            String tokenEncript = generarTokenGetObjeto(dataToken);

                            // establecer limite de fechas de hace un mes
                            Date fechaActual = new Date();
                            Date fechaActualSinHora = getDateSinHoraByDate(fechaActual);
                            Date fechaFin = sumarDiasAFecha(fechaActual, -28);
                            Date fechaInicio = sumarDiasAFecha(fechaActual, -31);
                            String fechaFinCad = formatDateSinHora(fechaFin);
                            String fechaInicioCad = formatDateSinHora(fechaInicio);

                            JsonNode bodyFeedEstudianteLstCron = getBodyFeedEstudianteLstCron(fechaInicioCad, fechaFinCad, nombreObjetoFeed, false, false);
                            List<JsonNode> feedEstudianteLst = integrationService.getListaObjetosApiPaginado(dataToken, tokenEncript, bodyFeedEstudianteLstCron, "feedback_estudiante");
                            if (feedEstudianteLst != null && feedEstudianteLst.size() > 0) {
                                // buscar Admin:
                                String bodyAdminAmauta = getBodyAdminAmautaAE();
                                JsonNode respAdminAmauta = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyAdminAmauta, "usuairo");
                                JsonNode usuarioAdmin = respAdminAmauta != null && respAdminAmauta.size() > 0 ? respAdminAmauta.get(0) : null;

                                // obtener nombre del primer estado:
//                                String bodyWfFeedEstud = getBodyWfFeedEstudPrimerEstado(feedEstudianteLst.get(0).get("id").asText());
//                                JsonNode respWfFeedEstud = amautaRepositry.getListaObjetosApi(dataToken, tokenEncript, bodyWfFeedEstud, "wf_feedback_estudiante");
                                if (usuarioAdmin != null) {
                                    //String nombrePrimerNodo = respWfFeedEstud.get(0).get("nodo").asText();
                                    List<String> idsEstudLeccionProcess = new ArrayList<>();
                                    for (JsonNode feedEstudiante : feedEstudianteLst) {
                                        String fechaCreacionCad = feedEstudiante.get("fecha_creacion").asText();
                                        Date fechaCreacionSinHora;
                                        // obtener fecha de creacion sin hora y en formato de hora local:
                                        if (!cronObjeto.isMantenerSinTimeZone()) {
                                            // convertir de utc a hora local:
                                            Date fechaTemp = sumarRestarHorasByGMT(fechaCreacionCad, cronObjeto.getGmt());
                                            fechaCreacionSinHora = getDateSinHoraByDate(fechaTemp);
                                        } else {
                                            fechaCreacionSinHora = getFechaByCadenaSinHora(fechaCreacionCad);
                                        }
                                        Integer diferenciaDias = getDiasDiferencia(fechaCreacionSinHora, fechaActualSinHora);
                                        // ** SI HAN PASADO 30 DIAS DESDE LA FECHA CREACION **
                                        if (diferenciaDias.equals(30) && feedEstudiante.get("estudiante_leccion") != null &&
                                                !feedEstudiante.get("estudiante_leccion").isNull()) {
                                            String idEstudianteLeccion = feedEstudiante.get("estudiante_leccion").get("id").asText();
                                            if (!verificarElementoEnListado(idEstudianteLeccion, idsEstudLeccionProcess)) {
                                                idsEstudLeccionProcess.add(idEstudianteLeccion);
                                                // buscar si ya fue asignada al admin en otra fecha:
                                                JsonNode bodyFeedEstudAdimExist = getBodyFeedEstudAdimExist(idEstudianteLeccion, usuarioAdmin.get("id").asText(), nombreObjetoFeed);
                                                List<JsonNode> feedEstudAdimExistLst = integrationService.getListaObjetosApiPaginado(dataToken, tokenEncript, bodyFeedEstudAdimExist, "feedback_estudiante");
                                                if (feedEstudAdimExistLst != null && feedEstudAdimExistLst.size() > 0) {
                                                    logger.info("*** Cron asingar Feed Admin. ya tiene una asignación para el admin. idEstudainteLeccion:" + idEstudianteLeccion);
                                                } else {
                                                    // obtener los feedback_estudiante de la estudiante_leccion actual y marcarla como procesada
                                                    List<JsonNode> miGrupofeedEstudianteLst = new ArrayList<>();
                                                    for (JsonNode feedEstudianteGrupo : feedEstudianteLst) {
                                                        if (feedEstudianteGrupo.get("estudiante_leccion") != null && !feedEstudianteGrupo.get("estudiante_leccion").isNull() &&
                                                                feedEstudianteGrupo.get("estudiante_leccion").get("id").asText().equals(idEstudianteLeccion)) {
                                                            miGrupofeedEstudianteLst.add(feedEstudianteGrupo);
                                                        }
                                                    }
                                                    // verificar grupoEstudianteFeedback list para asignar o no al Admin:
                                                    boolean asignarAdmin = true;
                                                    for (JsonNode feedEstudG : miGrupofeedEstudianteLst) {
                                                        if (feedEstudG.get("usuario") != null && !feedEstudG.get("usuario").isNull() &&
                                                                feedEstudG.get("usuario").get("id") != null) {
                                                            asignarAdmin = false;
                                                        } else if (feedEstudG.get("estado_feedback_estudiante") != null && !feedEstudG.get("estado_feedback_estudiante").isNull() &&
                                                                feedEstudG.get("estado_feedback_estudiante").get("nombre").asText().equals(ESTADO_APROBADO)) {
                                                            asignarAdmin = false;
                                                        }
                                                    }
                                                    if (asignarAdmin) {
                                                        String idEalaucionEsctita = null;
                                                        if (feedEstudiante.get("evaluacion_escrita") != null && !feedEstudiante.get("evaluacion_escrita").isNull()) {
                                                           idEalaucionEsctita = feedEstudiante.get("evaluacion_escrita").get("id").asText();
                                                        }else{
                                                            logger.info("Cron asingar Feed Admin feedback sin evaluacionescrita");
                                                        }
                                                        String bodyInsertFeddbackEstudAE = getBodyInsertFeddbackEstudCron(idEstudianteLeccion, idEalaucionEsctita, usuarioAdmin);
                                                        JsonNode respInsertFeedEstudAE = integrationService.insertarObjetoApi(dataToken, bodyInsertFeddbackEstudAE, "estudiante_leccion");
                                                        if (respInsertFeedEstudAE == null || (respInsertFeedEstudAE != null && respInsertFeedEstudAE.size() == 0)) {
                                                            logger.info("Cron asingar Feed Admin. ocurrió un error al insertar en feedback_estudiante. idEstudainteLeccion:" + idEstudianteLeccion);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            logger.info("====ERROR ejecución objeto cron asignarFeedAdminCron : " + cronObjeto.getId() + " EX:" + e.getMessage());
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.info("====ERROR ejecución asignarFeedAdminCron: " + e.getMessage());
        }
        return true;
    }

    @Override
    public boolean notificarCorreoEstudianteLeccionCron() {
        final String tipoLeccionConferencia = "Conferencia";
        final String nomEstudianteLeccion = "estudiante_leccion";
        final String nomObjetoLeccion = "leccion";
        try {
            List<CronObjeto> cronObjetoList = cronObjetoService.getAllCronObjeto();
            if (cronObjetoList != null && cronObjetoList.size() > 0) {
                for (CronObjeto cronObjeto : cronObjetoList) {
                    if (cronObjeto.getId().startsWith(KEY_CRON_ASIGNAR_LECCIONES)) {
                        try {
                            DataToken dataToken = new DataToken();
                            dataToken.setUrlApiRead(getUrlRead(cronObjeto.getPrefijo()));
                            dataToken.setUrlApiWrite(getUrlWrite(cronObjeto.getPrefijo()));
                            dataToken.setIdPublico(cronObjeto.getIdPublicoUsuario());
                            dataToken.setVersionAplicacion(cronObjeto.getVersion());
                            dataToken.setPrefijo(cronObjeto.getPrefijo());
                            String tokenEncript = generarTokenGetObjeto(dataToken);

                            // establecer limite de fechas:
                            Date fechaActual = new Date();
                            Date fechaActualSinHora = getDateSinHoraByDate(fechaActual);
                            Date fechaActualMas2d = sumarDiasAFecha(fechaActualSinHora, 2);
                            Date fechaFin = sumarDiasAFecha(fechaActual, 4);
                            Date fechaInicio = sumarDiasAFecha(fechaActual, -1);
                            String fechaFinCad = formatDateSinHora(fechaFin);
                            String fechaInicioCad = formatDateSinHora(fechaInicio);

                            JsonNode bodyLeccionesConfe = getBodyLeccionesConfeCron(fechaInicioCad, fechaFinCad, nomObjetoLeccion, tipoLeccionConferencia);
                            List<JsonNode> leccionesConfeLst = integrationService.getListaObjetosApiPaginado(dataToken, tokenEncript, bodyLeccionesConfe, nomObjetoLeccion);
                            if (leccionesConfeLst != null && leccionesConfeLst.size() > 0) {
                                for (JsonNode leccionConfe : leccionesConfeLst) {
                                    if (leccionConfe.get("fecha") != null && !leccionConfe.get("fecha").isNull()) {
                                        String fechaConferenciaCad = leccionConfe.get("fecha").asText();
                                        Date fechaConferenciaSinHora;
                                        String fechaHoraConferenciaCad;
                                        // obtener fecha de conferencia sin hora y en formato de hora local:
                                        if (!cronObjeto.isMantenerSinTimeZone()) {
                                            // convertir de utc a hora local:
                                            Date fechaTemp = sumarRestarHorasByGMT(fechaConferenciaCad, cronObjeto.getGmt());
                                            fechaHoraConferenciaCad = getFechaHoraByDate(fechaTemp);
                                            fechaConferenciaSinHora = getDateSinHoraByDate(fechaTemp);
                                        } else {
                                            fechaConferenciaSinHora = getFechaByCadenaSinHora(fechaConferenciaCad);
                                            fechaHoraConferenciaCad = getFechaHoraByCadena(fechaConferenciaCad);
                                        }
                                        // NOTIFICAR SI fechaActual <= fechaConferencia <= (fechaActual + 2 dias)
                                        if ((fechaActualSinHora.before(fechaConferenciaSinHora) || fechaActualSinHora.equals(fechaConferenciaSinHora)) &&
                                                (fechaConferenciaSinHora.before(fechaActualMas2d) || fechaConferenciaSinHora.equals(fechaActualMas2d))) {
                                            Integer diasFaltan = getDiasDiferencia(fechaActualSinHora, fechaConferenciaSinHora);
                                            JsonNode bodyEstudLeccionNotifi = getEstudLeccionNotifiCron(nomEstudianteLeccion, leccionConfe.get("id").asText());
                                            List<JsonNode> estudLeccionNotifiLst = integrationService.getListaObjetosApiPaginado(dataToken, tokenEncript, bodyEstudLeccionNotifi, nomEstudianteLeccion);
                                            if (estudLeccionNotifiLst != null && estudLeccionNotifiLst.size() > 0) {
                                                List<String> destinatariosLst = new ArrayList<>();
                                                for (JsonNode estudLeccion : estudLeccionNotifiLst) {
                                                    if (estudLeccion.get("estudiante") != null && estudLeccion.get("estudiante").get("usuario").get("correo") != null) {
                                                        String correo = estudLeccion.get("estudiante").get("usuario").get("correo").asText();
                                                        if (!correo.isEmpty() && isEmailValid(correo) && !verificarElementoEnListado(correo, destinatariosLst)
                                                                && destinatariosLst.size() < 500) {
                                                            destinatariosLst.add(correo);
                                                        }
                                                    }
                                                }
                                                if (destinatariosLst.size() > 0) {
                                                    JsonNode camposObjetoCorreo = estudLeccionNotifiLst.get(0);
                                                    ((ObjectNode) camposObjetoCorreo).put("n_dias_faltan", diasFaltan != null ? diasFaltan : 0);
                                                    ((ObjectNode) camposObjetoCorreo.get("leccion")).put("fecha", fechaHoraConferenciaCad);

                                                    // hacer peticion emareportes para notificar:
                                                    String bodyNotifiCorreo = getBodyNotificarCorreo(destinatariosLst, ASUNTO_CRON_NOTIFI, camposObjetoCorreo, nomEstudianteLeccion, cronObjeto.getVersion());
                                                    String urlEmareportes = getUrlEmaReportes(cronObjeto.getPrefijo()) + "notificacion/enviarCorreo";
                                                    HttpHeaders headers = new HttpHeaders();
                                                    headers.setContentType(MediaType.APPLICATION_JSON);
                                                    headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
                                                    integracionRepository.getResponseEntityPOST(urlEmareportes, headers, bodyNotifiCorreo);
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        } catch (Exception e) {
                            logger.info("====ERROR ejecución objeto cron notificarCorreoEstudianteLeccionCron : " + cronObjeto.getId() + " EX:" + e.getMessage());
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.info("====ERROR ejecución notificarCorreoEstudianteLeccionCron: " + e.getMessage());
        }
        return true;
    }

    @Override
    public RespuestaVO actualizarEstadoEstudianteLeccion(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        RespuestaVO response = new RespuestaVO();
        response.setCode("200");
        try {
            String estadoFinalizado = "Finalizado";
            String nombreObjetoEstudLeccion = "estudiante_leccion";
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
            JsonNode objetoBaseFeed = objetoRequestJson.get("objeto");
            String idObjetoFeed = objetoBaseFeed.get("id").asText();
            String ultimoNodo = objetoRequestJson.get("ultimoNodo").asText();
            DataToken dataToken = new DataToken(prefijo, idPublico, versionApp, urlWrite, urlRead);
            String tokenEncript = generarTokenGetObjeto(dataToken);

            boolean mantenerSinTimeZone = true;
            if (objetoRequestJson.get("mantenerSinTimeZone") != null && !objetoRequestJson.get("mantenerSinTimeZone").isNull()) {
                mantenerSinTimeZone = objetoRequestJson.get("mantenerSinTimeZone").asBoolean();
            }

            // buscar feedbackEstudiante:
            String bodyFeedBackEstudiante = getBodyFeedBackEstudiante(idObjetoFeed);
            JsonNode respLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyFeedBackEstudiante, "feedback_estudiante");
            if (respLst != null && respLst.size() > 0) {
                JsonNode miFeedbackEstudiante = respLst.get(0);
                if (miFeedbackEstudiante.get("estudiante_leccion") != null && !miFeedbackEstudiante.get("estudiante_leccion").isNull()) {
                    String idEstudLeccion = miFeedbackEstudiante.get("estudiante_leccion").get("id").asText();
                    // poner estado en finalizado:
                    String bodyActualizarEstudLeccion = getBodyEstadObjeto(idEstudLeccion, nombreObjetoEstudLeccion, estadoFinalizado);
                    integrationService.actualizarObjetoApi(dataToken, bodyActualizarEstudLeccion, nombreObjetoEstudLeccion, true);

                    // insertar wf:
                    String bodyInsertWf = armarObjetoWf(nombreObjetoEstudLeccion, idEstudLeccion, estadoFinalizado, mantenerSinTimeZone);
                    integrationService.insertarObjetoApi(dataToken, bodyInsertWf, "estudiante_curso");
                } else {
                    errList.addError("10.1", "error ws ActualizarEstadoWFEstudianteLeccion", "error al obtener estudiante_leccion de feedback_estudiante: " + idObjetoFeed);
                    throw errList;
                }
            } else {
                errList.addError("10.1", "error ws ActualizarEstadoWFEstudianteLeccion", "no fue posible obtener  feedback_estudiante: " + idObjetoFeed);
                throw errList;
            }
            return response;
        } catch (Exception e) {
            if (((MyErrorListException) e).getErrors() != null && ((MyErrorListException) e).getErrors().size() > 0) {
                errList.addErrorList(((MyErrorListException) e).getErrors());
            } else {
                errList.addError("10.1", "error ws calcularAvanceCursoFeedBack", "no fue posible calcular avance " + e.getMessage());
            }
            throw errList;
        }
    }

    @Override
    public RespuestaVO calcularAvanceRespuestaFeedBack(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        RespuestaVO response = new RespuestaVO();
        response.setCode("200");
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
            JsonNode objetoBase = objetoRequestJson.get("objeto");
            String idObjetoFeed1 = objetoBase.get("id").asText();
            String nombreObjetoFeed1 = objetoRequestJson.get("nombreObjeto").asText();
            String ultimoNodo = objetoRequestJson.get("ultimoNodo").asText();
            DataToken dataToken = new DataToken(prefijo, idPublico, versionApp, urlWrite, urlRead);
            String tokenEncript = generarTokenGetObjeto(dataToken);

            // buscar feedback1:
            String bodyfeedback1 = getBodyFeedback1(idObjetoFeed1, nombreObjetoFeed1);
            JsonNode respLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyfeedback1, nombreObjetoFeed1);
            if (respLst != null && respLst.size() > 0) {
                JsonNode feedback1 = respLst.get(0);
                if (feedback1.get("feedback_estudiante") != null && !feedback1.get("feedback_estudiante").isNull() &&
                        feedback1.get("feedback_estudiante").get("estudiante_leccion") != null && !feedback1.get("feedback_estudiante").get("estudiante_leccion").isNull()) {
                    JsonNode estudiante = feedback1.get("feedback_estudiante").get("estudiante_leccion").get("estudiante");
                    JsonNode leccion = feedback1.get("feedback_estudiante").get("estudiante_leccion").get("leccion");
                    String idCurso = feedback1.get("feedback_estudiante").get("estudiante_leccion").get("leccion").get("modulo").get("curso").get("id").asText();

                    // buscar feedback1 que pertenezcan al estudiante del objeto feedback_estudiante.estudiante_leccion
                    JsonNode bodyReFeedback1EstudianteLst = getBodyReFeedback1Estudiante(estudiante.get("id").asText(), nombreObjetoFeed1);
                    List<JsonNode> reFeedback1EstudLst = integrationService.getListaObjetosApiPaginado(dataToken, tokenEncript, bodyReFeedback1EstudianteLst, nombreObjetoFeed1);

                    if (reFeedback1EstudLst != null && reFeedback1EstudLst.size() > 0) {
                        // hacer grupos por leccion y verificar si hay alguna con avance_calculado = true para contar ese avance:
                        List<String> idsLeccionesProcess = new ArrayList<>();
                        int contadorAvance = 0;
                        boolean marcarAvanceCalculado = false;
                        for (JsonNode reFeedback1Estud : reFeedback1EstudLst) {
                            String pIdLeccion = reFeedback1Estud.get("feedback_estudiante").get("estudiante_leccion").get("leccion").get("id").asText();
                            if (!verificarElementoEnListado(pIdLeccion, idsLeccionesProcess)) {
                                idsLeccionesProcess.add(pIdLeccion);
                                // filtrar reFeedback de la misma leccion:
                                boolean tieneAvanceCalculado = false;
                                boolean esLeccionEjecucion = false;
                                for (JsonNode reFeedGrupo : reFeedback1EstudLst) {
                                    JsonNode leccionGrupo = reFeedGrupo.get("feedback_estudiante").get("estudiante_leccion").get("leccion");
                                    if (leccionGrupo.get("id").asText().equals(pIdLeccion)) {
                                        // verificar si avance_calculado = true para contar avance en esa leccion:
                                        if (reFeedGrupo.get("avance_calculado") != null && reFeedGrupo.get("avance_calculado").asBoolean()) {
                                            contadorAvance++;
                                            tieneAvanceCalculado = true;
                                        }
                                        if (leccionGrupo.get("id").asText().equals(leccion.get("id").asText())) {
                                            esLeccionEjecucion = true;
                                        }
                                    }
                                }
                                /* analizar banderas para contabilizar avance: si no hay ninguna marcada como avance_calculado verificar que
                                 se trate de la misma leccion que se esta ejecutando en el momento para incluirla en el avance */
                                if (!tieneAvanceCalculado && esLeccionEjecucion) {
                                    contadorAvance++;
                                    marcarAvanceCalculado = true;
                                }
                            }
                        }
                        // obtener estudiante_curso:
                        String bodyEstudianteCurso = getBodyEstudianteCurso(estudiante.get("id").asText(), idCurso);
                        JsonNode respEstudCursoLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyEstudianteCurso, "estudiante_curso");
                        if (respEstudCursoLst != null && respEstudCursoLst.size() > 0 && respEstudCursoLst.get(0).get("curso") != null) {
                            JsonNode estudianteCurso = respEstudCursoLst.get(0);

                            // buscar lecciones de tipo evaluacion escrita del curso:
                            String bodyLeccionesCursoAvance = getBodyLeccionesCursoAvance(NOMBRE_TIPO_LECCION_FEEDBACK, idCurso);
                            JsonNode leccionesLst = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyLeccionesCursoAvance, "leccion");

                            Integer totalLecciones = leccionesLst.size();

                            Double avanceRefeed = Double.valueOf((float) (contadorAvance * 10) / totalLecciones);

                            Double avanceLeccion = (double) 0;
                            if (estudianteCurso.get("avance_leccion") != null) {
                                avanceLeccion = estudianteCurso.get("avance_leccion").asDouble();
                            }
                            Double avanceFeedBack = (double) 0;
                            if (estudianteCurso.get("avance_feed") != null) {
                                avanceFeedBack = estudianteCurso.get("avance_feed").asDouble();
                            }

                            Double avanceTotalTemp = formatearDecimales((avanceRefeed + avanceLeccion + avanceFeedBack), 1);
                            String avanceTotal = avanceTotalTemp + "%";
                            // actualizar estudinte_curso:
                            String bodyUpdateEstudCurso = getBodyActualizarAvance(estudianteCurso.get("id").asText(), avanceTotal, avanceRefeed, TIPO_AVANCE_REFEED);
                            integrationService.actualizarObjetoApi(dataToken, bodyUpdateEstudCurso, "estudiante_curso", true);

                            // actualizar feedback1:
                            if (marcarAvanceCalculado) {
                                String bodyUpdateRefeed = getBodyUpdateRefeed(idObjetoFeed1);
                                integrationService.actualizarObjetoApi(dataToken, bodyUpdateRefeed, "feedback1", true);
                            }
                        } else {
                            errList.addError("10.1", "error ws calcularAvanceRespuestaFeedBack", "no fue posible obtener estudiante_curso. estudiante: " + estudiante.get("id").asText());
                            throw errList;
                        }
                    }
                } else {
                    errList.addError("10.1", "error ws calcularAvanceRespuestaFeedBack", "no fue posible obtener relaciones de " + nombreObjetoFeed1 + " id: " + idObjetoFeed1);
                    throw errList;
                }
            } else {
                errList.addError("10.1", "error ws calcularAvanceRespuestaFeedBack", "no fue posible obtener " + nombreObjetoFeed1 + " id: " + idObjetoFeed1);
                throw errList;
            }
            return response;
        } catch (Exception e) {
            if (((MyErrorListException) e).getErrors() != null && ((MyErrorListException) e).getErrors().size() > 0) {
                errList.addErrorList(((MyErrorListException) e).getErrors());
            } else {
                errList.addError("10.1", "error ws calcularAvanceRespuestaFeedBack", "no fue posible calcular avance " + e.getMessage());
            }
            throw errList;
        }
    }

    /**
     * Este metodo aprueba las lecciones de un estudiante de forma automatica
     * pasados 14 dias desde que fueron asignadas al administrador   
     * @return estado de la accion 
    */
    @Override
    public boolean aprobarFeedAdminCron() {
        final String nombreObjetoFeed = "feedback_estudiante";
        try {
            List<CronObjeto> cronObjetoList = cronObjetoService.getAllCronObjeto();
            if (cronObjetoList != null && cronObjetoList.size() > 0) {
                for (CronObjeto cronObjeto : cronObjetoList) {
                    if (cronObjeto.getId().startsWith(KEY_CRON_ASIGNAR_LECCIONES)) {
                        try {
                            DataToken dataToken = new DataToken();
                            dataToken.setUrlApiRead(getUrlRead(cronObjeto.getPrefijo()));
                            dataToken.setUrlApiWrite(getUrlWrite(cronObjeto.getPrefijo()));
                            dataToken.setIdPublico(cronObjeto.getIdPublicoUsuario());
                            dataToken.setVersionAplicacion(cronObjeto.getVersion());
                            dataToken.setPrefijo(cronObjeto.getPrefijo());
                            String tokenEncript = generarTokenGetObjeto(dataToken);

                            // establecer limite de fechas de hace un mes
                            Date fechaActual = new Date();
                            Date fechaActualSinHora = getDateSinHoraByDate(fechaActual);
                            Date fechaFin = sumarDiasAFecha(fechaActual, -13);
                            Date fechaInicio = sumarDiasAFecha(fechaActual, -15);
                            String fechaFinCad = formatDateSinHora(fechaFin);
                            String fechaInicioCad = formatDateSinHora(fechaInicio);

                            JsonNode bodyFeedEstudianteLstCron = getBodyFeedEstudianteLstCron(fechaInicioCad, fechaFinCad, nombreObjetoFeed, true, true);
                            List<JsonNode> feedEstudianteLst = integrationService.getListaObjetosApiPaginado(dataToken, tokenEncript, bodyFeedEstudianteLstCron, "feedback_estudiante");
                            if (feedEstudianteLst != null && feedEstudianteLst.size() > 0) {
                                // buscar Admin:
                                String bodyAdminAmauta = getBodyAdminAmautaAE();
                                JsonNode respAdminAmauta = integrationService.getListaObjetosApi(dataToken, tokenEncript, bodyAdminAmauta, "usuairo");
                                JsonNode usuarioAdmin = respAdminAmauta != null && respAdminAmauta.size() > 0 ? respAdminAmauta.get(0) : null;

                                // obtener nombre del primer estado:
//                                String bodyWfFeedEstud = getBodyWfFeedEstudPrimerEstado(feedEstudianteLst.get(0).get("id").asText());
//                                JsonNode respWfFeedEstud = amautaRepositry.getListaObjetosApi(dataToken, tokenEncript, bodyWfFeedEstud, "wf_feedback_estudiante");
                                if (usuarioAdmin != null) {
                                    //String nombrePrimerNodo = respWfFeedEstud.get(0).get("nodo").asText();
                                    List<String> idsEstudLeccionProcess = new ArrayList<>();
                                    for (JsonNode feedEstudiante : feedEstudianteLst) {
                                        String fechaCreacionCad = feedEstudiante.get("fecha_creacion").asText();
                                        Date fechaCreacionSinHora;
                                        String fechaActualWF = "";
                                        // obtener fecha de creacion sin hora y en formato de hora local:
                                        if (!cronObjeto.isMantenerSinTimeZone()) {
                                            // convertir de utc a hora local:
                                            fechaActualWF = formatDateConSinTimeZoneSave(new Date(), true);
                                            Date fechaTemp = sumarRestarHorasByGMT(fechaCreacionCad, cronObjeto.getGmt());
                                            fechaCreacionSinHora = getDateSinHoraByDate(fechaTemp);
                                        } else {
                                            fechaCreacionSinHora = getFechaByCadenaSinHora(fechaCreacionCad);
                                            fechaActualWF = formatDateConSinTimeZoneSave(new Date(), false);
                                        }
 
                                        JsonNode bodyFeedbackForm = getBodyInsertFeddbackFormAprobed(cronObjeto.getVersion(),feedEstudiante.get("id").asInt(),usuarioAdmin.get("id").asInt(), usuarioAdmin.get("username").asText(), cronObjeto.getPrefijo());
                                        JsonNode bodyWFFeedback= getBodyInsertWFFeddbackEstudAprobed(feedEstudiante.get("id").asInt(),usuarioAdmin.get("id").asInt(), usuarioAdmin.get("username").asText(), fechaActualWF);
                                        JsonNode bodyUpdateFeedback = getBodyUpdateStateFeedbackStudent(feedEstudiante.get("id").asInt(), fechaActualWF);
                        
                                        Integer diferenciaDias = getDiasDiferencia(fechaCreacionSinHora, fechaActualSinHora);
                                        // ** SI HAN PASADO 14 DIAS DESDE LA ASIGNACION AL ADMIN **
                                        if (diferenciaDias.equals(14) && feedEstudiante.get("estudiante_leccion") != null &&
                                                !feedEstudiante.get("estudiante_leccion").isNull()) {
                                            integrationService.insertarObjetoApi(dataToken, bodyFeedbackForm.toString(), "feedback_formulario");
                                            integrationService.insertarObjetoApi(dataToken, bodyWFFeedback.toString(), "wf_feedback_estudiante");
                                            integrationService.actualizarObjetoApi(dataToken, bodyUpdateFeedback.toString(), "feedback_estudiante", false);
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            logger.info("====ERROR ejecución objeto cron aprobar lecciones estudiante : " + cronObjeto.getId() + " EX:" + e.getMessage());
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.info("====ERROR ejecución aprobar lecciones estudiante cron: " + e.getMessage());
        }
        return true;
    }
}
