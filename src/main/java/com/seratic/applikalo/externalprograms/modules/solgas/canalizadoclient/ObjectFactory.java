//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.25 a las 10:32:01 AM COT 
//


package com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ZsdfIngresoMediciones }
     * 
     */
    public ZsdfIngresoMediciones createZsdfIngresoMediciones() {
        return new ZsdfIngresoMediciones();
    }

    /**
     * Create an instance of {@link ZsdttIngmedicion }
     * 
     */
    public ZsdttIngmedicion createZsdttIngmedicion() {
        return new ZsdttIngmedicion();
    }

    /**
     * Create an instance of {@link ZsdfIngresoMedicionesResponse }
     * 
     */
    public ZsdfIngresoMedicionesResponse createZsdfIngresoMedicionesResponse() {
        return new ZsdfIngresoMedicionesResponse();
    }

    /**
     * Create an instance of {@link ZsdttReturnCanalizadoWs }
     * 
     */
    public ZsdttReturnCanalizadoWs createZsdttReturnCanalizadoWs() {
        return new ZsdttReturnCanalizadoWs();
    }

    /**
     * Create an instance of {@link ZsdfCorteReconexionEquipo }
     * 
     */
    public ZsdfCorteReconexionEquipo createZsdfCorteReconexionEquipo() {
        return new ZsdfCorteReconexionEquipo();
    }

    /**
     * Create an instance of {@link ZsdttCorecequipo }
     * 
     */
    public ZsdttCorecequipo createZsdttCorecequipo() {
        return new ZsdttCorecequipo();
    }

    /**
     * Create an instance of {@link ZsdfCorteReconexionEquipoResponse }
     * 
     */
    public ZsdfCorteReconexionEquipoResponse createZsdfCorteReconexionEquipoResponse() {
        return new ZsdfCorteReconexionEquipoResponse();
    }

    /**
     * Create an instance of {@link ZsdsReturnCanalizadoWs }
     * 
     */
    public ZsdsReturnCanalizadoWs createZsdsReturnCanalizadoWs() {
        return new ZsdsReturnCanalizadoWs();
    }

    /**
     * Create an instance of {@link ZsdsIngmedicion }
     * 
     */
    public ZsdsIngmedicion createZsdsIngmedicion() {
        return new ZsdsIngmedicion();
    }

    /**
     * Create an instance of {@link ZsdsCorecequipo }
     * 
     */
    public ZsdsCorecequipo createZsdsCorecequipo() {
        return new ZsdsCorecequipo();
    }

}
