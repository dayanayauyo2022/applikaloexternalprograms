package com.seratic.applikalo.externalprograms.modules.activosmineros.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.POJONode;
import com.seratic.applikalo.externalprograms.modules.activosmineros.dto.DatosNotificacion;
import com.seratic.applikalo.externalprograms.modules.activosmineros.dto.NotificacionReporteDTO;
import com.seratic.applikalo.externalprograms.modules.activosmineros.dto.PasivoDTO;
import com.seratic.applikalo.externalprograms.modules.activosmineros.repositorio.ObtenerDataImp;
import com.seratic.applikalo.externalprograms.modules.activosmineros.utils.EnviarMensaje;
import com.seratic.applikalo.externalprograms.modules.entities.Componente;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.apiGo.Where;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static com.seratic.applikalo.externalprograms.modules.activosmineros.utils.ConstantesLlave.*;

@Service
public class ArmarNotificacionImp implements ArmarNotificacion{

    private static final Log LOG = LogFactory.getLog(ArmarNotificacionImp.class);
    @Value(value = "${activos-mineros.envio-notificacion.schema}")
    private String schema;
    @Value(value = "${activos-mineros.envio-notificacion.id-publico}")
    private String idPublico;
    @Value( value = "#{${topicsMap}}")
    private Map<String, String> topicsMap;
    @Value(value = "${activos-mineros.envio-notificacion.idversionApp}")
    private Integer idVersionApp;
    @Value(value = "${activos-mineros.envio-notificacion.proyecto-estado-id}")
    private Integer idEstadoProyecto;
    @Value(value = "${activos-mineros.envio-notificacion.pasivo-estado-id}")
    private Integer idEstadoPasivo;
    @Value(value = "${activos-mineros.envio-notificacion.nombre-nodo-notificacion}")
    private String nombreNodoNotificacion;
    @Value(value = "${activos-mineros.envio-notificacion.idFormulario-proyectos}")
    private Integer idFormularioProyecto;
    @Autowired
    private EnviarMensaje enviarMensaje;
    @Autowired
    private ObtenerDataImp dataRepository;
    private static final DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
    private static final DateTimeFormatter outFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final String KEYEXCUTE = "excute";
    private ObjectMapper mapper = new ObjectMapper();

    /*
    Metodo encargado de notificar proyectos no supervisados
     */
    @Override
    public void armarNotificacionProyectosNoSupervisados() {
        List<JsonNode> verificacionProyectos = dataRepository.obtenerVerificacionProyecto();
        List<JsonNode> proyectosSinVerificacion = new ArrayList<>(0);
        List<ObjectNode> proyectosVerificados = new ArrayList<>(0);
        dataRepository.obtenerProyectos().stream().forEach(proyecto -> {
            try {
                if (!Objects.equals(proyecto.get(KEYESTADOPROYECTO), "{}") && !idEstadoProyecto.equals(proyecto.get(KEYESTADOPROYECTO).get("id").asInt())) {
                    List<JsonNode> proyectosEsSupervisado = obtenerSupervisionesProyecto(proyecto, verificacionProyectos);
                    if (proyectosEsSupervisado.isEmpty()) {
                        proyectosSinVerificacion.add(proyecto);
                    } else {
                        asignarSupervisionesProyecto(proyectosVerificados, proyectosEsSupervisado, mapper.convertValue(proyecto, ObjectNode.class));
                    }
                }
            } catch (Exception e) {
                LOG.error( " ArmarNotificacionImp  ->  armarNotificacionProyectosNoSupervisados  Fallo: no fue posible validar el proyecto con id, " + proyecto, e);
            }
        });
        validarFechaSupervicionProyectoNuevo(proyectosSinVerificacion);
        validarFechaSupervisionProyectos(proyectosVerificados);
    }

    private List<JsonNode> obtenerSupervisionesProyecto(JsonNode proyecto, List<JsonNode> supervisiones) {
        List<JsonNode> supervisionesProyecto = new ArrayList<>(0);
        supervisiones.stream().forEach(supervision -> {
            if (Objects.nonNull(supervision.get("proyecto").get("id")) && Objects.equals(supervision.get("proyecto").get("id").asInt(), proyecto.get("id").asInt())) {
                supervisionesProyecto.add(supervision);
            }
        });
        return supervisionesProyecto;
    }

    private void asignarSupervisionesProyecto(List<ObjectNode> proyectosVerificados, List<JsonNode> verificacionProyecto, ObjectNode proyectoYSupervision) {
        List<String> fechasSupervision = new ArrayList<>(0);
        if (!verificacionProyecto.isEmpty() && verificacionProyecto.size() > 1) {
            verificacionProyecto.stream().forEach(supervision -> fechasSupervision.add(supervision.get("fecha").asText()));
            proyectoYSupervision.set(KEYFECHASUPERVISION, mapper.valueToTree(fechasSupervision));
        } else {
            proyectoYSupervision.put(KEYFECHASUPERVISION, verificacionProyecto.get(0).get("fecha").asText());
        }
        proyectosVerificados.add(proyectoYSupervision);
    }

    @Override
    public void validarFechaSupervicionProyectoNuevo(List<JsonNode> protectosNoSupervicionNuevos) {
        LocalDate fechaActual = LocalDate.now();
        if (protectosNoSupervicionNuevos.isEmpty()) {
            LOG.info("No hay proyectos nuevos por supervisar.");
        } else {
            protectosNoSupervicionNuevos.stream().forEach(proyectoNoSupervision -> {
                if (ChronoUnit.DAYS.between(LocalDate.parse(proyectoNoSupervision.get("fecha_creacion").asText(), inputFormat), fechaActual) >= 2) {
                    DatosNotificacion datosNotificacion = new DatosNotificacion();
                    datosNotificacion.setNombreProyecto(proyectoNoSupervision.get(KEYNOMBRE).asText());
                    enviarNotificacionKafka(proyectoNoSupervision, mapper.convertValue(aramarPlantillaNotifcacionWebyCorreo(datosNotificacion, 3), ObjectNode.class), null);
                }
            });
        }
    }

    @Override
    public void validarFechaSupervisionProyectos(List<ObjectNode> proyectosNoSupervision) {
        LocalDate fechaActual = LocalDate.now();
        if (proyectosNoSupervision.isEmpty()) {
            LOG.info("No hay proyectos por supervisar.");
        } else {
            proyectosNoSupervision.stream().forEach(proyectoNoSupervision -> {
                if (proyectoNoSupervision.get(KEYFECHASUPERVISION).isArray()){
                    JsonNode fechaSupervisionProyecto = proyectoNoSupervision.get(KEYFECHASUPERVISION);
                    AtomicReference<LocalDate> oldDate = new AtomicReference<>(LocalDate.ofEpochDay(365));
                    validateMostCurrentDate(fechaSupervisionProyecto, oldDate);
                    if (ChronoUnit.DAYS.between(oldDate.get(), fechaActual) > 1) {
                        DatosNotificacion datosNotificacion = new DatosNotificacion();
                        datosNotificacion.setNombreProyecto(proyectoNoSupervision.get(KEYNOMBRE).asText());
                        enviarNotificacionKafka(mapper.convertValue(proyectoNoSupervision, JsonNode.class), aramarPlantillaNotifcacionWebyCorreo(datosNotificacion, 3), null);
                    }
                } else {
                    if (ChronoUnit.DAYS.between(LocalDate.parse(proyectoNoSupervision.get(KEYFECHASUPERVISION).asText(), inputFormat), fechaActual) > 1) {
                        DatosNotificacion datosNotificacion = new DatosNotificacion();
                        datosNotificacion.setNombreProyecto(proyectoNoSupervision.get(KEYNOMBRE).asText());
                        enviarNotificacionKafka(mapper.convertValue(proyectoNoSupervision, JsonNode.class), aramarPlantillaNotifcacionWebyCorreo(datosNotificacion, 3), null);
                    }
                }
            });
        }
    }

    private void validateMostCurrentDate(JsonNode fechaSupervisionProyecto, AtomicReference<LocalDate> oldFecha) {
        fechaSupervisionProyecto.iterator().forEachRemaining(fecha -> {
            if (LocalDate.parse(fecha.asText(), inputFormat).isAfter(oldFecha.get())) {
                oldFecha.set(LocalDate.parse(fecha.asText(), inputFormat));
            }
        });
    }

    // Se consulta el proyecto creado, si este presenta campos vacios en su creacion se notifica que campos no se diligenciaron
    @Override
    public void armarNotificacionProyectoCamposVacios(Where filtro, String idPublicoOwner, String prefijo) {
        ObjectNode proyecto = dataRepository.obtenerProyectoPorKey(filtro, idPublicoOwner);
        List<String> camposVacios = validarCamposVaciosDelProyecto(proyecto);
        DatosNotificacion dataNotificacion = new DatosNotificacion();
        dataNotificacion.setNombreProyecto(proyecto.get(KEYNOMBRE).asText());
        dataNotificacion.setCampos(camposVacios);
        if (camposVacios.isEmpty()) {
            LOG.info(" Proyecto OK  " + proyecto.get("id"));
        } else {
            enviarNotificacionKafka(mapper.convertValue(proyecto, JsonNode.class), aramarPlantillaNotifcacionWebyCorreo(dataNotificacion, 2), prefijo);
        }
    }

    private List<String> validarCamposVaciosDelProyecto(ObjectNode proyecto){
        List<String> plantillaFormularioCamposVacios = new ArrayList<>(0);
        dataRepository.obtenerComponentePorId(idFormularioProyecto).getComponentes().stream().forEach(componente -> {
            try {
                if(componente.getTipoComponente().getId() != 1001){
                    String textoCampoKey = getKeyTipoComponente(componente);
                    //Validar si es un campo relacion
                    if (textoCampoKey.contains(".")) {
                        campoRelacion(proyecto, plantillaFormularioCamposVacios, componente, textoCampoKey);
                    } else {
                        campoRaiz(proyecto, plantillaFormularioCamposVacios, componente, textoCampoKey);
                    }
                }
            } catch (Exception e) {
                LOG.error(" Fallo buscando campo del componente " + componente.getId() + ", los atributos no se lograron leer " + componente.getAtributos() + ", id tipo componente " + componente.getTipoComponente().getId(), e);
            }
        });
        return plantillaFormularioCamposVacios;
    }

    private void campoRaiz(ObjectNode proyecto, List<String> plantillaFormularioCamposVacios, Componente componente, String textoCampoKey) {
        if (Objects.isNull(proyecto.get(textoCampoKey)) || Objects.equals(proyecto.get(textoCampoKey), "null") || proyecto.get(textoCampoKey).isArray()) {
            if (proyecto.get(textoCampoKey).isEmpty()){
                plantillaFormularioCamposVacios.add(componente.getNombre());
            }
        } else {
            campoObject(proyecto, plantillaFormularioCamposVacios, componente, textoCampoKey);
        }
    }

    private void campoObject(ObjectNode proyecto, List<String> plantillaFormularioCamposVacios, Componente componente, String textoCampoKey) {
        if (proyecto.get(textoCampoKey) instanceof ObjectNode) {
            if (Objects.equals(proyecto.get(textoCampoKey) ,"{}")) {
                plantillaFormularioCamposVacios.add(componente.getNombre());
            }
        } else if(proyecto.get(textoCampoKey) instanceof POJONode) {
            if (proyecto.get(textoCampoKey).size() == 0){
                plantillaFormularioCamposVacios.add(componente.getNombre());
            }
        } else {
            if (Objects.equals(proyecto.get(textoCampoKey).asText(), "") || Objects.equals(proyecto.get(textoCampoKey).asText(), "null")) {
                plantillaFormularioCamposVacios.add(componente.getNombre());
            }
        }
    }

    private String getKeyTipoComponente(Componente componente) throws JsonProcessingException {
        String textoCampoKey = null;
        if (componente.getTipoComponente().getId().equals(TIPO_COMPONENTE_TABLA)){
           textoCampoKey = mapper.readTree(componente.getAtributos()).get("nombre_campo").asText();
        } else {
            textoCampoKey = mapper.readTree(componente.getAtributos()).get("textoCampo").asText();
        }
        return textoCampoKey;
    }

    private void campoRelacion(ObjectNode proyecto, List<String> plantillaFormularioCamposVacios, Componente componente, String textoCampoKey) {
        String[] textoCampoRealcion = textoCampoKey.split("\\.");
        if (Objects.isNull(proyecto.get(textoCampoRealcion[0])) || proyecto.get(textoCampoRealcion[0]).isEmpty()) {
            plantillaFormularioCamposVacios.add(componente.getNombre());
        } else {
            if (proyecto.get(textoCampoRealcion[0]).isArray() && proyecto.get(textoCampoRealcion[0]).size() > 0) {
                plantillaFormularioCamposVacios.add(componente.getNombre());
            }
        }
    }

    @Override
    public void armarDataNotificacionProyectosFueraDePlazo(List<JsonNode> proyectos, LocalDate currentDate) {
        proyectos.stream().forEach(proyecto -> {
            DatosNotificacion datosNotificacion = new DatosNotificacion();
            datosNotificacion.setNombreProyecto(proyecto.get(KEYNOMBRE).asText());
            datosNotificacion.setFechaFueraDePlazo(proyecto.get(KEYFECHAFUERADEPLAZO).asText());
            List<PasivoDTO> pasivos = Arrays.asList(mapper.convertValue(proyecto.get("proyecto_pasivos"), PasivoDTO[].class).clone());
            if (pasivos.isEmpty()) {
                LOG.info("El proyecto no cuenta con pasivos relacionados.");
            } else {
                pasivos.stream().forEach(pasivo -> {
                    if (Objects.nonNull(pasivo.getFechaplazopasivo())) {
                        List<PasivoDTO> pasaivosProyecto = new ArrayList<>();
                        LocalDate fechaSinFormatoPasivo = LocalDate.parse(pasivo.getFechaplazopasivo(), inputFormat);
                        if (ChronoUnit.DAYS.between(currentDate, LocalDate.parse(outFormat.format(fechaSinFormatoPasivo))) >= 0 && Objects.equals(pasivo.getId(), idEstadoPasivo)) {
                            pasaivosProyecto.add(pasivo);
                        }
                        datosNotificacion.setPasivos(pasaivosProyecto);
                    }
                });
            }
            ObjectNode cuerpoNotificacion = aramarPlantillaNotifcacionWebyCorreo(datosNotificacion, 1);
            //Notifcar ejecucion nodo notificacion en emaReportes
            enviarNotificacionKafka(proyecto, cuerpoNotificacion, null);
        });
    }

    @Override
    public void armarNoitificacionProyectosCamposVacios() {
         List<JsonNode> dataProyectos = dataRepository.obtenerProyectos();
        dataProyectos.stream().forEach(proyecto -> {
            ObjectNode proyectoActual = mapper.convertValue(proyecto, ObjectNode.class);
            List<String> camposProyecto = validarCamposVaciosDelProyecto(proyectoActual);
            DatosNotificacion dataNotificacion = new DatosNotificacion();
            dataNotificacion.setNombreProyecto(proyecto.get(KEYNOMBRE).asText());
            dataNotificacion.setCampos(camposProyecto);
            if (camposProyecto.isEmpty()) {
                LOG.info(" Proyecto OK  " + proyecto.get("id"));
            } else {
                enviarNotificacionKafka(mapper.convertValue(proyecto, JsonNode.class), aramarPlantillaNotifcacionWebyCorreo(dataNotificacion, 2), null);
            }
        });
    }

    /*
        Metodo en cargado de armar el cuerpo del correo, se obtiene el componenteNodo y se actualiza su campo
        "atributos" con la configuracion del nuevo cuerpo, se persiste.
         */
    @Override
    public ObjectNode aramarPlantillaNotifcacionWebyCorreo(DatosNotificacion datosNotificacion, int caso) {
        ObjectNode retorno = mapper.createObjectNode();
        Componente componenteNodo = dataRepository.obtenerComponenteNode(nombreNodoNotificacion, idVersionApp);
        StringBuilder cuerpoNotificacion = new StringBuilder();
        retorno.put(KEYEXCUTE, false);
        if (Objects.nonNull(componenteNodo.getAtributos())) {
            try {
                switch (caso) {
                    case 1:
                        cuerpoNotificacion.append("<p>");
                        cuerpoNotificacion.append("Estimado Usuario");
                        cuerpoNotificacion.append("<br>");
                        cuerpoNotificacion.append("Se informa que el proyecto <strong>" + datosNotificacion.getNombreProyecto() + "</strong>, cuenta con fecha vigente de termino de proyecto el <strong>" + outFormat.format(inputFormat.parse(datosNotificacion.getFechaFueraDePlazo())) + "</strong>, actualmente está <strong>Fuera de Plazo.</strong>");
                        cuerpoNotificacion.append("</p>");
                        cuerpoNotificacion.append("<br>");
                        break;
                    case 2:
                        cuerpoNotificacion.append("<p>");
                        cuerpoNotificacion.append("Estimado usuario, se informa que el Proyecto <strong>" + datosNotificacion.getNombreProyecto() + "</strong> tiene los siguientes campos vacíos:");
                        cuerpoNotificacion.append("</p>");
                        cuerpoNotificacion.append("<br>");
                        cuerpoNotificacion.append("<ul>");
                        datosNotificacion.getCampos().stream().forEach(campo -> {
                            cuerpoNotificacion.append("<li>");
                            cuerpoNotificacion.append(campo);
                            cuerpoNotificacion.append("</li>");
                        });
                        cuerpoNotificacion.append("</ul>");
                        break;
                    case 3:
                        cuerpoNotificacion.append("<p>");
                        cuerpoNotificacion.append("Estimado Supervisor");
                        cuerpoNotificacion.append("<br>");
                        cuerpoNotificacion.append("Se informa que el proyecto, <strong>" + datosNotificacion.getNombreProyecto() + "</strong> no cuenta con supervisión para el día " + outFormat.format(LocalDate.now().minusDays(1)));
                        cuerpoNotificacion.append("<br>");
                        cuerpoNotificacion.append("Saludos.");
                        cuerpoNotificacion.append("</p>");
                        cuerpoNotificacion.append("<br>");
                        break;
                    default:
                        break;
                }
                ObjectNode atributosPersistir = procesarAtributosComponeteNodo(componenteNodo, cuerpoNotificacion, caso, datosNotificacion.getNombreProyecto());
                Boolean response = dataRepository.actualizarAtributosComponente(atributosPersistir);
                if (Boolean.TRUE.equals(response)) {
                    retorno.putPOJO(KEYATRIBUTOS, atributosPersistir.get("valor"));
                    retorno.put(KEYEXCUTE, true);
                }
            } catch (Exception e) {
                LOG.error(" Fallo armando plantilla de la notificacion, idComponente: " + componenteNodo.getId(), e);
                retorno.put(KEYEXCUTE, false);
            }
            return retorno;
        }
        return retorno;
    }

    @Override
    public ObjectNode procesarAtributosComponeteNodo(Componente componenteNodo, StringBuilder cuerpoNotificacion, int caso, String nombreProyecto) {
        ObjectNode retorno = mapper.createObjectNode();
        try {
            ObjectNode atributosBase = mapper.readValue(componenteNodo.getAtributos(), ObjectNode.class);
            String asuntoCorreo = "titulo";
            switch (caso){
                case 1:
                    atributosBase.put(asuntoCorreo, "Notificación Fecha vigente termino de proyecto: " + nombreProyecto);
                    break;
                case 2:
                    atributosBase.put(asuntoCorreo, "Notificación de campos vacíos del Proyecto: " + nombreProyecto);
                    break;
                case 3:
                    atributosBase.put(asuntoCorreo, "Notificación proyecto No Supervisado: " + nombreProyecto);
                    break;
                default:
            }
            if (atributosBase.isObject()) {
                ObjectNode notificacionCorreo = mapper.readValue(atributosBase.get("notificacionesPorCorreo").textValue(), ObjectNode.class);
                ObjectNode notificacionWeb = mapper.readValue(atributosBase.get("notificacionesPush").textValue(), ObjectNode.class);
                notificacionCorreo.put("texto_ingreso_plantilla_correo", cuerpoNotificacion.toString());
                notificacionWeb.put("texto_plantilla_ingreso", cuerpoNotificacion.toString());
                atributosBase.put("notificacionesPorCorreo", mapper.writeValueAsString(notificacionCorreo));
                atributosBase.put("notificacionesPush", mapper.writeValueAsString(notificacionWeb));
                componenteNodo.setAtributos(mapper.writeValueAsString(atributosBase));
                retorno.putPOJO("id", componenteNodo.getId());
                retorno.put(KEYATRIBUTOS, "update");
                retorno.putPOJO("valor", atributosBase);
            }

        } catch (Exception e) {
            LOG.error(" Fallo procesando 'atributos' del componente, idComponente: " + componenteNodo.getId(), e);
        }
        return retorno;
    }

    @Override
    public void enviarNotificacionKafka(JsonNode proyecto, ObjectNode atributosNotificacion, String prefijo) {
        if (atributosNotificacion.get(KEYEXCUTE).asBoolean()) {
            try {
                NotificacionReporteDTO notificacionObj = new NotificacionReporteDTO();
                List<JsonNode> jsonNodeList = new ArrayList<>();
                ObjectNode campoNotificacion = mapper.createObjectNode();
                campoNotificacion.putPOJO(KEYATRIBUTOS, atributosNotificacion.get(KEYATRIBUTOS));
                jsonNodeList.add(campoNotificacion);
                notificacionObj.setCompsNotificacion(jsonNodeList);
                notificacionObj.setCamposObjeto(mapper.convertValue(proyecto, JsonNode.class));
                notificacionObj.setVersionApp(String.valueOf(idVersionApp));
                notificacionObj.setAmbiente(schema.split("_")[0]);
                notificacionObj.setIdPublicoOwner(idPublico);
                if (!proyecto.get("usuario").isEmpty()) {
                    notificacionObj.setIdPublicoUsuario(proyecto.get("usuario").get("id_publico").asText());
                } else {
                    notificacionObj.setIdPublicoUsuario(null);
                }
                notificacionObj.setObjetoBase(mapper.writeValueAsString(proyecto));
                notificacionObj.setIdObjetoBase(String.valueOf(proyecto.get("id")));
                notificacionObj.setMantenerSinTimeZone(true);
                if (Objects.nonNull(prefijo)) {
                    enviarMensaje.sendMessageNotificaion(topicsMap.get(prefijo + "_"), mapper.writeValueAsString(notificacionObj));
                } else {
                    enviarMensaje.sendMessageNotificaion(topicsMap.get(schema), mapper.writeValueAsString(notificacionObj));
                }
            } catch (Exception e) {
                LOG.error(" Fallo enviando mensaje ", e);
            }
        }
    }
}
