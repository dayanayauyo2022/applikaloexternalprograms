package com.seratic.applikalo.externalprograms.modules.activosmineros.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProyectoDTO {

    private Integer id;
    private String nombre;
    @JsonProperty(value = "id_publico")
    private String idPublico;
    @JsonProperty(value = "proyecto_pasivos")
    private List<PasivoDTO> proyectoPasivos;
    private String fechaplazoproyecto;
    @JsonProperty(value = "estado_proyecto")
    private EstadoObj estadoProyecto;
    private EstadoObj usuario;

    public ProyectoDTO() {
        // default implementation ignored
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdPublico() {
        return idPublico;
    }

    public void setIdPublico(String idPublico) {
        this.idPublico = idPublico;
    }

    public List<PasivoDTO> getProyectoPasivos() {
        return proyectoPasivos;
    }

    public void setProyectoPasivos(List<PasivoDTO> proyectoPasivos) {
        this.proyectoPasivos = proyectoPasivos;
    }

    public String getFechaplazoproyecto() {
        return fechaplazoproyecto;
    }

    public void setFechaplazoproyecto(String fechaplazoproyecto) {
        this.fechaplazoproyecto = fechaplazoproyecto;
    }

    public EstadoObj getEstadoProyecto() {
        return estadoProyecto;
    }

    public void setEstadoProyecto(EstadoObj estadoProyecto) {
        this.estadoProyecto = estadoProyecto;
    }

    public EstadoObj getUsuario() {
        return usuario;
    }

    public void setUsuario(EstadoObj usuario) {
        this.usuario = usuario;
    }
}
