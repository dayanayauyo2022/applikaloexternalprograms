package com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.VOs;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public class ObjetoConAtributosVO {
    private JsonNode objeto;
    private List<JsonNode> listaCampos;

    public ObjetoConAtributosVO(JsonNode objeto, List<JsonNode> listaCampos) {
        this.objeto = objeto;
        this.listaCampos = listaCampos;
    }

    public JsonNode getObjeto() {
        return objeto;
    }

    public void setObjeto(JsonNode objeto) {
        this.objeto = objeto;
    }

    public List<JsonNode> getListaCampos() {
        return listaCampos;
    }

    public void setListaCampos(List<JsonNode> listaCampos) {
        this.listaCampos = listaCampos;
    }
}
