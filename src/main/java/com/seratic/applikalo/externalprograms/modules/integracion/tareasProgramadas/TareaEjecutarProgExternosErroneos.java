package com.seratic.applikalo.externalprograms.modules.integracion.tareasProgramadas;

import com.seratic.applikalo.externalprograms.modules.integracion.IntegrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TareaEjecutarProgExternosErroneos {
    private static final Logger logger = LoggerFactory.getLogger(TareaEjecutarProgExternosErroneos.class);

    @Autowired
    IntegrationService integrationService;

    // @Scheduled(cron = "${cron.ejecutar.prog.erroneos}", zone ="America/Lima")
    public void ejecutarProgExternosErroneos() {
        try {
            integrationService.ejecutarProgExternosErroneos();
        } catch (Exception e) {
            logger.info("====ERROR INESPERADO CRON EJECUTAR PROG EXTERNOS ERRONEOS ===== posible causa: "+e.getMessage());
        }
    }
}
