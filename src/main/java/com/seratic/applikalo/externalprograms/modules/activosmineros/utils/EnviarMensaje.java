package com.seratic.applikalo.externalprograms.modules.activosmineros.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component()
public class EnviarMensaje {

    private static final Log LOG = LogFactory.getLog(EnviarMensaje.class);
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessageNotificaion(String topic, String message) {
        try {

            ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topic, message);
            LOG.info("  Mensaje enviado al Kafka-topic:  " + topic );
            future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
                @Override
                public void onSuccess(SendResult<String, String> result) {
                    LOG.info("Sent message=[" + message + "] with offset=[" + result.getRecordMetadata().offset() + "]");
                }
                @Override
                public void onFailure(Throwable ex) {
                    LOG.info("Unable to send message=[" + message + "] due to : " + ex.getMessage());
                }
            });
        } catch (Exception e) {
            LOG.error("error al enviar mensaje de nodo notifiacion", e);
        }
    }
}
