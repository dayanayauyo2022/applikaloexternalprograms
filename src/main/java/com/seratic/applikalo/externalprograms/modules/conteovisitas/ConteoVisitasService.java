package com.seratic.applikalo.externalprograms.modules.conteovisitas;

import com.seratic.applikalo.externalprograms.modules.integracion.VOs.RespuestaVO;

public interface ConteoVisitasService {
    RespuestaVO contarVisitas(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead);
}
