package com.seratic.applikalo.externalprograms.modules.integracion.entities.errors;

import java.util.ArrayList;
import java.util.List;

/*
 * Clase utilizada para lamacenar un listado de errores que son retornados al controlador con la finalidad de informar al cliente lo que pudo haber ocasionado el error
 */
public class MyErrorListException extends RuntimeException {
    List<MyError> errors;

    public MyErrorListException() {
        this.errors = new ArrayList<>();
    }

    public List<MyError> getErrors() {
        return errors;
    }

    public void addError(String code, String campo, String msj) {
        MyError error = new MyError(code, campo, msj);
        this.errors.add(error);
    }

    public void addErrorList(List<MyError> listErrors) {
        this.errors.addAll(listErrors);
    }
}
