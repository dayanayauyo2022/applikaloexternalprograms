package com.seratic.applikalo.externalprograms.modules.amauta.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;

import java.util.ArrayList;
import java.util.List;

public class AvanceFeedBackUtils {

    public static String getBodyFeedBackEstudiante(String idFeedBackEstudiante) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro3 = mapper.createObjectNode();
            ((ObjectNode) filtro3).put("campo", "id");
            ((ObjectNode) filtro3).put("condicional", "=");
            ((ObjectNode) filtro3).put("conjuncion", "AND");
            ((ObjectNode) filtro3).put("objeto", "feedback_estudiante");
            ((ObjectNode) filtro3).put("parametro", idFeedBackEstudiante);
            filtros.add(filtro3);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1);
            ((ObjectNode) objetoPeticion).put("objeto", "feedback_estudiante");

            List<JsonNode> campos = new ArrayList<>();

            JsonNode campoIdEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoIdEstudiante).put("campo", "estudiante.id");
            campos.add(campoIdEstudiante);

            JsonNode campoIdCurso = mapper.createObjectNode();
            ((ObjectNode) campoIdCurso).put("campo", "estudiante_leccion.leccion.modulo.curso.id");
            campos.add(campoIdCurso);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body feedback_estudiante", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String getBodyLeccionesCursoAvance(String nombreTipoLeccion, String idCurso) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro2 = mapper.createObjectNode();
            ((ObjectNode) filtro2).put("campo", "nombre");
            ((ObjectNode) filtro2).put("condicional", "ILIKE");
            ((ObjectNode) filtro2).put("conjuncion", "AND");
            ((ObjectNode) filtro2).put("objeto", "leccion.tipo_leccion");
            ((ObjectNode) filtro2).put("parametro", "'"+nombreTipoLeccion+"'");
            filtros.add(filtro2);

            JsonNode filtro3 = mapper.createObjectNode();
            ((ObjectNode) filtro3).put("campo", "id");
            ((ObjectNode) filtro3).put("condicional", "=");
            ((ObjectNode) filtro3).put("conjuncion", "AND");
            ((ObjectNode) filtro3).put("objeto", "leccion.modulo.curso");
            ((ObjectNode) filtro3).put("parametro", idCurso);
            filtros.add(filtro3);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 10000);
            ((ObjectNode) objetoPeticion).put("objeto", "leccion");

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campoTipoLeccion = mapper.createObjectNode();
            ((ObjectNode) campoTipoLeccion).put("campo", "tipo_leccion.nombre");
            campos.add(campoTipoLeccion);

            JsonNode campoIdCurso = mapper.createObjectNode();
            ((ObjectNode) campoIdCurso).put("campo", "modulo.curso.id");
            campos.add(campoIdCurso);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body leccion", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String getBodyLstFeedEstudiante(String idEstudiante, String idCurso) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro2 = mapper.createObjectNode();
            ((ObjectNode) filtro2).put("campo", "id");
            ((ObjectNode) filtro2).put("condicional", "=");
            ((ObjectNode) filtro2).put("conjuncion", "AND");
            ((ObjectNode) filtro2).put("objeto", "feedback_estudiante.estudiante");
            ((ObjectNode) filtro2).put("parametro", idEstudiante);
            filtros.add(filtro2);

            JsonNode filtro3 = mapper.createObjectNode();
            ((ObjectNode) filtro3).put("campo", "id");
            ((ObjectNode) filtro3).put("condicional", "=");
            ((ObjectNode) filtro3).put("conjuncion", "AND");
            ((ObjectNode) filtro3).put("objeto", "feedback_estudiante.estudiante_leccion.leccion.modulo.curso");
            ((ObjectNode) filtro3).put("parametro", idCurso);
            filtros.add(filtro3);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 10000);
            ((ObjectNode) objetoPeticion).put("objeto", "feedback_estudiante");

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campoIdEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoIdEstudiante).put("campo", "estudiante.id");
            campos.add(campoIdEstudiante);

            JsonNode campoIdCurso = mapper.createObjectNode();
            ((ObjectNode) campoIdCurso).put("campo", "estudiante_leccion.leccion.modulo.curso.id");
            campos.add(campoIdCurso);

            JsonNode campoEstado = mapper.createObjectNode();
            ((ObjectNode) campoEstado).put("campo", "estado_feedback_estudiante.nombre");
            campos.add(campoEstado);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body feedback_estudiante", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String getBodyWfFeedEstudPrimerEstado(String idFeedBackEstudiante) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro2 = mapper.createObjectNode();
            ((ObjectNode) filtro2).put("campo", "id");
            ((ObjectNode) filtro2).put("condicional", "=");
            ((ObjectNode) filtro2).put("conjuncion", "AND");
            ((ObjectNode) filtro2).put("objeto", "wf_feedback_estudiante.feedback_estudiante");
            ((ObjectNode) filtro2).put("parametro", idFeedBackEstudiante);
            filtros.add(filtro2);

            List<JsonNode> camposOrdenArray = new ArrayList<>();
            JsonNode campoOrdenId = mapper.createObjectNode();
            ((ObjectNode) campoOrdenId).put("campo", "id");
            ((ObjectNode) campoOrdenId).put("ascendente", true);
            camposOrdenArray.add(campoOrdenId);

            ((ObjectNode) objetoPeticion).putArray("orden").addAll(camposOrdenArray);
            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1);
            ((ObjectNode) objetoPeticion).put("objeto", "wf_feedback_estudiante");

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campoRel = mapper.createObjectNode();
            ((ObjectNode) campoRel).put("campo", "feedback_estudiante.id");
            campos.add(campoRel);

            JsonNode campoNodo = mapper.createObjectNode();
            ((ObjectNode) campoNodo).put("campo", "nodo");
            campos.add(campoNodo);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body feedback_estudiante", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String getBodyFeedback1(String idFeedback1, String nombreObjeto) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro = mapper.createObjectNode();
            ((ObjectNode) filtro).put("campo", "id");
            ((ObjectNode) filtro).put("condicional", "=");
            ((ObjectNode) filtro).put("conjuncion", "AND");
            ((ObjectNode) filtro).put("objeto", nombreObjeto);
            ((ObjectNode) filtro).put("parametro", idFeedback1);
            filtros.add(filtro);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1);
            ((ObjectNode) objetoPeticion).put("objeto", nombreObjeto);

            List<JsonNode> campos = new ArrayList<>();

            JsonNode campoIdEstudLeccion = mapper.createObjectNode();
            ((ObjectNode) campoIdEstudLeccion).put("campo", "feedback_estudiante.estudiante_leccion.estudiante.id");
            campos.add(campoIdEstudLeccion);

            JsonNode campoIdLeccion = mapper.createObjectNode();
            ((ObjectNode) campoIdLeccion).put("campo", "feedback_estudiante.estudiante_leccion.leccion.modulo.curso.id");
            campos.add(campoIdLeccion);

            JsonNode campoBanderaAvance = mapper.createObjectNode();
            ((ObjectNode) campoBanderaAvance).put("campo", "avance_calculado");
            campos.add(campoBanderaAvance);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body " + nombreObjeto, "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static JsonNode getBodyReFeedback1Estudiante(String idEstudiante, String nombreObjeto) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();
            List<JsonNode> filtros = new ArrayList<>();

            JsonNode filtro = mapper.createObjectNode();
            ((ObjectNode) filtro).put("campo", "id");
            ((ObjectNode) filtro).put("condicional", "=");
            ((ObjectNode) filtro).put("conjuncion", "AND");
            ((ObjectNode) filtro).put("objeto", nombreObjeto + ".feedback_estudiante.estudiante_leccion.estudiante");
            ((ObjectNode) filtro).put("parametro", idEstudiante);
            filtros.add(filtro);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 100000);
            ((ObjectNode) objetoPeticion).put("objeto", nombreObjeto);

            List<JsonNode> campos = new ArrayList<>();

            JsonNode campoIdEstudLeccion = mapper.createObjectNode();
            ((ObjectNode) campoIdEstudLeccion).put("campo", "feedback_estudiante.estudiante_leccion.estudiante.id");
            campos.add(campoIdEstudLeccion);

            JsonNode campoIdLeccion = mapper.createObjectNode();
            ((ObjectNode) campoIdLeccion).put("campo", "feedback_estudiante.estudiante_leccion.leccion.id");
            campos.add(campoIdLeccion);

            JsonNode campoBanderaAvance = mapper.createObjectNode();
            ((ObjectNode) campoBanderaAvance).put("campo", "avance_calculado");
            campos.add(campoBanderaAvance);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion;
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body " + nombreObjeto, "ex: " + e.getMessage());
            throw errList;
        }
    }
}
