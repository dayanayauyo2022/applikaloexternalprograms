## /objetos/crearObjeto(Crear/Actualizar)

Servicios POST para la ejecución del programa de creación/actualización de objetos. Según la información de la 
petición se trata de realizar un proceso de insercion o actualización de registros de una tabla determinada.
Se espera un body con los siguientes campos:
- objeto: un json con el id del objeto wf base
- nombreObjeto: el nombre del objeto wf base
- programaExterno: un objeto json con la configuración del nodo que está almacenado en ema (Componente)

A partir del campo programaExterno se obtiene el campo atributosCampos que es una lista que contiene los campos
y los nombres de los objetos que se deben insertar o actualizar. En primer lugar se recorre este listado para obtener
los objetos (tipo_campo = 5), luego se vuelve a recorrer la lista para obtener los campos que le correspondan a cada
objeto. luego se procede a obtener los valores para cada campo, estos pueden ser a partir de:
- **Valor Estático**: es un valor que llega en el objeto json de esta petición en el campo "valorConfigurar". Cuando
se trata del proceso de actualización, el id del objeto debe llegar en este campo.
- **Valor WF**: se debe obtener el valor a partir del objeto wf_nombreObjetoWf segun la ruta de campos especificada
en el objeto json de la petición (valorConfigurar)
- **Rango de estados**: se espera un estado inicial y un estado final en el objeto de la petición 
(valorEstadoInicial y valorEstadoFinal), de esta manera se calcula la diferencia entre sus fechas de creación y 
se retorna este valor en un formato de horas.
- **Funciones JS**: en el campo valorFuncionJs del objeto de la petición se espera una porción de código javascript en
formato texto. Se realiza un procesamiento de este código javascript y se ejecuta mediante el sdk de java y retorna
la respuesta que resulte de su ejecución. El procesamiento consiste en extraer las variables definidas y consultar
sus valores en el api GO, luego se reemplaza los windows.alert por espacio vacio para que se asuma como retorno. 

### Ejemplo ###

* URL: http://ema2progextbeta.latinapps.co/objetos/crearObjeto
* Headers: ``` token : eyJhbGciOiJIUzI1NiJ9.eyJpZFB1YmxpY28iOiJkODNiM2FmYjJiNTc4Nzk1OGJjMTQzMGQ3ZjI3NGU1NmFmMWNhYWNlNDRiOTg5NjU2MzQ1YTc0OGRhNjNhMWEzIiwidmVyc2lvbkFwbGljYWNpb24iOjU4MywicHJlZmlqbyI6ImRlc2EiLCJ1cmxBcGlSZWFkIjoiaHR0cHM6Ly9lbWEycmVhZGVyZGVzYS5sYXRpbmFwcHMuY28vYXBpcmVhZC8iLCJleHAiOjE1OTE5MzIwNTIsInVybEFwaVdyaXRlIjoiaHR0cDovL2xvY2FsaG9zdDo4MDg4LyJ9.osieSpRUtXkc2Fkteb2HKGXbcKmaXiLU44ei5vXPSxY```
* Body:
    ```
        {
            "objeto": {
                "id": 3,
                "key": "69fbd59363e40c5eddb3de3b932276c4f9d6e5802ef832e5880de5afccf08594"
            },
            "nombreObjeto": "mr_registro_de_tarea",
            "programaExterno": {
                "preguntaSeleccion": 1,
                "movil": true,
                "tipoEstado": 111,
                "textoBoton": "Afirmativa",
                "web": true,
                "atributosCampos": [
                    {
                        "valorConfigurarTxt": "",
                        "valorConfigurarCh": null,
                        "valorCrearObjeto": "colaborador",
                        "valorEstadoInicial": "",
                        "tipoCampo": {
                            "url_icono": "https://tdpdigitalstore.s3.amazonaws.com/res_apps/2019_01_24_04_29_4851%20generico.svg",
                            "id": 5,
                            "campos": [],
                            "nombre": "objeto"
                        },
                        "validoAlmacenar": true,
                        "idCampoPrograma": "",
                        "vinculadoA": null,
                        "programa": {
                            "color": "#4FF87B",
                            "editable": true,
                            "tipoPrograma": {
                                "descripcion": null,
                                "id": 2,
                                "nombre": "Actualizar objetos"
                            },
                            "urlIcono": null,
                            "id": 5,
                            "iconoNotNull": false,
                            "nombre": "Actualizar Objeto"
                        },
                        "valorCrear": "colaborador",
                        "tipoCampoNombre": "objeto",
                        "obligatorio": false,
                        "nombre": "colaborador",
                        "valorActualizar": "colaborador",
                        "tipoConfiguracion": {
                            "descripcion": null,
                            "id": 1,
                            "nombre": "Valor WF"
                        },
                        "valorConfigurar": "wf_datosmedicoocupacion.datosmedicoocupacion.colaborador.fecha_modificacion",
                        "valorEstadoFinal": "",
                        "fechaEjecucionCh": null,
                        "id": 1,
                        "valorConfigurarDt": null,
                        "valorCampoCrear": "",
                        "valorConfigurarTf": null,
                        "db": false
                    },
                    {
                      "valorConfigurarTxt": "",
                      "valorConfigurarCh": null,
                      "valorEstadoInicial": "",
                      "programa": {
                        "color": "#4FF87B",
                        "editable": true,
                        "tipoPrograma": {
                          "descripcion": null,
                          "id": 1,
                          "nombre": "Crear objetos"
                        },
                        "urlIcono": null,
                        "id": 1,
                        "iconoNotNull": false,
                        "nombre": "Crear tarea"
                      },
                      "nombre": "Función Test",
                      "tipoConfiguracion": {
                        "descripcion": null,
                        "id": 2,
                        "nombre": "Función"
                      },
                      "fechaEjecucionCh": null,
                      "id": 2,
                      "valorConfigurarDt": null,
                      "valorCampoCrear": " ",
                      "crearPartirDe": "Función",
                      "valorFuncionJs": "var objeto, cliente.direccion, cliente.numdocumento, wf_concesionario.id_usuario_creacion.id, cliente.telefono, cliente.fecha_creacion;// Describe esta función...function buscar(objeto) {  if (objeto == 'cliente.razon_social') {    window.alert('Cliente ubicado');  }}if (cliente.direccion == 'Avenida Cantuarias') {  window.alert('Cliente en Cantuarias');} else if (cliente.numdocumento == wf_concesionario.id_usuario_creacion.id) {  window.alert('Nuevo Campo');}if (cliente.telefono == '3153381472') {  window.alert('Cliente Movistar');}if (cliente.fecha_creacion == '2020/08/05') {  window.alert('Fecha');}",
                      "valorCrearObjeto": null,
                      "tipoCampo": {
                        "url_icono": "https://tdpdigitalstore.s3.amazonaws.com/res_apps/2019_01_24_04_29_4851%20generico.svg",
                        "id": 1,
                        "campos": [],
                        "nombre": "texto"
                      },
                      "validoAlmacenar": true,
                      "idCampoPrograma": null,
                      "vinculadoA": "cliente",
                      "valorCrear": "cliente.razonsocial",
                      "tipoCampoNombre": "texto",
                      "obligatorio": false,
                      "valorConfigurar": "Mi función Agregar",
                      "valorConfigurarCmb": "",
                      "valorEstadoFinal": "",
                      "valorConfigurarFuncion": "Mi función Agregar",
                      "valorConfigurarTf": null,
                      "db": false
                    }
                ],
                "nombrePrograma": "Actualizar estado del col",
                "tipoPrograma": 2,
                "condicion": true
            }
        }
    ``` 