package com.seratic.applikalo.externalprograms.modules.integracion.tareasProgramadas;

import com.seratic.applikalo.externalprograms.modules.amauta.AmautaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TareasAmauta {
    private static final Logger logger = LoggerFactory.getLogger(TareaReiniciarContadores.class);

    @Autowired
    AmautaService amautaService;

   // @Scheduled(cron = "${cron.asignar.feed.admin}", zone ="America/Lima")
    public void asignarFeedAdminCron() {
        try {
            amautaService.asignarFeedAdminCron();
        } catch (Exception e) {
            logger.info("====ERROR INESPERADO CRON ASIGNAR FEEDBACK ADMIN===== posible causa: ", e);
        }
    }

    @Scheduled(cron = "${cron.conferencia.notificar}", zone ="America/Lima")
    public void conferenciasNotificar() {
        try {
            amautaService.notificarCorreoEstudianteLeccionCron();
        } catch (Exception e) {
            logger.info("====ERROR INESPERADO CRON CONFERENCIASNOTIFICAR===== posible causa: ", e);
        }
    }

    @Scheduled(cron = "${cron.aprobar.feed.admin}", zone ="America/Lima")
    public void aprobarFeedAdminCron() {
        try {
            amautaService.aprobarFeedAdminCron();
        } catch (Exception e) {
            logger.info("====ERROR INESPERADO CRON APROBAR FEEDBACK ADMIN===== posible causa: ", e);
        }
    }
}
