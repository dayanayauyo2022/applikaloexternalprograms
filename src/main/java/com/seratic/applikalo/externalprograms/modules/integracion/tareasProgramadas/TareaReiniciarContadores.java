package com.seratic.applikalo.externalprograms.modules.integracion.tareasProgramadas;

import com.seratic.applikalo.externalprograms.modules.conteoactividades.ConteoActividadesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TareaReiniciarContadores {
    private static final Logger logger = LoggerFactory.getLogger(TareaReiniciarContadores.class);

    @Autowired
    ConteoActividadesService conteoActividadesService;

    @Scheduled(cron = "${cron.reinicio.contadores}", zone ="America/Lima")
    public void reiniciarContadores() {
        try {
            conteoActividadesService.reiniciarContadores();
        } catch (Exception e) {
            logger.info("====ERROR INESPERADO CRON REINICIO_CONTADORES===== posible causa: "+e.getMessage());
        }
    }
}
