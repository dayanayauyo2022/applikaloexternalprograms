package com.seratic.applikalo.externalprograms.modules.prosegur.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName(value = "evento")
public class EventoDTO {

    private String key;
    private String tipo;
    @JsonProperty(value = "nombre_cliente")
    private String nombreCliente;
    private UbicacionDTO ubicacion;
    private String direccion;
    @JsonProperty(value = "fecha_hora")
    private String fechaHora;

    private String campo1;
    private String campo2;

    public EventoDTO() {
    }

    public EventoDTO(String key, String tipo, String nombreCliente, String direccion, UbicacionDTO ubicacion, String fechaHora) {
        this.key = key;
        this.tipo = tipo;
        this.nombreCliente = nombreCliente;
        this.ubicacion = ubicacion;
        this.direccion = direccion;
        this.fechaHora = fechaHora;
    }
    /**
     *
     * key: Nombre evento
     * Datos: array de 8 elementos
     * Solo se debe usar validando la longitud de 8*/
    public EventoDTO(String key, String[] datos) {

        this.key = key;
        this.tipo = datos[0];
        this.nombreCliente = datos[1];
        this.direccion = datos[2];
        this.ubicacion = new UbicacionDTO(datos[3], datos[4]);
        this.fechaHora = datos[5];
        this.campo1 = datos[6];
        this.campo2 = datos[7];
    }

    public String getKey() {
        return key;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public UbicacionDTO getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(UbicacionDTO ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getCampo1() {
        return campo1;
    }

    public void setCampo1(String campo1) {
        this.campo1 = campo1;
    }

    public String getCampo2() {
        return campo2;
    }

    public void setCampo2(String campo2) {
        this.campo2 = campo2;
    }
}
