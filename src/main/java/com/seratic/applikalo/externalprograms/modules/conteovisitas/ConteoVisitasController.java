package com.seratic.applikalo.externalprograms.modules.conteovisitas;

import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping(value = "/visitas")
public class ConteoVisitasController {
    private static final Logger logger = LoggerFactory.getLogger(ConteoVisitasController.class);

    @Autowired
    ConteoVisitasService conteoVisitasService;

    @RequestMapping(method = POST, value = "/conteoVisitas", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> conteoVisitas(@RequestBody String requestBody,
                                           @RequestAttribute("prefijo") String prefijo,
                                           @RequestAttribute("versionAplicacion") Integer versionAplicacion,
                                           @RequestAttribute("idPublico") String idPublico,
                                           @RequestAttribute("urlApiWrite") String urlApiWrite,
                                           @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    conteoVisitasService.contarVisitas(requestBody, prefijo, versionAplicacion,
                            idPublico, urlApiWrite, urlApiRead),
                    headers,
                    HttpStatus.OK
            );
        } catch (MyErrorListException myEx) {
            logger.info("CONTROLLER conteoVisitas: " + serializeErrors(myEx.getErrors()));
            return new ResponseEntity<>(serializeErrors(myEx.getErrors()), headers, HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            logger.info("CONTROLLER conteoVisitas : " + ex.getMessage());
            return new ResponseEntity<>((MultiValueMap<String, String>) ex, BAD_REQUEST);
        }
    }

    private String serializeErrors(Object object) {
        return new JSONSerializer()
                .exclude("*.class")
                .exclude("cause")
                .exclude("localizedMessage")
                .exclude("stackTraceDepth")
                .serialize(object);
    }
}
