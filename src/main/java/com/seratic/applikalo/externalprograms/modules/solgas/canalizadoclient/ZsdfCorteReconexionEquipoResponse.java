//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.3.0 
// Visite <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.25 a las 10:32:01 AM COT 
//


package com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EtReturn" type="{urn:sap-com:document:sap:soap:functions:mc-style}ZsdsReturnCanalizadoWs"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "etReturn"
})
@XmlRootElement(name = "ZsdfCorteReconexionEquipoResponse")
public class ZsdfCorteReconexionEquipoResponse {

    @XmlElement(name = "EtReturn", required = true)
    protected ZsdsReturnCanalizadoWs etReturn;

    /**
     * Obtiene el valor de la propiedad etReturn.
     * 
     * @return
     *     possible object is
     *     {@link ZsdsReturnCanalizadoWs }
     *     
     */
    public ZsdsReturnCanalizadoWs getEtReturn() {
        return etReturn;
    }

    /**
     * Define el valor de la propiedad etReturn.
     * 
     * @param value
     *     allowed object is
     *     {@link ZsdsReturnCanalizadoWs }
     *     
     */
    public void setEtReturn(ZsdsReturnCanalizadoWs value) {
        this.etReturn = value;
    }

}
