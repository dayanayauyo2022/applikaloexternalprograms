package com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.VOs;

public class WfVariableVo {
    private String llave;
    private String valor;
    private String[] camposRuta;
    private Integer numCamposRuta;

    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String[] getCamposRuta() {
        return camposRuta;
    }

    public void setCamposRuta(String[] camposRuta) {
        this.camposRuta = camposRuta;
    }

    public Integer getNumCamposRuta() {
        return numCamposRuta;
    }

    public void setNumCamposRuta(Integer numCamposRuta) {
        this.numCamposRuta = numCamposRuta;
    }
}
