//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.23 a las 12:16:31 PM COT 
//
package com.seratic.applikalo.externalprograms.modules.solgas.soap.gen;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para tanque complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="tanque">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="activotanque" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="codigotanque" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="capacidad" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="edificio" type="{http://www.seratic.com/applikalo/externalprograms/modules/solgas/soap/gen}edificio" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tanque", propOrder = {
    "id",
    "activotanque",
    "codigotanque",
    "capacidad",
    "edificio"
})
@JsonTypeName(value = "tanque")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tanque {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected Integer id;
    protected Boolean activotanque;
    protected String codigotanque;
    protected Integer capacidad;
    @JsonIgnoreProperties({"envionotificacion", "correonotificacion", "activoedificio", "tipo", "distrito", "provincia", "departamento", "ubicacion", "direccion", "nombre"})
    protected Edificio edificio;

    /**
     * Obtiene el valor de la propiedad id.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad activotanque.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isActivotanque() {
        return activotanque;
    }

    /**
     * Define el valor de la propiedad activotanque.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public void setActivotanque(Boolean value) {
        this.activotanque = value;
    }

    /**
     * Obtiene el valor de la propiedad codigotanque.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodigotanque() {
        return codigotanque;
    }

    /**
     * Define el valor de la propiedad codigotanque.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCodigotanque(String value) {
        this.codigotanque = value;
    }

    /**
     * Obtiene el valor de la propiedad capacidad.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getCapacidad() {
        return capacidad;
    }

    /**
     * Define el valor de la propiedad capacidad.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setCapacidad(Integer value) {
        this.capacidad = value;
    }

    /**
     * Obtiene el valor de la propiedad edificio.
     *
     * @return possible object is {@link Edificio }
     *
     */
    public Edificio getEdificio() {
        return edificio;
    }

    /**
     * Define el valor de la propiedad edificio.
     *
     * @param value allowed object is {@link Edificio }
     *
     */
    public void setEdificio(Edificio value) {
        this.edificio = value;
    }

}
