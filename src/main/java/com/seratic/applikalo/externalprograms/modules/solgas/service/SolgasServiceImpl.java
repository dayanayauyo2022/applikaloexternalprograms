package com.seratic.applikalo.externalprograms.modules.solgas.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.integracion.IntegracionRepository;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import static com.seratic.applikalo.externalprograms.modules.security.utils.TokenManagerUtil.generarTokenGetObjeto;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient.CanalizadoClient;
import com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient.ZsdfCorteReconexionEquipo;
import com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient.ZsdfCorteReconexionEquipoResponse;
import com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient.ZsdfIngresoMediciones;
import com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient.ZsdfIngresoMedicionesResponse;
import com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient.ZsdsCorecequipo;
import com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient.ZsdsIngmedicion;
import com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient.ZsdttCorecequipo;
import com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient.ZsdttIngmedicion;
import com.seratic.applikalo.externalprograms.modules.solgas.utils.UtilsSolgas;
import com.seratic.applikalo.externalprograms.objectmanager.dto.ObjectManagerResponseDTO;
import com.seratic.applikalo.externalprograms.objectmanager.repository.ObjectManagerRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import static com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.utils.UtilsObjetos.armarPeticionGetObjetoWfApi;
import static com.seratic.applikalo.externalprograms.modules.solgas.utils.ConstantesStatic.KEYFECHACREACION;
import static com.seratic.applikalo.externalprograms.modules.solgas.utils.ConstantesStatic.KEYLECTURAACTUAL;
import static com.seratic.applikalo.externalprograms.modules.solgas.utils.ConstantesStatic.KEYLECTURAMEDIDOR;
import static com.seratic.applikalo.externalprograms.modules.solgas.utils.ConstantesStatic.KEYMEDIDOR;
import static com.seratic.applikalo.externalprograms.modules.solgas.utils.ConstantesStatic.KEYMEDIDOROBJ;
import static com.seratic.applikalo.externalprograms.modules.solgas.utils.ConstantesStatic.KEYOBJETO;
import static com.seratic.applikalo.externalprograms.modules.solgas.utils.ConstantesStatic.KEYSTATUSCODE;
import static com.seratic.applikalo.externalprograms.modules.solgas.utils.ConstantesStatic.MSJNOCONNECT_API;

import java.util.Locale;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author MONDRAGON
 */
@Service
public class SolgasServiceImpl implements SolgasService {

    private static final Log LOG = LogFactory.getLog(SolgasServiceImpl.class);
    @Value(value = "${solgas.programacion-automatica.schema}")
    private String schema;
    @Value(value = "${solgas.programacion-automatica.urlReader}")
    private String urlAPIRead;
    @Value(value = "${solgas.programacion-automatica.urlWriter}")
    private String urlAPIWrite;
    @Value(value = "${solgas.programacion-automatica.idVersionApp}")
    private Integer idVersionApp;
    @Value(value = "${solgas.programacion-automatica.idFuenteEdificio}")
    private Integer idFuenteEdificio;
    @Value(value = "${solgas.programacion-automatica.idFuenteEstadosProgramacion}")
    private Integer idFuenteEstadoProgramacion;
    @Value(value = "${solgas.programacion-automatica.id-publico}")
    private String idPublico;
    @Value(value = "${solgas.programacion-automatica.estadoInicio}")
    private List<String> estadoInicioProgramacion;
    @Value(value = "${solgas.programacion-automatica.estadoFin}")
    private String estadoFinProgramacion;
    @Value(value = "${solgas.programacion-automatica.estadoCrearProgramacion}")
    private String estadoCrearProgramacion;
    @Value(value = "${solgas.calculopromedio.numlecturasconsumosinvalidosinspeccion}")
    private int numLecturasConsumosInvalidosInspeccion;
    @Value("${solgas.insertSolgasSOAP.idFuenteDato-lecturaMedidor}")
    private String idFuenteLecturaMeidor;
    @Value(value = "${solgas.insertSolgasSOAP.idEstsdoMeidorUpdate}")
    private String idEstsdoMeidorUpdate;
    private static final String NOMBRE = "nombre";
    private static final DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
    private static final DateTimeFormatter outFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter outFormatTime = DateTimeFormatter.ofPattern("HH:mm:ss");
    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private IntegracionRepository integracionRepository;
    @Autowired
    private ObjectManagerRepository repository;
    @Autowired
    private CanalizadoClient canalizadoClient;
    private UtilsSolgas utilsSolgas = new UtilsSolgas();

    @Override
    public void insertMediciones(String schema, String idPublico, String versionApplicacion, String urlApiWrite, String urlApiRead, Integer idEstadoMedidor) throws JsonProcessingException {
        ObjectManagerResponseDTO responseApi = mapper.readValue(repository.getGeneric(urlAPIRead + "funcion/getCamposObjeto/" + versionApplicacion + "/" + idPublico, new DataToken(schema, idPublico, Integer.valueOf(versionApplicacion), null, null), utilsSolgas.getBodyMedidorByEstate(idEstadoMedidor)), ObjectManagerResponseDTO.class);
        if (responseApi.isSuccess() && !responseApi.getData().isEmpty()) {
            ZsdfIngresoMediciones inMedidores = new ZsdfIngresoMediciones();
            ZsdttIngmedicion ingmedicion = new ZsdttIngmedicion();
            inMedidores.setItIngresoMedicion(ingmedicion);
            List<ZsdsIngmedicion> items = ingmedicion.getItem();
            List<JsonNode> medidores = new ArrayList<>(0);
            responseApi.getData().forEach(object -> {
                ZsdsIngmedicion item = new ZsdsIngmedicion();
                item.setCentro(object.at("/medidor/centro").asText());
                item.setMedidor(object.at("/medidor/codigo").asText());
                item.setFechaLectura(outFormat.format(inputFormat.parse(object.get(KEYFECHACREACION).asText())));
                try {
                    item.setHoraLectura(DatatypeFactory.newInstance().newXMLGregorianCalendar(outFormatTime.format(LocalDateTime.parse(object.get(KEYFECHACREACION).asText(), inputFormat).toLocalTime())));
                } catch (DatatypeConfigurationException e) {
                    e.printStackTrace();
                }
                item.setLectura(BigDecimal.valueOf(object.at("/medidor/ultimalectura").asDouble()));
                item.setPresion(BigDecimal.valueOf(object.at("/medidor/presionfabrica").asDouble()));
                medidores.add(mapper.convertValue(object, JsonNode.class));
                items.add(item);
            });
            insertMedidorSolgasSOAP(inMedidores, medidores, new DataToken(schema, idPublico, Integer.valueOf(versionApplicacion), urlApiWrite, urlApiRead));
        }
    }

    public void insertMedidorSolgasSOAP(ZsdfIngresoMediciones ingmedicion, List<JsonNode> estadoMedidor, DataToken dataToken) {
        List<JsonNode> medidoresUpdate = new ArrayList<>(0);
        ZsdfIngresoMedicionesResponse response;
        if (ingmedicion.getItIngresoMedicion().getItem().size() == 100) {
            response = sendDataSolgas(ingmedicion);
        } else {
            response = insertMedidoresToBatch(ingmedicion);
        }
        response.getEtReturn().getItem().forEach(medidorInsert -> medidoresUpdate.add(estadoMedidor.stream().filter(jsonMedidor -> Objects.equals(jsonMedidor.at("/medidor/codigo").asText(), medidorInsert.getMedidor())).findAny().get()));
        medidoresUpdate.forEach(medidorCurrentUpdate -> {
            String responseUpdate = repository.update(dataToken, utilsSolgas.bodyUpdateEstadoMedidor(medidorCurrentUpdate, Integer.valueOf(idEstsdoMeidorUpdate)), false, false, true, false);
            if (Objects.isNull(responseUpdate)) {
                LOG.error( "  SolgasServiceImpl  ->  insertMedidorSolgasSOAP  Fallo actualizando estado del medidor:  " + medidorCurrentUpdate);
            }
        });
    }

    private ZsdfIngresoMedicionesResponse insertMedidoresToBatch(ZsdfIngresoMediciones ingmedicion) {
        ZsdfIngresoMedicionesResponse response = new ZsdfIngresoMedicionesResponse();
        ZsdfIngresoMediciones zsdttIngmedicion = new ZsdfIngresoMediciones();
        ZsdttIngmedicion zsdttIngmedicion1 = new ZsdttIngmedicion();
        zsdttIngmedicion.setItIngresoMedicion(zsdttIngmedicion1);
        List<ZsdsIngmedicion> items = zsdttIngmedicion1.getItem();
        int indexMedidores  = 0;
        for (int i = 0; i < ingmedicion.getItIngresoMedicion().getItem().size(); i++) {
            items.add(ingmedicion.getItIngresoMedicion().getItem().get(i));
            indexMedidores++;
            if (indexMedidores == 100) {
                response = sendDataSolgas(zsdttIngmedicion);
                items.clear();
                indexMedidores = 0;
            }
        }
        if (indexMedidores > 1) {
            response = sendDataSolgas(zsdttIngmedicion);
        }
        return response;
    }

    private ZsdfIngresoMedicionesResponse sendDataSolgas(ZsdfIngresoMediciones ingmedicion) {
        ZsdfIngresoMedicionesResponse response = new ZsdfIngresoMedicionesResponse();
        try {
            response = canalizadoClient.zsdfIngresoMediciones(ingmedicion);
        } catch (Exception e) {
            LOG.error("  SolgasServiceImpl  ->  insertMedidorSolgasSOAP  Fallo insertando el medidores: ", e);
        }
        return response;
    }

    @Override
    public void insertCorteReconexiones() throws JsonProcessingException {
        ObjectManagerResponseDTO responseApi = mapper.readValue(repository.getGeneric(urlAPIRead + "funcion/getCamposObjeto/" + "/" + idPublico, new DataToken(schema, idPublico, Integer.valueOf(0), null, null), utilsSolgas.getBodyMedidorByEstate(0)), ObjectManagerResponseDTO.class);
        List<ZsdsCorecequipo> items = new ArrayList<>(0);
        ZsdsCorecequipo item = new ZsdsCorecequipo();
        ZsdfCorteReconexionEquipoResponse zsdfCorteReconexionEquipo = canalizadoClient.zsdfCorteReconexionEquipo( new ZsdfCorteReconexionEquipo());
    }

    @Override
    public boolean calcularPromedio(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        try {
            if (prefijo != null && !prefijo.isEmpty() && versionApp != null && idPublico != null && !idPublico.isEmpty()) {
                DecimalFormat df2 = new DecimalFormat("#.##");
                JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
                JsonNode objeto = objetoRequestJson.get(KEYOBJETO);
                String idLecturaMedidor = objeto.get("id").asText();

                List<String> campos = new ArrayList<>();
                campos.add("medidor.id");
                String body = armarPeticionGetObjetoWfApi(KEYLECTURAMEDIDOR, idLecturaMedidor, null, campos, false, 1, 0, 1);
                DataToken dataTokenApiRead = new DataToken(prefijo, idPublico, versionApp, urlWrite, urlRead);
                String tokenEncriptApiRead = generarTokenGetObjeto(dataTokenApiRead);
                ResponseEntity<String> responseGetObjeto = integracionRepository.getObjetoApi(dataTokenApiRead, tokenEncriptApiRead, body);
                JsonNode nodoRespGetObjeto = mapper.readTree(responseGetObjeto.getBody());
                if (nodoRespGetObjeto.get(KEYSTATUSCODE) != null && !nodoRespGetObjeto.get(KEYSTATUSCODE).asText().equals("200")) {
                    errList.addError("412", KEYMEDIDOR, MSJNOCONNECT_API);
                    throw errList;
                }
                String idMedidor = nodoRespGetObjeto.get("data").get(0).get(KEYMEDIDOROBJ).get("id").asText();
                //CONSULTAR ULTIMOS 13 REGISTROS DEL MEDIDOR
                campos.clear();
                campos.add("medidor.id");
                campos.add(KEYLECTURAACTUAL);
                campos.add(KEYFECHACREACION);
                body = armarPeticionGetObjetoWfApi(KEYLECTURAMEDIDOR, idMedidor, KEYMEDIDOROBJ, campos, false, 1, 0, 999);
                responseGetObjeto = integracionRepository.getObjetoApi(dataTokenApiRead, tokenEncriptApiRead, body);
                nodoRespGetObjeto = mapper.readTree(responseGetObjeto.getBody());
                if (nodoRespGetObjeto.get(KEYSTATUSCODE) != null && !nodoRespGetObjeto.get(KEYSTATUSCODE).asText().equals("200")) {
                    errList.addError("412", KEYMEDIDOR, MSJNOCONNECT_API);
                    throw errList;
                }
                JsonNode lecturas = nodoRespGetObjeto.get("data");
                int cantidadLecturas = lecturas.size();
                List<Double> lecturasValidas = new ArrayList<>();
                List<Integer> posicionLecturasValidas = new ArrayList<>();
                String fechaUltimaLecturaValida = null;
                double lecturaAnterior = -1;
                for (int i = cantidadLecturas - 1; i >= 0; i--) {
                    JsonNode lectura = lecturas.get(i);
                    double lecturaActual = lectura.get(KEYLECTURAACTUAL).asDouble();
                    if ((lecturaActual > 0) && (lecturaAnterior == -1) || (lecturaActual > lecturaAnterior)) {
                        lecturasValidas.add(0, lecturaActual);
                        posicionLecturasValidas.add(0, i);
                        fechaUltimaLecturaValida = lectura.get(KEYFECHACREACION).asText();
                        lecturaAnterior = lecturaActual;
                    }
                }
                int cantidadLecturasValidas = lecturasValidas.size();
                boolean crearInspeccionMedidor = (cantidadLecturas >= numLecturasConsumosInvalidosInspeccion) && ((cantidadLecturasValidas == 0) || (posicionLecturasValidas.get(0) > numLecturasConsumosInvalidosInspeccion - 1));

                //ACTUALIZAR DATA DEL MEDIDOR
                if (cantidadLecturasValidas > 0) {
                    double lecturaActual = lecturasValidas.get(0);
                    double consumoActual = (cantidadLecturasValidas > 1) ? ((lecturaActual - lecturasValidas.get(1)) / (posicionLecturasValidas.get(1) - posicionLecturasValidas.get(0))) : lecturaActual;
                    double promedioConsumoActual = (cantidadLecturasValidas > 2) ? ((lecturaActual - lecturasValidas.get(cantidadLecturasValidas - 1)) / (posicionLecturasValidas.get(cantidadLecturasValidas - 1) - posicionLecturasValidas.get(0))) : consumoActual;
                    body = armarPeticionUpdateMedidorApiWrite(KEYMEDIDOROBJ, idMedidor, lecturaActual, df2.format(consumoActual), df2.format(promedioConsumoActual), !crearInspeccionMedidor);
                } else if (crearInspeccionMedidor) {
                    body = armarPeticionUpdateConsumo3MesesMedidorApiWrite(KEYMEDIDOROBJ, idMedidor, !crearInspeccionMedidor);
                }
                if ((cantidadLecturasValidas > 0) || crearInspeccionMedidor) {
                    ResponseEntity<String> respUpdateObject = integracionRepository.updateServicioUpdateApigo(dataTokenApiRead, body, true, false, false);
                    JsonNode nodoRespUpdate = mapper.readTree(respUpdateObject.getBody());
                    if (nodoRespUpdate.get(KEYSTATUSCODE) != null && !nodoRespUpdate.get(KEYSTATUSCODE).asText().equals("200")) {
                        errList.addError("412", "UPDATE API WRITE", "No fue posible actualizar los datos de objeto medidor");
                        throw errList;
                    }
                }
                if (crearInspeccionMedidor) {
                    String fechaCreacion = null;
                    if (fechaUltimaLecturaValida == null) {
                        campos.clear();
                        campos.add(KEYFECHACREACION);
                        body = armarPeticionGetFechaUltimaLecturaValidaWfApi(idMedidor);
                        responseGetObjeto = integracionRepository.getObjetoApi(dataTokenApiRead, tokenEncriptApiRead, body);
                        nodoRespGetObjeto = mapper.readTree(responseGetObjeto.getBody());
                        if (nodoRespGetObjeto.get(KEYSTATUSCODE) != null && !nodoRespGetObjeto.get(KEYSTATUSCODE).asText().equals("200")) {
                            errList.addError("412", KEYMEDIDOR, MSJNOCONNECT_API);
                            throw errList;
                        }
                        fechaCreacion = nodoRespGetObjeto.get("data").get(0).get(KEYFECHACREACION).asText();
                    } else {
                        fechaCreacion = fechaUltimaLecturaValida;
                    }
                    //CREA DATA DE LA INSPECCION DEL MEDIDOR
                    body = armarPeticionCreateInspeccionApiWrite("inspeccion", idMedidor, fechaCreacion);
                    ResponseEntity<String> response = integracionRepository.insertApi(dataTokenApiRead, body);
                    JsonNode responseJsonNode = mapper.readTree(response.getBody());
                    if (responseJsonNode.get(KEYSTATUSCODE) != null && !responseJsonNode.get(KEYSTATUSCODE).asText().equals("200")) {
                        errList.addError("412", "INSERT API WRITE", "No fue posible insertar los datos de objeto inspeccion_medidor");
                        throw errList;
                    }
                }
                return true;
            } else {
                errList.addError("1.1", "faltan precondiciones", "los parametros prefijo y versionApp son obligatorios");
                throw errList;
            }
        } catch (MyErrorListException e) {
            if ((e.getErrors() != null) && (!e.getErrors().isEmpty())) {
                errList.addErrorList((e).getErrors());
            } else {
                errList.addError("10.2", "Error proceso de calculo promedio:", e.getMessage());
            }
            throw errList;
        } catch (Exception e) {
            errList.addError("10.2", "Error proceso de calculo promedio:", e.getMessage());
            throw errList;
        }
    }

    @Override
    public void programacionAutomatica() {
        DataToken dataTokenApiRead = new DataToken(schema, idPublico, idVersionApp, urlAPIWrite, urlAPIRead);
        ObjectNode body = mapper.createObjectNode();
        ArrayNode filtros = body.putArray("filtros");
        ObjectNode filtro = mapper.createObjectNode();
        filtro.put("campo", "nombre");
        filtro.put("condicional", "IN");
        filtro.put(KEYOBJETO, "estado_programacion");
        filtro.put("parametro", "'" + estadoFinProgramacion + "','" + estadoCrearProgramacion + "'");
        filtros.add(filtro);
        List<Map<String, Object>> response = integracionRepository.getObjetoApiCamposPaginacion(dataTokenApiRead, idVersionApp, dataTokenApiRead.getPrefijo(), idFuenteEstadoProgramacion, "/" + dataTokenApiRead.getIdPublico(), body.toString());
        if (response.size() == 2) {
            String idEstadoProgramacionFinalizado = null;
            String idEstadoCrearProgramacion = null;
            for (Map<String, Object> estadoProgamacion : response) {
                if (estadoProgamacion.get(NOMBRE).equals(estadoFinProgramacion)) {
                    idEstadoProgramacionFinalizado = estadoProgamacion.get("id").toString();
                }
                if (estadoProgamacion.get(NOMBRE).equals(estadoCrearProgramacion)) {
                    idEstadoCrearProgramacion = estadoProgamacion.get("id").toString();
                }
            }
            if (idEstadoProgramacionFinalizado != null && idEstadoCrearProgramacion != null) {
                body = mapper.createObjectNode();
                filtros = body.putArray("filtros");
                filtro = mapper.createObjectNode();
                filtro.put("campo", "nombre");
                filtro.put("condicional", "IN");
                filtro.put(KEYOBJETO, "estado_programacion");
                String parametro = "";
                for (String estado : estadoInicioProgramacion) {
                    if (!parametro.isEmpty()) {
                        parametro += ",";
                    }
                    parametro += "'" + estado + "'";
                }
                filtro.put("parametro", parametro);
                filtros.add(filtro);
                response = integracionRepository.getObjetoApiCamposPaginacion(dataTokenApiRead, idVersionApp, dataTokenApiRead.getPrefijo(), idFuenteEdificio, "/" + dataTokenApiRead.getIdPublico(), body.toString());
                if (!response.isEmpty()) {
                    for (Map<String, Object> programacion : response) {
                        Map<String, Object> edificio = (Map<String, Object>) programacion.get("edificio");
                        Map<String, Object> usuario = (Map<String, Object>) programacion.get("usuario");
                        String bodyUpdateObjeto = armarPeticionUpdateProgramacionApiWrite("programacion", programacion.get("id").toString(), idEstadoProgramacionFinalizado);
                        integracionRepository.updateServicioUpdateApigo(dataTokenApiRead, bodyUpdateObjeto, true, false, false);
                        String bodyCreateObjeto = armarPeticionCreateProgramacionApiWrite("programacion", idVersionApp, edificio != null ? edificio.get("id").toString() : null, usuario != null ? usuario.get("id").toString() : null, idEstadoCrearProgramacion);
                        integracionRepository.updateApi(dataTokenApiRead, bodyCreateObjeto, false);
                    }
                }
            }
        }
    }

    private String armarPeticionGetFechaUltimaLecturaValidaWfApi(String idMedidor) {
        ObjectNode objetoPeticion = mapper.createObjectNode();
        List<JsonNode> filtros = new ArrayList<>();
        ObjectNode filtro = mapper.createObjectNode();
        filtro.put("campo", "id");
        filtro.put("condicional", "=");
        filtro.put(KEYOBJETO, KEYMEDIDOROBJ);
        filtro.put("parametro", idMedidor);
        filtros.add(filtro);
        filtro = mapper.createObjectNode();
        filtro.put("campo", KEYLECTURAACTUAL);
        filtro.put("condicional", ">");
        filtro.put(KEYOBJETO, KEYLECTURAMEDIDOR);
        filtro.put("parametro", 0);
        filtros.add(filtro);

        List<JsonNode> camposOrdenArray = new ArrayList<>();
        JsonNode campoOrdenFecha = mapper.createObjectNode();
        ((ObjectNode) campoOrdenFecha).put("campo", "fecha_creacion");
        ((ObjectNode) campoOrdenFecha).put("ascendente", false);
        camposOrdenArray.add(campoOrdenFecha);

        objetoPeticion.putArray("orden").addAll(camposOrdenArray);
        objetoPeticion.putArray("filtros").addAll(filtros);
        objetoPeticion.put("page", 1);
        objetoPeticion.put("start", 0);
        objetoPeticion.put("pagesize", 1);
        objetoPeticion.put(KEYOBJETO, KEYLECTURAMEDIDOR);

        List<JsonNode> campos = new ArrayList<>();
        ObjectNode nodoCampo = mapper.createObjectNode();
        nodoCampo.put("campo", KEYFECHACREACION);
        campos.add(nodoCampo);
        nodoCampo = mapper.createObjectNode();
        nodoCampo.put("campo", KEYLECTURAACTUAL);
        campos.add(nodoCampo);

        objetoPeticion.putArray("campos").addAll(campos);

        return objetoPeticion.toString();
    }

    private String armarPeticionUpdateMedidorApiWrite(String nombreObjeto, String idMedidor, Double lecturaActual, String consumoActual, String promedioConsumoActual, boolean consumo3meses) {
        ObjectNode objetoPeticion = mapper.createObjectNode();
        ObjectNode camposMedidor = mapper.createObjectNode();
        camposMedidor.put("id", idMedidor);
        camposMedidor.put("consumopromedio", promedioConsumoActual);
        camposMedidor.put("ultimalectura", lecturaActual);
        camposMedidor.put("ultimoconsumo", consumoActual);
        camposMedidor.put("consumo3meses", consumo3meses);
        objetoPeticion.put(nombreObjeto, camposMedidor);
        return objetoPeticion.toString();
    }

    private String armarPeticionUpdateConsumo3MesesMedidorApiWrite(String nombreObjeto, String idMedidor, boolean consumo3meses) {
        ObjectNode objetoPeticion = mapper.createObjectNode();

        ObjectNode camposMedidor = mapper.createObjectNode();
        camposMedidor.put("id", idMedidor);
        camposMedidor.put("consumo3meses", consumo3meses);

        objetoPeticion.put(nombreObjeto, camposMedidor);

        return objetoPeticion.toString();
    }

    private String armarPeticionUpdateProgramacionApiWrite(String nombreObjeto, String idProgramacion, String idEstadoProgramacion) {
        JsonNode objetoPeticion = mapper.createObjectNode();

        JsonNode camposProgramacion = mapper.createObjectNode();
        ((ObjectNode) camposProgramacion).put("id", idProgramacion);
        JsonNode estadoProgramacion = mapper.createObjectNode();
        ((ObjectNode) estadoProgramacion).put("id", idEstadoProgramacion);

        ((ObjectNode) camposProgramacion).put("estado_programacion", estadoProgramacion);

        ((ObjectNode) objetoPeticion).put(nombreObjeto, camposProgramacion);
        return objetoPeticion.toString();
    }

    private String armarPeticionCreateProgramacionApiWrite(String nombreObjeto, int versionApp, String idEdificio, String idUsuario, String idEstadoCrearProgramacion) {
        ObjectNode camposProgramacion = mapper.createObjectNode();
        ObjectNode edificio = mapper.createObjectNode();
        edificio.put("id", idEdificio);
        camposProgramacion.put("edificio", edificio);
        camposProgramacion.put("key", versionApp + "_" + (idUsuario == null ? "" : idUsuario) + "_" + new Date().getTime());
        if (idUsuario != null) {
            ObjectNode usuario = mapper.createObjectNode();
            usuario.put("id", idUsuario);
            camposProgramacion.put("usuario", usuario);
        }
        ObjectNode objetoProgramacion = mapper.createObjectNode();
        objetoProgramacion.putPOJO(nombreObjeto, camposProgramacion);
        ArrayNode arrayNode = mapper.createArrayNode();
        arrayNode.addAll(Arrays.asList(objetoProgramacion));
        ObjectNode objetoPeticion = mapper.createObjectNode();
        objetoPeticion.putPOJO("Objetos", arrayNode);
        return objetoPeticion.toString();
    }

    private String armarPeticionCreateInspeccionApiWrite(String nombreObjeto, String idMedidor, String fechaUltimaLecturaValida) {
        ObjectNode request = mapper.createObjectNode();
        ObjectNode inspecciones = mapper.createObjectNode();
        ObjectNode inspeccion = mapper.createObjectNode();
        ObjectNode medidor = mapper.createObjectNode();
        medidor.put("id", idMedidor);
        inspeccion.putPOJO(KEYMEDIDOROBJ, medidor);
        if (fechaUltimaLecturaValida != null) {
            inspeccion.put("ultima_lectura_valida", fechaUltimaLecturaValida);
        }
        inspecciones.putPOJO(nombreObjeto, inspeccion);

        ArrayNode arrayNode = mapper.createArrayNode();
        arrayNode.addAll(Arrays.asList(inspecciones));

        request.putPOJO("Objetos", arrayNode);
        return request.toString();
    }

}
