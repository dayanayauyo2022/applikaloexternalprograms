package com.seratic.applikalo.externalprograms.modules.entities;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "nombre",
        "descripcion",
        "url_icono",
        "disenioWeb",
        "disenioMovil",
        "atributos",
        "visualizacion"
})
public class TipoComponente {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("nombre")
    private String nombre;
    @JsonProperty("descripcion")
    private String descripcion;
    @JsonProperty("url_icono")
    private Object urlIcono;
    @JsonProperty("disenioWeb")
    private Object disenioWeb;
    @JsonProperty("disenioMovil")
    private Object disenioMovil;
    @JsonProperty("atributos")
    private Object atributos;
    @JsonProperty("visualizacion")
    private Object visualizacion;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("nombre")
    public String getNombre() {
        return nombre;
    }

    @JsonProperty("nombre")
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @JsonProperty("descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    @JsonProperty("descripcion")
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @JsonProperty("url_icono")
    public Object getUrlIcono() {
        return urlIcono;
    }

    @JsonProperty("url_icono")
    public void setUrlIcono(Object urlIcono) {
        this.urlIcono = urlIcono;
    }

    @JsonProperty("disenioWeb")
    public Object getDisenioWeb() {
        return disenioWeb;
    }

    @JsonProperty("disenioWeb")
    public void setDisenioWeb(Object disenioWeb) {
        this.disenioWeb = disenioWeb;
    }

    @JsonProperty("disenioMovil")
    public Object getDisenioMovil() {
        return disenioMovil;
    }

    @JsonProperty("disenioMovil")
    public void setDisenioMovil(Object disenioMovil) {
        this.disenioMovil = disenioMovil;
    }

    @JsonProperty("atributos")
    public Object getAtributos() {
        return atributos;
    }

    @JsonProperty("atributos")
    public void setAtributos(Object atributos) {
        this.atributos = atributos;
    }

    @JsonProperty("visualizacion")
    public Object getVisualizacion() {
        return visualizacion;
    }

    @JsonProperty("visualizacion")
    public void setVisualizacion(Object visualizacion) {
        this.visualizacion = visualizacion;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}