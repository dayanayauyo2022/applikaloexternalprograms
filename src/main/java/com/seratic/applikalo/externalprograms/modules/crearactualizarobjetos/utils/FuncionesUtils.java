package com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.VOs.WfVariableVo;

import java.util.ArrayList;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.utils.UtilsObjetos.obtenerValorCampoWf;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.JsonUtils.mergeObjectsJson;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.Utils.isNumeric;

public class FuncionesUtils {

    /*Método para extraer variables del script. Se espera que siempre esten desde el inicio hasta un ;*/
    public static final String[] extraerVariables(String script) {
        try {
            String variablesCad = "";
            boolean encontrado = false;
            for (int i = 0; i < script.length() && !encontrado; i++) {
                if (script.charAt(i) == ';') {
                    encontrado = true;
                } else {
                    variablesCad += script.charAt(i);
                }
            }
            variablesCad = variablesCad.replace("var ", "");
            String[] lstVarialbes = variablesCad.split(",");
            if (lstVarialbes != null) {
                for (int i = 0; i < lstVarialbes.length; i++) {
                    lstVarialbes[i] = lstVarialbes[i].trim();
                }
                return lstVarialbes;
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    /*verificar si hay variables wf para obtener valor json*/
    public static final String obtenerVariableWf(JsonNode listaObjetoWf, String nomObjetoBase, String[] variablesLst) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<WfVariableVo> variablesWfLst = null;
            String objetoWf = "wf_" + nomObjetoBase;
            for (int i = 0; i < variablesLst.length; i++) {
                if (variablesLst[i].startsWith(objetoWf)) {
                    JsonNode campoValorJson = obtenerValorCampoWf(listaObjetoWf, variablesLst[i], false);
                    String valor = campoValorJson != null ? campoValorJson.asText() : "";
                    String[] camposRuta = variablesLst[i].split("\\.");
                    if (camposRuta != null && camposRuta.length > 0) {
                        WfVariableVo varWf = new WfVariableVo();
                        varWf.setLlave(variablesLst[i]);
                        varWf.setValor(valor);
                        varWf.setCamposRuta(camposRuta);
                        varWf.setNumCamposRuta(camposRuta.length);
                        if (variablesWfLst == null) {
                            variablesWfLst = new ArrayList<>();
                        }
                        variablesWfLst.add(varWf);
                    }
                }
            }
            if (variablesWfLst != null && variablesWfLst.size() > 0) {
                // ordenar lista variablesWfLst de menor a mayor segun longitud de camposRuta
                variablesWfLst.sort((o1, o2) -> o1.getNumCamposRuta().compareTo(o2.getNumCamposRuta()));

                JsonNode objetoFinal = null;
                for (int i = 0; i < variablesWfLst.size(); i++) {
                    String[] camposRutas = variablesWfLst.get(i).getCamposRuta();
                    JsonNode objetoActual = mapper.createObjectNode();
                    for (int y = camposRutas.length - 1; y >= 0; y--) {
                        if (y == camposRutas.length - 1) {
                            if (variablesWfLst.get(i).getValor() != null && isNumeric(variablesWfLst.get(i).getValor())) {
                                Integer valorNum = Integer.parseInt(variablesWfLst.get(i).getValor());
                                ((ObjectNode) objetoActual).put(camposRutas[y], valorNum);
                            } else {
                                ((ObjectNode) objetoActual).put(camposRutas[y], variablesWfLst.get(i).getValor());
                            }
                        } else {
                            JsonNode objetoTemp = mapper.createObjectNode();
                            ((ObjectNode) objetoTemp).put(camposRutas[y], objetoActual);
                            objetoActual = objetoTemp;
                        }
                    }
                    if (objetoFinal == null) {
                        objetoFinal = objetoActual;
                    } else {
                        objetoFinal = mergeObjectsJson(objetoFinal, objetoActual);
                    }
                }
                if (objetoFinal != null) {
                    String variableWfResult = objetoFinal.get(objetoWf).toString();
                    return variableWfResult;
                }
            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public static final String obtenerOtrasVariables(String[] variablesLst, String nomObjetoBase) {
        String variablesCad = "";
        for (int i = 0; i< variablesLst.length; i++) {
            if (!variablesLst[i].startsWith("wf_" + nomObjetoBase)) {
                variablesCad += "," +variablesLst[i];
            }
        }
        if (!variablesCad.isEmpty()) {
            // quitar primera coma:
            variablesCad = variablesCad.substring(1);
        }
        return variablesCad;
    }

    public static final String reemplazarVariablesWf(String script, String variablesCad) {
        try {
            String scriptNoVariables = "";
            boolean encontrado = false;
            for (int i = 0; i < script.length() && !encontrado; i++) {
                if (script.charAt(i) == ';') {
                    encontrado = true;
                    for (int y = i+1; y < script.length(); y++) {
                        scriptNoVariables += script.charAt(y);
                    }
                }
            }
            String scriptResult = variablesCad + " " + scriptNoVariables;
            return scriptResult;
        } catch (Exception e) {
            return script;
        }
    }

    public static final String reemplazarRetornos(String script) {
        String scriptFinal = "";
        try {
            for (int i = 0; i < script.length(); i++) {
                if (i + 6 < script.length() && script.charAt(i) == 'w' &&
                        script.charAt(i+1) == 'i' && script.charAt(i+2) == 'n' && script.charAt(i+3) == 'd' &&
                        script.charAt(i+4) == 'o' && script.charAt(i+5) == 'w' && script.charAt(i+6) == '.') {
                    String valorRetorno = "";
                    boolean encontrado = false;
                    for (int y = i; y < script.length() && !encontrado; y++) {
                        if (script.charAt(y) == '(') {
                            encontrado = true;
                            boolean terminoValor = false;
                            int indiceFin = 0;
                            for (int z = y+1; z < script.length() && !terminoValor; z ++) {
                                if (script.charAt(z) == ')') {
                                    terminoValor = true;
                                    indiceFin = z;
                                } else {
                                    valorRetorno += script.charAt(z);
                                }
                            }
                            if (indiceFin > 0) {
                                i = indiceFin;
                            }
                        }
                    }
                    scriptFinal += valorRetorno;
                } else {
                    scriptFinal += script.charAt(i);
                }
            }
            return scriptFinal;
        }catch (Exception e) {
            return script;
        }
    }
}
