package com.seratic.applikalo.externalprograms.modules.activosmineros.dto;

import java.util.List;

public class DatosNotificacion {

    private String nombreProyecto;
    private String fechaFueraDePlazo;
    private PasivoDTO pasivo;
    private List<PasivoDTO> pasivos;
    private List<String> campos;

    public DatosNotificacion() {
        // default implementation ignored
    }

    public String getNombreProyecto() {
        return nombreProyecto;
    }

    public void setNombreProyecto(String nombreProyecto) {
        this.nombreProyecto = nombreProyecto;
    }

    public String getFechaFueraDePlazo() {
        return fechaFueraDePlazo;
    }

    public void setFechaFueraDePlazo(String fechaFueraDePlazo) {
        this.fechaFueraDePlazo = fechaFueraDePlazo;
    }

    public PasivoDTO getPasivo() {
        return pasivo;
    }

    public void setPasivo(PasivoDTO pasivo) {
        this.pasivo = pasivo;
    }

    public List<PasivoDTO> getPasivos() {
        return pasivos;
    }

    public void setPasivos(List<PasivoDTO> pasivos) {
        this.pasivos = pasivos;
    }

    public List<String> getCampos() {
        return campos;
    }

    public void setCampos(List<String> campos) {
        this.campos = campos;
    }
}
