/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.modules.prosegur.endpoint;

import com.seratic.applikalo.externalprograms.modules.prosegur.dto.SimBajaDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.service.ProsegurService;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import javax.mail.Flags;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;

/**
 *
 * @author ivang
 */
@MessageEndpoint
public class ImapMessageReceiverEndpoint {

    private static final Logger logger = LoggerFactory.getLogger(ImapMessageReceiverEndpoint.class);
    @Autowired
    private ProsegurService prosegurService;
    @Value("${prosegur.mailDownReceiver.prefijo}")
    private String prefijo;
    @Value("${prosegur.mailDownReceiver.idPublico}")
    private String idPublico;
    @Value("${prosegur.mailDownReceiver.versionAplicacion}")
    private Integer versionAplicacion;
    @Value(value = "${prosegur.mailDownReceiver.versionAplicacionMunicipalidad}")
    private Integer versionAplicacionMunicipalidad;
    @Value("${prosegur.mailDownReceiver.urlWriter}")
    private String urlWriter;
    @Value("${prosegur.mailDownReceiver.urlReader}")
    private String urlReader;

    @ServiceActivator(inputChannel = "imapReceivingChannel")
    public void receiveMessage(Message<MimeMessage> message) {
        try {
            logger.info("receiveMessage, se recibio un correo para baja de sim");
            DataToken dataToken = new DataToken(prefijo, idPublico, versionAplicacion, urlWriter, urlReader);
            MimeMessage mimeMessage = message.getPayload();
            Object content = mimeMessage.getContent();
            if (content instanceof String) {
                List<SimBajaDTO> simBajaDTOs = new ArrayList<>();
                for (StringTokenizer stringTokenizer = new StringTokenizer(((String) content).trim(), "\r\n"); stringTokenizer.hasMoreTokens();) {
                    String icc = stringTokenizer.nextToken().trim();
                    simBajaDTOs.add(new SimBajaDTO(icc));
                }
                if (prosegurService.createDownSIM(simBajaDTOs, dataToken)) {
                    mimeMessage.setFlag(Flags.Flag.SEEN, true);
                }
            }
            logger.info("receiveMessage, se proceso un correo para baja de sim");
        } catch (Exception e) {
            logger.error("receiveMessage fallo", e);
        }
    }

    @ServiceActivator(inputChannel = "eventReceivingChannel")
    public void receiveMessageEvent(Message<MimeMessage> message) {
        try {
            logger.info("receiveMessage, se recibio un correo para persistir evento");
            DataToken dataToken = new DataToken(prefijo, idPublico, versionAplicacionMunicipalidad, urlWriter, urlReader);
            if (message.getPayload().getSize() > 0 && prosegurService.createEvento(dataToken, message.getPayload())) {
                message.getPayload().setFlag(Flags.Flag.SEEN, true);
            }
        } catch (Exception e) {
            logger.error(" ImapMessageReceiverEndpoint  ->  receiveMessageEvent Fallo", e);
        }
    }
}
