package com.seratic.applikalo.externalprograms.modules.entities;

public class Campo {
    private Integer id;
    TipoCampo tipoCampo;
    private Object propietario;
    private String nombre;
    private String etiqueta;
    private boolean llavePrincipal;
    private boolean campoObligatorio;
    private String objetoRelacion = null;
    private boolean enabled;

    public Campo() {
        // default implementation ignored
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoCampo getTipoCampo() {
        return tipoCampo;
    }

    public void setTipoCampo(TipoCampo tipoCampo) {
        this.tipoCampo = tipoCampo;
    }

    public Object getPropietario() {
        return propietario;
    }

    public void setPropietario(Object propietario) {
        this.propietario = propietario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public boolean isLlavePrincipal() {
        return llavePrincipal;
    }

    public void setLlavePrincipal(boolean llavePrincipal) {
        this.llavePrincipal = llavePrincipal;
    }

    public boolean isCampoObligatorio() {
        return campoObligatorio;
    }

    public void setCampoObligatorio(boolean campoObligatorio) {
        this.campoObligatorio = campoObligatorio;
    }

    public String getObjetoRelacion() {
        return objetoRelacion;
    }

    public void setObjetoRelacion(String objetoRelacion) {
        this.objetoRelacion = objetoRelacion;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
