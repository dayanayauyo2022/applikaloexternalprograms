package com.seratic.applikalo.externalprograms.modules.integracion;

import com.fasterxml.jackson.databind.JsonNode;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;

import java.util.List;

public interface IntegrationService {
    boolean ejecutarProgExternosErroneos();
    List<JsonNode> getListaObjetosApiPaginado(DataToken dataToken, String tokenEncript, JsonNode body, String nombreObjeto);
    JsonNode getListaObjetosApi(DataToken dataToken, String tokenEncript, String body, String nombreObjeto);
    JsonNode actualizarObjetoApi(DataToken dataToken, String body, String nombreObjeto, boolean ignorarTipoFlujo);
    JsonNode insertarObjetoApi(DataToken dataToken, String body, String nombreObjeto);
}
