package com.seratic.applikalo.externalprograms.modules.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TipoCampo {
    private Integer id;
    private String nombre;
    @JsonProperty(value = "url_icono")
    private String urlIcono;
    List< Object > campos = null;

    public TipoCampo() {
        // default implementation ignored
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrlIcono() {
        return urlIcono;
    }

    public void setUrlIcono(String urlIcono) {
        this.urlIcono = urlIcono;
    }

    public List<Object> getCampos() {
        return campos;
    }

    public void setCampos(List<Object> campos) {
        this.campos = campos;
    }
}
