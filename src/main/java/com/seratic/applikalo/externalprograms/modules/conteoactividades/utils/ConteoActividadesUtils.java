package com.seratic.applikalo.externalprograms.modules.conteoactividades.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.ArrayList;
import java.util.List;

public class ConteoActividadesUtils {

    /*armar peticion para actualizar objeto en api write*/
    public static final String armarPeticionUpdateApiWrite(String nombreObjetoBase, JsonNode camposObjeto) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<JsonNode> listaObjetos = new ArrayList<>();
            JsonNode objUpdte = mapper.createObjectNode();
            ((ObjectNode) objUpdte).put(nombreObjetoBase, camposObjeto);
            listaObjetos.add(objUpdte);

            JsonNode objToSave = mapper.createObjectNode();
            ((ObjectNode) objToSave).putArray("objetos").addAll(listaObjetos);
            return objToSave.toString();
        } catch (Exception e) {
            return null;
        }
    }

    /*generar llave primaria de objeto conteo*/
    public static final String generarPkObjetoConteo(String ambienteObj, String prefijo, Integer version, String nombreObjeto, String idObjeto) {
        try {
            return ambienteObj + "|" + prefijo + "|" + version + "|" + nombreObjeto + "|" + idObjeto;
        } catch (Exception e) {
            return "";
        }
    }

    /*generar llave primaria de objetos en mongo*/
    public static final String generarPkObjetoMongo(String key, String prefijo, Integer version, String nombreObjeto, String idObjeto) {
        try {
            return key + "|" + prefijo + "|" + version + "|" + nombreObjeto + "|" + idObjeto;
        } catch (Exception e) {
            return "";
        }
    }

    /*Método para crear body de peticion getObjeto en el api go*/
    public static final String armarPeticioBuscarObjeto(String nombreObjeto, String idObjeto, boolean isUsuario) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();
            JsonNode filtro = mapper.createObjectNode();
            ((ObjectNode) filtro).put("campo", "id");
            ((ObjectNode) filtro).put("condicional", "=");
            ((ObjectNode) filtro).put("objeto", nombreObjeto);
            ((ObjectNode) filtro).put("parametro", idObjeto);
            filtros.add(filtro);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1);
            ((ObjectNode) objetoPeticion).put("objeto", nombreObjeto);

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campoNodo = mapper.createObjectNode();
            ((ObjectNode) campoNodo).put("campo", "id");
            campos.add(campoNodo);

            if (isUsuario) {
                JsonNode campoUsername = mapper.createObjectNode();
                ((ObjectNode) campoUsername).put("campo", "username");
                campos.add(campoUsername);

                JsonNode campoNombres = mapper.createObjectNode();
                ((ObjectNode) campoNombres).put("campo", "nombres");
                campos.add(campoNombres);

                JsonNode campoEstado = mapper.createObjectNode();
                ((ObjectNode) campoEstado).put("campo", "estado");
                campos.add(campoEstado);

                JsonNode campoPerfil = mapper.createObjectNode();
                ((ObjectNode) campoPerfil).put("campo", "perfil.id");
                campos.add(campoPerfil);
            }

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);

            return objetoPeticion.toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static final List<String> obtenerCamposParaPeticionGetObjetoApi(String nombreObjeto) {
        List<String> campos = new ArrayList<>();
        campos.add("nodo");
        campos.add(nombreObjeto + ".id");
        campos.add("registroactividad.tipoactividad.contar");
        campos.add("registroactividad.tipoactividad.nombre_contador");
        campos.add("fecha_creacion");
        return campos;
    }
}
