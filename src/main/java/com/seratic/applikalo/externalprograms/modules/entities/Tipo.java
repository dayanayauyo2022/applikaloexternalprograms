package com.seratic.applikalo.externalprograms.modules.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Tipo {

    private Integer id;
    private String nombre;
    private String descripcion;
    @JsonProperty(value = "url_icono")
    private String urlIcono;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrlIcono() {
        return urlIcono;
    }

    public void setUrlIcono(String urlIcono) {
        this.urlIcono = urlIcono;
    }
}
