package com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos;

import com.seratic.applikalo.externalprograms.modules.integracion.VOs.RespuestaVO;

public interface CrearActualizarObjetosService {
    public RespuestaVO crearActualizarObjeto(boolean isCreacion, String objetoRequest, String prefijo, Integer versionApp,
                                             String idPublico, String urlWrite, String urlRead);
}
