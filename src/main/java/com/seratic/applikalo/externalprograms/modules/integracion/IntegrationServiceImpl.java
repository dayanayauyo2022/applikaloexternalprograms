package com.seratic.applikalo.externalprograms.modules.integracion;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.entity.ProgExternoError;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.service.ProgExternoErrorService;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.integracion.Constants.*;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.Utils.urlBaseProgExternos;
import static com.seratic.applikalo.externalprograms.modules.security.utils.TokenManagerUtil.generarTokenProgExternos;

@Service
public class IntegrationServiceImpl implements IntegrationService {

    private static final Logger logger = LoggerFactory.getLogger(IntegrationServiceImpl.class);

    @Autowired
    ProgExternoErrorService progExternoErrorService;

    @Autowired
    IntegracionRepository integracionRepository;

    @Override
    public boolean ejecutarProgExternosErroneos() {
        try {
            List<ProgExternoError> progExternoErrorLst = progExternoErrorService.getAllProgExternoError();
            if (progExternoErrorLst != null && progExternoErrorLst.size() > 0) {
                for (ProgExternoError progExternoError : progExternoErrorLst) {
                    try {
                        if (progExternoError.getNumeroEjecuciones() != null && progExternoError.getNumeroEjecuciones() <= NUM_EJECUCIONES_PROG_EXTERNOS_ERRONEOS) {
                            String url = urlBaseProgExternos(progExternoError.getPrefijo()) + progExternoError.getUrl();
                            DataToken dataToken = new DataToken(progExternoError.getPrefijo(), progExternoError.getIdPublicoUsuario(),
                                    progExternoError.getVersionApp(), progExternoError.getUrlApiWrite(), progExternoError.getUrlApiRead());
                            String token = generarTokenProgExternos(dataToken);
                            HttpHeaders headers = new HttpHeaders();
                            headers.setContentType(MediaType.APPLICATION_JSON);
                            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
                            headers.add("token", token);
                            if (progExternoError.getTipoPeticion().equals(TIPO_PETICION_POST)) {
                                integracionRepository.getResponseEntityPOST(url, headers, progExternoError.getBody());
                            } else if (progExternoError.getTipoPeticion().equals(TIPO_PETICION_GET)) {
                                integracionRepository.getResponseEntityGET(url, headers);
                            }
                        } else {
                            progExternoErrorService.deleteProgExternoError(progExternoError);
                        }
                    } catch (Exception e) {
                        logger.info("**** ERROR cron progExterno Mongo: " + progExternoError.getId());
                    }
                }
            }
            return true;
        } catch (Exception e) {
            logger.info("**** ERROR cron ejecutarProgExternosErroneos: " + e.getMessage());
            return false;
        }
    }

    @Override
    public List<JsonNode> getListaObjetosApiPaginado(DataToken dataToken, String tokenEncript, JsonNode body, String nombreObjeto) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ResponseEntity<String> responseGetObjeto = integracionRepository.getObjetoApi(dataToken, tokenEncript, body.toString());
            ObjectMapper mapper = new ObjectMapper();
            List<JsonNode> resultTotal = new ArrayList<>();
            if (responseGetObjeto.getBody() != null && !responseGetObjeto.getBody().isEmpty() && responseGetObjeto.getBody().startsWith("{")
                    && responseGetObjeto.getBody().endsWith("}")) {
                JsonNode nodoRespGetObjeto = mapper.readTree(responseGetObjeto.getBody());
                if (nodoRespGetObjeto.get("data") != null && !nodoRespGetObjeto.get("data").isNull()) {
                    for (JsonNode nodoResp : nodoRespGetObjeto.get("data")) {
                        resultTotal.add(nodoResp);
                    }
                    Integer total = nodoRespGetObjeto.get("total").asInt();
                    if (total > nodoRespGetObjeto.get("data").size()) {
                        Integer numIteraciones = (total / nodoRespGetObjeto.get("data").size()) - 1;
                        Integer page = 2;
                        for (int i = 0; i < numIteraciones; i++) {
                            ((ObjectNode) body).put("page", page);
                            ResponseEntity<String> responseGetObjetoIterator = integracionRepository.getObjetoApi(dataToken, tokenEncript, body.toString());
                            JsonNode nodoResponseGetObjetoIterator = mapper.readTree(responseGetObjetoIterator.getBody());
                            if (nodoResponseGetObjetoIterator.get("data") != null && !nodoResponseGetObjetoIterator.get("data").isNull()) {
                                for (JsonNode nodoRespIterator : nodoResponseGetObjetoIterator.get("data")) {
                                    resultTotal.add(nodoRespIterator);
                                }
                                page += 1;
                            } else {
                                errList.addError("10.1", "Error obteniendo lista paginada Itercion" + nombreObjeto + " api", "resp:" + responseGetObjeto.getBody());
                                throw errList;
                            }
                        }
                    }
                    return resultTotal;
                } else {
                    errList.addError("10.1", "Error obteniendo lista paginada " + nombreObjeto + " api", "resp:" + responseGetObjeto.getBody());
                    throw errList;
                }
            } else {
                errList.addError("10.1", "Error obteniendo lista paginada " + nombreObjeto + " api", "resp:" + responseGetObjeto.getBody());
                throw errList;
            }
        } catch (Exception e) {
            errList.addError("10.1", "Error obteniendo lista paginada " + nombreObjeto + " api", "ex: " + e.getMessage());
            throw errList;
        }
    }

    @Override
    public JsonNode getListaObjetosApi(DataToken dataToken, String tokenEncript, String body, String nombreObjeto) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ResponseEntity<String> responseGetObjeto = integracionRepository.getObjetoApi(dataToken, tokenEncript, body);
            ObjectMapper mapper = new ObjectMapper();
            if (responseGetObjeto.getBody() != null && !responseGetObjeto.getBody().isEmpty() && responseGetObjeto.getBody().startsWith("{")
                    && responseGetObjeto.getBody().endsWith("}")) {
                JsonNode nodoRespGetObjeto = mapper.readTree(responseGetObjeto.getBody());
                if (nodoRespGetObjeto.get("data") != null && !nodoRespGetObjeto.get("data").isNull()) {
                    JsonNode listaResult = nodoRespGetObjeto.get("data");
                    return listaResult;
                } else {
                    errList.addError("10.1", "Error obteniendo lista " + nombreObjeto + " api", "resp:" + responseGetObjeto.getBody());
                    throw errList;
                }
            } else {
                errList.addError("10.1", "Error obteniendo lista " + nombreObjeto + " api", "resp:" + responseGetObjeto.getBody());
                throw errList;
            }
        } catch (Exception e) {
            errList.addError("10.1", "Error obteniendo lista " + nombreObjeto + " api", "ex: " + e.getMessage());
            throw errList;
        }
    }

    @Override
    public JsonNode actualizarObjetoApi(DataToken dataToken, String body, String nombreObjeto, boolean ignorarTipoFlujo) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ResponseEntity<String> responseUpdate = integracionRepository.updateServicioUpdateApigo(dataToken, body, ignorarTipoFlujo, true, true);
            ObjectMapper mapper = new ObjectMapper();
            if (responseUpdate.getBody() != null && !responseUpdate.getBody().isEmpty() && responseUpdate.getBody().startsWith("{")
                    && responseUpdate.getBody().endsWith("}")) {
                JsonNode nodoRespUpdateObjeto = mapper.readTree(responseUpdate.getBody());
                if (nodoRespUpdateObjeto != null && !nodoRespUpdateObjeto.isNull()) {
                    return nodoRespUpdateObjeto;
                } else {
                    errList.addError("10.1", "Error actualizando " + nombreObjeto + " api", "resp:" + responseUpdate.getBody());
                    throw errList;
                }
            } else {
                errList.addError("10.1", "Error actualizando " + nombreObjeto + " api", "resp:" + responseUpdate.getBody());
                throw errList;
            }
        } catch (Exception e) {
            errList.addError("10.1", "Error actualizando " + nombreObjeto + " api", "ex:" + e.getMessage());
            throw errList;
        }
    }

    @Override
    public JsonNode insertarObjetoApi(DataToken dataToken, String body, String nombreObjeto) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ResponseEntity<String> responseInsert = integracionRepository.insertApi(dataToken, body);
            ObjectMapper mapper = new ObjectMapper();
            if (responseInsert.getBody() != null && !responseInsert.getBody().isEmpty() && responseInsert.getBody().startsWith("[")
                    && responseInsert.getBody().endsWith("]")) {
                JsonNode nodoRespInsertObjeto = mapper.readTree(responseInsert.getBody());
                if (nodoRespInsertObjeto != null && !nodoRespInsertObjeto.isNull()) {
                    return nodoRespInsertObjeto;
                } else {
                    errList.addError("10.1", "Error insertar " + nombreObjeto + " api", "resp:" + responseInsert.getBody());
                    throw errList;
                }
            } else {
                errList.addError("10.1", "Error insertar " + nombreObjeto + " api", "resp:" + responseInsert.getBody());
                throw errList;
            }
        } catch (Exception e) {
            errList.addError("10.1", "Error insertar " + nombreObjeto + " api", "ex:" + e.getMessage());
            throw errList;
        }
    }
}
