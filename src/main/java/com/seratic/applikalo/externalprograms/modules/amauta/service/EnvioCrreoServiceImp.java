package com.seratic.applikalo.externalprograms.modules.amauta.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.seratic.applikalo.externalprograms.modules.amauta.utils.AmautaUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;


@Service
public class EnvioCrreoServiceImp {

    @Autowired
    private ObtenerDataAmautaClaseImp obtenerData;
    @Autowired
    private EmpresaEmaService empresaEmaService;
    private static final DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
    private static final DateTimeFormatter outFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final String CORREO = "correo";
    private static final String NOMBRE = "nombre";
    private static final String FECHA = "fecha";

    public void envioCorreo(){
        LocalDate currentDate = LocalDate.now();
        Map<String, String> datosCorreo = new HashMap<>(3);
        AtomicReference<String> fecha = new AtomicReference<>();
        AtomicReference<Long> numDias = new AtomicReference<>();
        AtomicReference<LocalDate> fechaClase = new AtomicReference<>();
        obtenerData.obtenerEstudianteCurso().stream().forEach(estudianteCurso -> {
            obtenerData.obtenerClasesEnVivo().stream().forEach(claseEnVivo -> {
                LocalDate fechaSinFoprmato = LocalDate.parse((String) claseEnVivo.get(FECHA), inputFormat);
                fechaClase.set(LocalDate.parse(outFormat.format(fechaSinFoprmato), outFormat));
                numDias.set(ChronoUnit.DAYS.between(currentDate, fechaClase.get()));
                obtenerData.obtenerUsuarios().stream().forEach(usuario -> {
                    validarEstudiantesCurso(datosCorreo, fecha, numDias, fechaClase, estudianteCurso, claseEnVivo, usuario);
                });
            });
        });
    }

    private void validarEstudiantesCurso(Map<String, String> datosCorreo, AtomicReference<String> fecha, AtomicReference<Long> numDias, AtomicReference<LocalDate> fechaClase, Map<String, Object> estudianteCurso, Map<String, Object> claseEnVivo, Map<String, Object> usuario) {
        if (usuario.get("estado").equals("true") && estudianteCurso.get("idUsuario").equals(usuario.get("id")) && claseEnVivo.get("idPeriodo").equals(estudianteCurso.get("idPeriodo")) && claseEnVivo.get("idCurso").equals(estudianteCurso.get("idCurso")) && estudianteCurso.get("activo").equals(Boolean.TRUE)){
            final List<String> correos = new ArrayList<>();
            fecha.set((String) claseEnVivo.get(FECHA));
            datosCorreo.put(FECHA, (String) claseEnVivo.get(FECHA));
            datosCorreo.put(NOMBRE, claseEnVivo.get(NOMBRE).toString());
            datosCorreo.put("link", claseEnVivo.get("link").toString());
            if (usuario.get(CORREO) != null && usuario.get(CORREO) != "") {
                correos.add(usuario.get(CORREO).toString());
                try {
                    evaluarDias(numDias.get(), fechaClase.get(), correos, datosCorreo);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void evaluarDias(long numDias, LocalDate fechaClase, List<String> correos, Map<String, String> datosCorreo) throws JsonProcessingException, javax.mail.MessagingException {
        int dia;
        switch ((int) numDias){
            case 0:
                dia = 0;
                armarCorreoDatos(fechaClase, correos, datosCorreo, dia);
                break;
            case 1:
                dia = 1;
                armarCorreoDatos(fechaClase, correos, datosCorreo, dia);
                break;
            case 2:
                dia = 2;
                armarCorreoDatos(fechaClase, correos, datosCorreo, dia);
                break;
            default:
                break;
        }
    }

    private void armarCorreoDatos(LocalDate fechaClase, List<String> correos, Map<String, String> datosCorreo, int dia) throws JsonProcessingException, javax.mail.MessagingException {
        String htmlCuerpoCorreo;
        htmlCuerpoCorreo = "<html>" +
                "<body>" +
                "<ul>" +
                "<li>" + "N&uacute;mero de d&iacute;as que faltan: " + dia + "</li>" +
                "<li>" + "Fecha de conferencia: " + outFormat.format(fechaClase) + "</li>" +
                "<li>" + "Nombre de la conferencia: " + datosCorreo.get(NOMBRE) + "</li>" +
                "<li>" + "Link de la conferencia: " + "<a href='" + datosCorreo.get("link") + "'>" + "link" + "</a>" + "</li>" +
                "</ul>" +
                "</body>" +
                "</html>";
        enviarCorreo(correos, htmlCuerpoCorreo);
    }

    private void enviarCorreo(List<String> correos, String htmlCuerpo) throws MessagingException, JsonProcessingException, javax.mail.MessagingException {
        JsonNode empresaAmauta = empresaEmaService.obtenerEmpresaById();
        Map<String, Object> autenticacionCorreo = new ObjectMapper().readValue(empresaAmauta.get("configuracionCorreo").textValue(), Map.class);
        JavaMailSenderImpl configEmail = AmautaUtils.configurarPropiedadesEmail(autenticacionCorreo.get("mail-smtp-username").toString(), autenticacionCorreo.get("mail-smtp-password").toString());
        MimeMessage msg = configEmail.createMimeMessage();
        AmautaUtils.armarCorreo(msg, correos.toArray(new String[]{}), "Correo recordatorio de lección en vivo", htmlCuerpo, null, null);
        configEmail.send(msg);
    }

}
