package com.seratic.applikalo.externalprograms.modules.activosmineros.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.activosmineros.dto.DatosNotificacion;
import com.seratic.applikalo.externalprograms.modules.entities.Componente;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.apiGo.Where;

import java.time.LocalDate;
import java.util.List;

public interface ArmarNotificacion {

    void armarNotificacionProyectosNoSupervisados();
    void validarFechaSupervicionProyectoNuevo(List<JsonNode> protectosNoSupervicionNuevos);
    void validarFechaSupervisionProyectos(List<ObjectNode> proyectosNoSupervision);
    void armarDataNotificacionProyectosFueraDePlazo(List<JsonNode> proyectos, LocalDate currentDate);
    void armarNotificacionProyectoCamposVacios(Where filtro, String idPublicoOwner, String prefijo);
    void armarNoitificacionProyectosCamposVacios();
    ObjectNode aramarPlantillaNotifcacionWebyCorreo(DatosNotificacion datosNotificacion, int caso);
    ObjectNode procesarAtributosComponeteNodo(Componente componenteNodo, StringBuilder cuerpoNotificacion, int caso, String nombreProyecto);
    void enviarNotificacionKafka(JsonNode proyecto, ObjectNode atributosotificacion, String prefijo);
}
