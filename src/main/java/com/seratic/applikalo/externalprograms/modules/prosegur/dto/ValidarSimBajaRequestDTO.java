/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.modules.prosegur.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author ivang
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ValidarSimBajaRequestDTO {

    private SimBajaDownDTO objeto;

    public SimBajaDownDTO getObjeto() {
        return objeto;
    }

    public void setObjeto(SimBajaDownDTO objeto) {
        this.objeto = objeto;
    }
}
