//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.23 a las 12:16:31 PM COT 
//
package com.seratic.applikalo.externalprograms.modules.solgas.soap.gen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para ubicacion complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="ubicacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lng" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ubicacion", propOrder = {
    "lat",
    "lng"
})
public class Ubicacion {

    protected String lat;
    protected String lng;

    /**
     * Obtiene el valor de la propiedad lat.
     *
     * @return possible object is {@link String }
     *
     */
    public String getLat() {
        return lat;
    }

    /**
     * Define el valor de la propiedad lat.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setLat(String value) {
        this.lat = value;
    }

    /**
     * Obtiene el valor de la propiedad lng.
     *
     * @return possible object is {@link String }
     *
     */
    public String getLng() {
        return lng;
    }

    /**
     * Define el valor de la propiedad lng.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setLng(String value) {
        this.lng = value;
    }

}
