package com.seratic.applikalo.externalprograms.modules.entities;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Componente {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("campo")
    private Campo campo;
    @JsonProperty("campoObjetoRelacion")
    private Object campoObjetoRelacion;
    @JsonProperty("fuenteDatos")
    private FuenteDatos fuenteDatos;
    @JsonProperty("funcion")
    private Object funcion;
    @JsonProperty("tipoComponente")
    private TipoComponente tipoComponente;
    @JsonProperty("orden")
    private Integer orden;
    @JsonProperty("nombre")
    private String nombre;
    @JsonProperty("valor")
    private String valor;
    @JsonProperty("disenio")
    private Object disenio;
    @JsonProperty("atributos")
    private String atributos;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("tipoVisualizacionDinamica")
    private Object tipoVisualizacionDinamica;
    @JsonProperty("componentes")
    private List<Componente> componentes;
    @JsonProperty("acciones")
    private List<Accion> acciones = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public Componente() {
        // default implementation ignored
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("campo")
    public Campo getCampo() {
        return campo;
    }

    @JsonProperty("campo")
    public void setCampo(Campo campo) {
        this.campo = campo;
    }

    @JsonProperty("campoObjetoRelacion")
    public Object getCampoObjetoRelacion() {
        return campoObjetoRelacion;
    }

    @JsonProperty("campoObjetoRelacion")
    public void setCampoObjetoRelacion(Object campoObjetoRelacion) {
        this.campoObjetoRelacion = campoObjetoRelacion;
    }

    public FuenteDatos getFuenteDatos() {
        return fuenteDatos;
    }

    public void setFuenteDatos(FuenteDatos fuenteDatos) {
        this.fuenteDatos = fuenteDatos;
    }

    @JsonProperty("funcion")
    public Object getFuncion() {
        return funcion;
    }

    @JsonProperty("funcion")
    public void setFuncion(Object funcion) {
        this.funcion = funcion;
    }

    @JsonProperty("tipoComponente")
    public TipoComponente getTipoComponente() {
        return tipoComponente;
    }

    @JsonProperty("tipoComponente")
    public void setTipoComponente(TipoComponente tipoComponente) {
        this.tipoComponente = tipoComponente;
    }

    @JsonProperty("orden")
    public Integer getOrden() {
        return orden;
    }

    @JsonProperty("orden")
    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    @JsonProperty("nombre")
    public String getNombre() {
        return nombre;
    }

    @JsonProperty("nombre")
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @JsonProperty("valor")
    public String getValor() {
        return valor;
    }

    @JsonProperty("valor")
    public void setValor(String valor) {
        this.valor = valor;
    }

    @JsonProperty("disenio")
    public Object getDisenio() {
        return disenio;
    }

    @JsonProperty("disenio")
    public void setDisenio(Object disenio) {
        this.disenio = disenio;
    }

    @JsonProperty("atributos")
    public String getAtributos() {
        return atributos;
    }

    @JsonProperty("atributos")
    public void setAtributos(String atributos) {
        this.atributos = atributos;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("tipoVisualizacionDinamica")
    public Object getTipoVisualizacionDinamica() {
        return tipoVisualizacionDinamica;
    }

    @JsonProperty("tipoVisualizacionDinamica")
    public void setTipoVisualizacionDinamica(Object tipoVisualizacionDinamica) {
        this.tipoVisualizacionDinamica = tipoVisualizacionDinamica;
    }

    public List<Componente> getComponentes() {
        return componentes;
    }

    public void setComponentes(List<Componente> componentes) {
        this.componentes = componentes;
    }

    @JsonProperty("acciones")
    public List<Accion> getAcciones() {
        return acciones;
    }

    @JsonProperty("acciones")
    public void setAcciones(List<Accion> acciones) {
        this.acciones = acciones;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
