package com.seratic.applikalo.externalprograms.modules.activosmineros.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Service
public class ConsultaHttpImp implements ConsultaHttp{

    private static final Log LOG = LogFactory.getLog(ConsultaHttpImp.class);
    private static String marcaLOG = " Fallo la consulta URL: ";

    @Override
    public <T> T getHttp(String url, Class<T> classResponse) {
        T retorno = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            HttpEntity<String> request = new HttpEntity<>(null, headers);
            T responseEntity = new RestTemplate().exchange(url, HttpMethod.GET, request, classResponse).getBody();
            if (Objects.nonNull(responseEntity)) {
                retorno = responseEntity;
            } else {
                LOG.error(marcaLOG + url);
                return null;
            }
        } catch (Exception e) {
            LOG.error(marcaLOG + url, e);
        }
        return retorno;
    }

    @Override
    public <T> T postHttp(String url, Class<T> classResponse, Object body) {
        T retorno = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            HttpEntity<Object> request = new HttpEntity<>(body, headers);
            T responseEntity = new RestTemplate().exchange(url, HttpMethod.POST, request, classResponse).getBody();
            if (Objects.nonNull(responseEntity)) {
                retorno = responseEntity;
            } else {
                LOG.error(marcaLOG + url);
                return null;
            }
        } catch (Exception e) {
            LOG.error(marcaLOG + url, e);
        }
        return retorno;
    }
}
