package com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient;

import org.apache.http.auth.UsernamePasswordCredentials;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.Default;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

@Configuration
public class CanalizadoConfigClient {

    @Value(value = "${solgas.insertSolgasSOAP.wsdl-MedidorReconexion}")
    private String urlWsdl;
    @Value(value = "${solgas.insertSolgasSOAP.user}")
    private String userNameWSDL;
    @Value(value = "${solgas.insertSolgasSOAP.password}")
    private String passwordWSDL;

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.seratic.applikalo.externalprograms.modules.solgas.canalizadoclient");
        return marshaller;
    }

    @Bean
    public CanalizadoClient canalizadoClient(Jaxb2Marshaller marshaller) {
        CanalizadoClient client = new CanalizadoClient();
        client.setDefaultUri(urlWsdl);
        client.setMarshaller(marshaller);
        client.getWebServiceTemplate().setMessageSender(defaultMwMessageSender());
        client.setUnmarshaller(marshaller);
        return client;
    }

    @Bean
    public HttpComponentsMessageSender defaultMwMessageSender() {
        HttpComponentsMessageSender messageSender = new HttpComponentsMessageSender();
        messageSender.setCredentials(new UsernamePasswordCredentials(userNameWSDL, passwordWSDL));
        return messageSender;
    }
}
