package com.seratic.applikalo.externalprograms.modules.activosmineros.repositorio;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.activosmineros.dto.ConstanteDTO;
import com.seratic.applikalo.externalprograms.modules.activosmineros.dto.PasivoDTO;
import com.seratic.applikalo.externalprograms.modules.entities.Componente;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.apiGo.Where;

import java.util.List;

public interface ObtenerData {

    List<JsonNode> obtenerVerificacionProyecto();
    ObjectNode obtenerProyectoPorKey(Where filtro, String idPublicoOwner);
    List<JsonNode> obtenerProyectos();
    List<PasivoDTO> obtenerPasivos();
    ConstanteDTO obtenerConstantePorLlaveEmaData(String llave);
    Componente obtenerComponenteNode(String nombreNodo, Integer versionApp);
    Boolean actualizarAtributosComponente(ObjectNode obj);
    Componente obtenerComponentePorId(Integer idComponente);
}
