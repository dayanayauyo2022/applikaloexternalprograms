package com.seratic.applikalo.externalprograms.modules.conteoactividades;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.integracion.IntegracionRepository;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.RespuestaVO;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.entity.ObjetoConteo;
import com.seratic.applikalo.externalprograms.modules.integracion.mongodb.service.ObjetoConteoService;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.conteoactividades.utils.ConteoActividadesUtils.*;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.FechasUtils.getFechaDiaAnterior;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.Utils.getAmbiente;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.Utils.verificarElementoEnListado;
import static com.seratic.applikalo.externalprograms.modules.security.utils.TokenManagerUtil.generarTokenGetObjeto;
import static com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.utils.UtilsObjetos.armarPeticionGetObjetoWfApi;

@Service
public class ConteoActividadesServiceImpl implements ConteoActividadesService {

    private static final Logger logger = LoggerFactory.getLogger(ConteoActividadesServiceImpl.class);

    @Autowired
    private IntegracionRepository integracionRepository;

    @Autowired
    private ObjetoConteoService objetoConteoService;

    @Override
    public RespuestaVO contarActividades(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        // consultar ultima labor y verificar si el nodo siguiente es actividad:
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
            JsonNode objetoBase = objetoRequestJson.get("objeto");
            String nombreObjeto = objetoRequestJson.get("nombreObjeto").asText();
            String idObjeto = objetoBase.get("id").asText();
            List<String> campos = obtenerCamposParaPeticionGetObjetoApi(nombreObjeto);
            String bodyGetObjeto = armarPeticionGetObjetoWfApi(nombreObjeto, idObjeto, "", campos, true, 1, 0, 100);
            DataToken dataTokenApiRead = new DataToken(prefijo, idPublico, versionApp, urlWrite, urlRead);
            String tokenEncriptApiRead = generarTokenGetObjeto(dataTokenApiRead);
            ResponseEntity<String> responseGetObjeto = integracionRepository.getObjetoApi(dataTokenApiRead, tokenEncriptApiRead, bodyGetObjeto);
            JsonNode nodoRespGetObjeto = mapper.readTree(responseGetObjeto.getBody());
            if (nodoRespGetObjeto.get("statusCode") != null && !nodoRespGetObjeto.get("statusCode").asText().equals("200")) {
                errList.addError("412", "Tabla wf", "No fue posible obtner nodo finalizar labor: " + nombreObjeto);
                throw errList;
            }
            JsonNode listaNodosWf = nodoRespGetObjeto.get("data");
            // buscar ultimo nodo actividad y verificar que el siguiente sea fin labor en orden descendente
            if (listaNodosWf != null && listaNodosWf.size() > 0) {
                int llegoAfinLabor = -1;
                List<JsonNode> nodosWfContarList = new ArrayList<>();
                for (int i = 0; i < listaNodosWf.size() && llegoAfinLabor == -1; i++) {
                    if (listaNodosWf.get(i).get("nodo").asText().equals("Labor finalizada")) {
                        for (int y = i + 1; y < listaNodosWf.size() && llegoAfinLabor == -1; y++) {
                            if (listaNodosWf.get(y).get("nodo").asText().equals("Finalizar labor")) {
                                llegoAfinLabor = 1;
                            } else {
                                if (listaNodosWf.get(y).get("nodo").asText().contains("registroactividad")
                                        || listaNodosWf.get(y).get("nodo").asText().contains("oportunidad")) {
                                    nodosWfContarList.add(listaNodosWf.get(y));
                                }
                            }
                        }
                    }
                }
                if (nodosWfContarList.size() > 0) {
                    for (JsonNode nodoWfContar : nodosWfContarList) {
                        // consultar registro en mongodb para actualizar el objeto o crear un nuevo registro si no existe.
                        String ambienteObj = getAmbiente(urlRead);
                        String pkObjetoConteo = generarPkObjetoConteo(ambienteObj, prefijo, versionApp, nombreObjeto, idObjeto);
                        ObjetoConteo objetoConteo = objetoConteoService.getObjetoConteoById(pkObjetoConteo);
                        List<String> contadores = new ArrayList<>();
                        if (objetoConteo == null) {
                            objetoConteo = new ObjetoConteo();
                            objetoConteo.setId(pkObjetoConteo);
                            objetoConteo.setIdObjetoApi(idObjeto);
                            objetoConteo.setNombre(nombreObjeto);
                            objetoConteo.setPrefijo(prefijo);
                            objetoConteo.setVersion(versionApp);
                            objetoConteo.setUrlApiRead(urlRead);
                            objetoConteo.setUrlApiWrite(urlWrite);
                        } else {
                            if (objetoConteo.getContadores() != null) {
                                contadores.addAll(objetoConteo.getContadores());
                            }
                        }
                        objetoConteo.setIdPublicoUsuario(idPublico);

                        if (nodoWfContar.get("nodo").asText().contains("registroactividad")) {
                            boolean contar = false;
                            JsonNode objetoTipoActividad = nodoWfContar.get("registroactividad").get("tipoactividad");
                            if (objetoTipoActividad.get("contar") != null && !objetoTipoActividad.get("contar").isNull() && objetoTipoActividad.get("contar").asBoolean()) {
                                contar = objetoTipoActividad.get("contar").asBoolean();
                            }
                            if (contar) {
                                if (objetoTipoActividad.get("nombre_contador") != null && objetoTipoActividad.get("nombre_contador") != null) {
                                    if (!verificarElementoEnListado(objetoTipoActividad.get("nombre_contador").asText(), contadores)) {
                                        contadores.add(objetoTipoActividad.get("nombre_contador").asText());
                                    }
                                    String campoContadorDia = objetoTipoActividad.get("nombre_contador").asText() + "_dia";
                                    String campoContadorMes = objetoTipoActividad.get("nombre_contador").asText() + "_mes";
                                    Integer valorContadorDia = 1;
                                    Integer valorContadorMes = 1;
                                    try {
                                        if (objetoBase.get(campoContadorDia) != null && !objetoBase.get(campoContadorDia).isNull()) {
                                            valorContadorDia += objetoBase.get(campoContadorDia).asInt();
                                            valorContadorMes += objetoBase.get(campoContadorMes).asInt();
                                        }
                                    } catch (Exception e) {
                                    }

                                    JsonNode objetoConteoUpdate = mapper.createObjectNode();
                                    ((ObjectNode) objetoConteoUpdate).put("id", idObjeto);
                                    ((ObjectNode) objetoConteoUpdate).put(campoContadorDia, valorContadorDia);
                                    ((ObjectNode) objetoConteoUpdate).put(campoContadorMes, valorContadorMes);

                                    String bodyUpdateObject = armarPeticionUpdateApiWrite(nombreObjeto, objetoConteoUpdate);
                                    ResponseEntity<String> respUpdateObject = integracionRepository.updateApi(dataTokenApiRead, bodyUpdateObject, true);
                                    JsonNode nodoRespUpdate = mapper.readTree(respUpdateObject.getBody());
                                    if (nodoRespUpdate.get("statusCode") != null && !nodoRespUpdate.get("statusCode").asText().equals("200")) {
                                        errList.addError("412", "UPDATE API WRITE", "No fue posible actualizar contador de objeto: " + nombreObjeto);
                                        throw errList;
                                    }
                                }
                            }
                        } else if (nodoWfContar.get("nodo").asText().contains("oportunidad")) {
                            if (!verificarElementoEnListado("oportunidades", contadores)) {
                                contadores.add("oportunidades");
                            }
                            // contar oportunidades
                            Integer valorContadorOportunidadesDia = 1;
                            Integer valorContadorOportunidadesMes = 1;
                            try {
                                if (objetoBase.get("oportunidades_dia") != null && !objetoBase.get("oportunidades_dia").isNull()) {
                                    valorContadorOportunidadesDia += objetoBase.get("oportunidades_dia").asInt();
                                    valorContadorOportunidadesMes += objetoBase.get("oportunidades_mes").asInt();
                                }
                            } catch (Exception e) {
                            }
                            JsonNode objetoConteoUpdate = mapper.createObjectNode();
                            ((ObjectNode) objetoConteoUpdate).put("id", idObjeto);
                            ((ObjectNode) objetoConteoUpdate).put("oportunidades_dia", valorContadorOportunidadesDia);
                            ((ObjectNode) objetoConteoUpdate).put("oportunidades_mes", valorContadorOportunidadesMes);

                            String bodyUpdateObject = armarPeticionUpdateApiWrite(nombreObjeto, objetoConteoUpdate);
                            ResponseEntity<String> respUpdateObject = integracionRepository.updateApi(dataTokenApiRead, bodyUpdateObject, true);
                            JsonNode nodoRespUpdate = mapper.readTree(respUpdateObject.getBody());
                            if (nodoRespUpdate.get("statusCode") != null && !nodoRespUpdate.get("statusCode").asText().equals("200")) {
                                errList.addError("412", "UPDATE API WRITE", "No fue posible actualizar contador de objeto: " + nombreObjeto);
                                throw errList;
                            }
                        }
                        objetoConteo.setContadores(contadores);
                        objetoConteoService.saveUpdateObjetoConteo(objetoConteo);
                    }
                }
            }
        } catch (MyErrorListException e) {
            if (e.getErrors() != null) {
                errList.addErrorList(e.getErrors());
            }
            if (errList.getErrors() == null || (errList.getErrors() != null && errList.getErrors().size() == 0)) {
                errList.addError("412", "Error conteo de actividades", e.getMessage());
            }
            logger.info("error al contar actividades: " + errList.getErrors().get(0).getMessage());
            throw errList;
        } catch (Exception e) {
            errList.addError("412", "Error conteo de actividades", e.getMessage());
            logger.info("Error conteo de actividades: " + e.getMessage());
        }

        if (errList.getErrors().size() > 0) {
            logger.info("error aconteo de actividades: " + errList.getErrors().get(0).getMessage());
            throw errList;
        } else {
            logger.info("****EXITO. CONTEO DE ACTIVIDADES ***");
            RespuestaVO response = new RespuestaVO();
            response.setCode("200");
            response.setMessage("success");
            return response;
        }
    }

    @Override
    public void reiniciarContadores() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<ObjetoConteo> objetoConteoList = objetoConteoService.getAllObjetoConteo();
            if (objetoConteoList != null && objetoConteoList.size() > 0) {
                for (ObjetoConteo objetoConteo : objetoConteoList) {
                    try {
                        // verificar si objeto todavia existe y verificar si el objeto es usuario:
                        String bodyGetObjeto;
                        if (objetoConteo.getNombre().equals("usuario")) {
                            bodyGetObjeto = armarPeticioBuscarObjeto(objetoConteo.getNombre(), objetoConteo.getIdObjetoApi(), true);
                        } else {
                            bodyGetObjeto = armarPeticioBuscarObjeto(objetoConteo.getNombre(), objetoConteo.getIdObjetoApi(), false);
                        }

                        DataToken dataTokenApiRead = new DataToken(objetoConteo.getPrefijo(), objetoConteo.getIdPublicoUsuario(),
                                objetoConteo.getVersion(), objetoConteo.getUrlApiWrite(), objetoConteo.getUrlApiRead());
                        String tokenEncriptApiRead = generarTokenGetObjeto(dataTokenApiRead);
                        ResponseEntity<String> responseGetObjeto = integracionRepository.getObjetoApi(dataTokenApiRead, tokenEncriptApiRead, bodyGetObjeto);
                        boolean actualizo = false;
                        if (responseGetObjeto != null && responseGetObjeto.getBody() != null && responseGetObjeto.getBody().startsWith("{")
                                && responseGetObjeto.getBody().endsWith("}")) {
                            JsonNode nodoRespGetObjeto = mapper.readTree(responseGetObjeto.getBody());
                            if (nodoRespGetObjeto != null && nodoRespGetObjeto.get("data") != null && nodoRespGetObjeto.get("data").size() > 0) {
                                JsonNode objetoApi = nodoRespGetObjeto.get("data").get(0);
                                if (objetoApi != null && objetoApi.get("id") != null && !objetoApi.get("id").isNull()) {
                                    // verificar si cambió de mes:
                                    boolean reiniciarContadorMes = false;
                                    Date fecha = new Date();
                                    Date fechaDiaAnterior = getFechaDiaAnterior(fecha);
                                    if (fecha.getMonth() != fechaDiaAnterior.getMonth()) {
                                        reiniciarContadorMes = true;
                                    }

                                    if (objetoConteo.getContadores() != null && objetoConteo.getContadores().size() > 0) {
                                        for (String campoContador : objetoConteo.getContadores()) {
                                            ((ObjectNode) objetoApi).set("hora_primera_visita", null);
                                            ((ObjectNode) objetoApi).put(campoContador + "_dia", 0);
                                            if (reiniciarContadorMes) {
                                                ((ObjectNode) objetoApi).put(campoContador + "_mes", 0);
                                            }
                                        }
                                        if (objetoApi.get("id_publico") != null) {
                                            ((ObjectNode) objetoApi).remove("id_publico");
                                        }
                                        // peticion apiwrite para actualizar:
                                        String bodyUpdateObject = armarPeticionUpdateApiWrite(objetoConteo.getNombre(), objetoApi);
                                        DataToken dataToken = new DataToken(objetoConteo.getPrefijo(), objetoConteo.getIdPublicoUsuario(), objetoConteo.getVersion(),
                                                objetoConteo.getUrlApiWrite(), objetoConteo.getUrlApiRead());

                                        logger.info("--------->****insertRegisterPost en CRON: idMongo" + objetoConteo.getId() + "url: " + objetoConteo.getUrlApiWrite());
                                        ResponseEntity<String> respUpdateObject = integracionRepository.updateApi(dataToken, bodyUpdateObject, true);
                                        JsonNode nodoRespUpdate = mapper.readTree(respUpdateObject.getBody());
                                        if (nodoRespUpdate.get("statusCode") != null && !nodoRespUpdate.get("statusCode").asText().equals("200")) {
                                            logger.info("--------->****ERROR insertRegisterPost en CRON: idMongo" + objetoConteo.getId() + "body: " + bodyUpdateObject);
                                        }
                                        actualizo = true;
                                    }
                                }
                            }
                        }
                        if (!actualizo) {
                            objetoConteoService.deleteObjetoConteo(objetoConteo);
                        }
                    } catch (Exception e) {
                        logger.info("--------->****ERROR limpiando objeto en CRON: idMongo" + objetoConteo.getId() + "descripción error: " + e.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            logger.info("--------->****ERROR ejecucion CRON: ", e.getMessage());
        }
    }

    @Override
    public RespuestaVO contarActividadesSinRagonEstados(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        // consultar ultima labor y verificar si el nodo siguiente es actividad:
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
            JsonNode objetoBase = objetoRequestJson.get("objeto");
            String nombreObjeto = objetoRequestJson.get("nombreObjeto").asText();
            String ultimoNodo = objetoRequestJson.get("ultimoNodo").asText();
            String idObjeto = objetoBase.get("id").asText();

            if (ultimoNodo != null && (ultimoNodo.contains("registroactividad") || ultimoNodo.contains("oportunidad"))) {
                List<String> campos = obtenerCamposParaPeticionGetObjetoApi(nombreObjeto);
                String bodyGetObjeto = armarPeticionGetObjetoWfApi(nombreObjeto, idObjeto, "", campos, true, 1, 0, 100);
                DataToken dataTokenApiRead = new DataToken(prefijo, idPublico, versionApp, urlWrite, urlRead);
                String tokenEncriptApiRead = generarTokenGetObjeto(dataTokenApiRead);
                ResponseEntity<String> responseGetObjeto = integracionRepository.getObjetoApi(dataTokenApiRead, tokenEncriptApiRead, bodyGetObjeto);
                JsonNode nodoRespGetObjeto = mapper.readTree(responseGetObjeto.getBody());
                if (nodoRespGetObjeto.get("statusCode") != null && !nodoRespGetObjeto.get("statusCode").asText().equals("200")) {
                    errList.addError("412", "Tabla wf", "No fue posible obtner nodo finalizar labor: " + nombreObjeto);
                    throw errList;
                }
                JsonNode listaNodosWf = nodoRespGetObjeto.get("data");
                if (listaNodosWf != null && listaNodosWf.size() > 0) {
                    boolean encontrado = false;
                    JsonNode nodoAContar = null;
                    for (int i = 0; i < listaNodosWf.size() && !encontrado; i++) {
                        if (listaNodosWf.get(i).get("nodo").asText().contains("registroactividad")
                                || listaNodosWf.get(i).get("nodo").asText().contains("oportunidad")) {
                            encontrado = true;
                            nodoAContar = listaNodosWf.get(i);
                        }
                    }
                    if (nodoAContar != null) {
                        // consultar registro en mongodb para actualizar el objeto o crear un nuevo registro si no existe.
                        String ambienteObj = getAmbiente(urlRead);
                        String pkObjetoConteo = generarPkObjetoConteo(ambienteObj, prefijo, versionApp, nombreObjeto, idObjeto);
                        ObjetoConteo objetoConteo = objetoConteoService.getObjetoConteoById(pkObjetoConteo);
                        List<String> contadores = new ArrayList<>();
                        if (objetoConteo == null) {
                            objetoConteo = new ObjetoConteo();
                            objetoConteo.setId(pkObjetoConteo);
                            objetoConteo.setIdObjetoApi(idObjeto);
                            objetoConteo.setNombre(nombreObjeto);
                            objetoConteo.setPrefijo(prefijo);
                            objetoConteo.setVersion(versionApp);
                            objetoConteo.setUrlApiRead(urlRead);
                            objetoConteo.setUrlApiWrite(urlWrite);
                        } else {
                            if (objetoConteo.getContadores() != null) {
                                contadores.addAll(objetoConteo.getContadores());
                            }
                        }
                        objetoConteo.setIdPublicoUsuario(idPublico);

                        if (nodoAContar.get("nodo").asText().contains("registroactividad")) {
                            boolean contar = false;
                            JsonNode objetoTipoActividad = nodoAContar.get("registroactividad").get("tipoactividad");
                            if (objetoTipoActividad.get("contar") != null && !objetoTipoActividad.get("contar").isNull() && objetoTipoActividad.get("contar").asBoolean()) {
                                contar = objetoTipoActividad.get("contar").asBoolean();
                            }
                            if (contar) {
                                if (objetoTipoActividad.get("nombre_contador") != null && objetoTipoActividad.get("nombre_contador") != null) {
                                    if (!verificarElementoEnListado(objetoTipoActividad.get("nombre_contador").asText(), contadores)) {
                                        contadores.add(objetoTipoActividad.get("nombre_contador").asText());
                                    }
                                    String campoContadorDia = objetoTipoActividad.get("nombre_contador").asText() + "_dia";
                                    String campoContadorMes = objetoTipoActividad.get("nombre_contador").asText() + "_mes";
                                    Integer valorContadorDia = 1;
                                    Integer valorContadorMes = 1;
                                    try {
                                        if (objetoBase.get(campoContadorDia) != null && !objetoBase.get(campoContadorDia).isNull()) {
                                            valorContadorDia += objetoBase.get(campoContadorDia).asInt();
                                            valorContadorMes += objetoBase.get(campoContadorMes).asInt();
                                        }
                                    } catch (Exception e) {

                                    }

                                    JsonNode objetoConteoUpdate = mapper.createObjectNode();
                                    ((ObjectNode) objetoConteoUpdate).put("id", idObjeto);
                                    ((ObjectNode) objetoConteoUpdate).put(campoContadorDia, valorContadorDia);
                                    ((ObjectNode) objetoConteoUpdate).put(campoContadorMes, valorContadorMes);

                                    String bodyUpdateObject = armarPeticionUpdateApiWrite(nombreObjeto, objetoConteoUpdate);
                                    ResponseEntity<String> respUpdateObject = integracionRepository.updateApi(dataTokenApiRead, bodyUpdateObject, true);
                                    JsonNode nodoRespUpdate = mapper.readTree(respUpdateObject.getBody());
                                    if (nodoRespUpdate.get("statusCode") != null && !nodoRespUpdate.get("statusCode").asText().equals("200")) {
                                        errList.addError("412", "UPDATE API WRITE", "No fue posible actualizar contador de objeto: " + nombreObjeto);
                                        throw errList;
                                    }
                                }
                            }
                        } else if (nodoAContar.get("nodo").asText().contains("oportunidad")) {
                            if (!verificarElementoEnListado("oportunidades", contadores)) {
                                contadores.add("oportunidades");
                            }
                            // contar oportunidades
                            Integer valorContadorOportunidadesDia = 1;
                            Integer valorContadorOportunidadesMes = 1;
                            try {
                                if (objetoBase.get("oportunidades_dia") != null && !objetoBase.get("oportunidades_dia").isNull()) {
                                    valorContadorOportunidadesDia += objetoBase.get("oportunidades_dia").asInt();
                                    valorContadorOportunidadesMes += objetoBase.get("oportunidades_mes").asInt();
                                }
                            } catch (Exception e) {
                            }
                            JsonNode objetoConteoUpdate = mapper.createObjectNode();
                            ((ObjectNode) objetoConteoUpdate).put("id", idObjeto);
                            ((ObjectNode) objetoConteoUpdate).put("oportunidades_dia", valorContadorOportunidadesDia);
                            ((ObjectNode) objetoConteoUpdate).put("oportunidades_mes", valorContadorOportunidadesMes);

                            String bodyUpdateObject = armarPeticionUpdateApiWrite(nombreObjeto, objetoConteoUpdate);
                            ResponseEntity<String> respUpdateObject = integracionRepository.updateApi(dataTokenApiRead, bodyUpdateObject, true);
                            JsonNode nodoRespUpdate = mapper.readTree(respUpdateObject.getBody());
                            if (nodoRespUpdate.get("statusCode") != null && !nodoRespUpdate.get("statusCode").asText().equals("200")) {
                                errList.addError("412", "UPDATE API WRITE", "No fue posible actualizar contador de objeto: " + nombreObjeto);
                                throw errList;
                            }
                        }
                        objetoConteo.setContadores(contadores);
                        objetoConteoService.saveUpdateObjetoConteo(objetoConteo);
                    }
                }
            }
        } catch (MyErrorListException e) {
            if (e.getErrors() != null) {
                errList.addErrorList(e.getErrors());
            }
            if (errList.getErrors() == null || (errList.getErrors() != null && errList.getErrors().size() == 0)) {
                errList.addError("412", "Error conteo de actividades", e.getMessage());
            }
            logger.info("error al contar actividades: " + errList.getErrors().get(0).getMessage());
            throw errList;
        } catch (Exception e) {
            errList.addError("412", "Error conteo de actividades", e.getMessage());
            logger.info("Error conteo de actividades: " + e.getMessage());
        }

        if (errList.getErrors().size() > 0) {
            logger.info("error aconteo de actividades: " + errList.getErrors().get(0).getMessage());
            throw errList;
        } else {
            logger.info("****EXITO. CONTEO DE ACTIVIDADES ***");
            RespuestaVO response = new RespuestaVO();
            response.setCode("200");
            response.setMessage("success");
            return response;
        }
    }

    @Override
    public RespuestaVO contarActividadesSinRagonEstadosBeta(String objetoRequest, String prefijo, Integer versionApp, String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        // consultar ultima labor y verificar si el nodo siguiente es actividad:
        try {
            String nombreObjetoFormActividad = "registro_de_activida";
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
            JsonNode objetoBase = objetoRequestJson.get("objeto");
            String nombreObjeto = objetoRequestJson.get("nombreObjeto").asText();
            String ultimoNodo = objetoRequestJson.get("ultimoNodo").asText();
            String idObjeto = objetoBase.get("id").asText();

            if (ultimoNodo != null && (ultimoNodo.contains(nombreObjetoFormActividad) || ultimoNodo.contains("oportunidad"))) {
                List<String> campos = obtenerCamposParaPeticionGetObjetoApi(nombreObjeto);
                String bodyGetObjeto = armarPeticionGetObjetoWfApi(nombreObjeto, idObjeto, "", campos, true, 1, 0, 100);
                DataToken dataTokenApiRead = new DataToken(prefijo, idPublico, versionApp, urlWrite, urlRead);
                String tokenEncriptApiRead = generarTokenGetObjeto(dataTokenApiRead);
                ResponseEntity<String> responseGetObjeto = integracionRepository.getObjetoApi(dataTokenApiRead, tokenEncriptApiRead, bodyGetObjeto);
                JsonNode nodoRespGetObjeto = mapper.readTree(responseGetObjeto.getBody());
                if (nodoRespGetObjeto.get("statusCode") != null && !nodoRespGetObjeto.get("statusCode").asText().equals("200")) {
                    errList.addError("412", "Tabla wf", "No fue posible obtner nodo finalizar labor: " + nombreObjeto);
                    throw errList;
                }
                JsonNode listaNodosWf = nodoRespGetObjeto.get("data");
                if (listaNodosWf != null && listaNodosWf.size() > 0) {
                    boolean encontrado = false;
                    JsonNode nodoAContar = null;
                    for (int i = 0; i < listaNodosWf.size() && !encontrado; i++) {
                        if (listaNodosWf.get(i).get("nodo").asText().contains(nombreObjetoFormActividad)
                                || listaNodosWf.get(i).get("nodo").asText().contains("oportunidad")) {
                            encontrado = true;
                            nodoAContar = listaNodosWf.get(i);
                        }
                    }
                    if (nodoAContar != null) {
                        // consultar registro en mongodb para actualizar el objeto o crear un nuevo registro si no existe.
                        String ambienteObj = getAmbiente(urlRead);
                        String pkObjetoConteo = generarPkObjetoConteo(ambienteObj, prefijo, versionApp, nombreObjeto, idObjeto);
                        ObjetoConteo objetoConteo = objetoConteoService.getObjetoConteoById(pkObjetoConteo);
                        List<String> contadores = new ArrayList<>();
                        if (objetoConteo == null) {
                            objetoConteo = new ObjetoConteo();
                            objetoConteo.setId(pkObjetoConteo);
                            objetoConteo.setIdObjetoApi(idObjeto);
                            objetoConteo.setNombre(nombreObjeto);
                            objetoConteo.setPrefijo(prefijo);
                            objetoConteo.setVersion(versionApp);
                            objetoConteo.setUrlApiRead(urlRead);
                            objetoConteo.setUrlApiWrite(urlWrite);
                        } else {
                            if (objetoConteo.getContadores() != null) {
                                contadores.addAll(objetoConteo.getContadores());
                            }
                        }
                        objetoConteo.setIdPublicoUsuario(idPublico);

                        if (nodoAContar.get("nodo").asText().contains(nombreObjetoFormActividad)) {
                            boolean contar = false;
                            JsonNode objetoTipoActividad = nodoAContar.get(nombreObjetoFormActividad).get("tipoactividad");
                            if (objetoTipoActividad.get("contar") != null && !objetoTipoActividad.get("contar").isNull() && objetoTipoActividad.get("contar").asBoolean()) {
                                contar = objetoTipoActividad.get("contar").asBoolean();
                            }
                            if (contar) {
                                if (objetoTipoActividad.get("nombre_contador") != null && objetoTipoActividad.get("nombre_contador") != null) {
                                    if (!verificarElementoEnListado(objetoTipoActividad.get("nombre_contador").asText(), contadores)) {
                                        contadores.add(objetoTipoActividad.get("nombre_contador").asText());
                                    }
                                    String campoContadorDia = objetoTipoActividad.get("nombre_contador").asText() + "_dia";
                                    String campoContadorMes = objetoTipoActividad.get("nombre_contador").asText() + "_mes";
                                    Integer valorContadorDia = 1;
                                    Integer valorContadorMes = 1;
                                    try {
                                        if (objetoBase.get(campoContadorDia) != null && !objetoBase.get(campoContadorDia).isNull()) {
                                            valorContadorDia += objetoBase.get(campoContadorDia).asInt();
                                            valorContadorMes += objetoBase.get(campoContadorMes).asInt();
                                        }
                                    } catch (Exception e) {

                                    }

                                    JsonNode objetoConteoUpdate = mapper.createObjectNode();
                                    ((ObjectNode) objetoConteoUpdate).put("id", idObjeto);
                                    ((ObjectNode) objetoConteoUpdate).put(campoContadorDia, valorContadorDia);
                                    ((ObjectNode) objetoConteoUpdate).put(campoContadorMes, valorContadorMes);

                                    String bodyUpdateObject = armarPeticionUpdateApiWrite(nombreObjeto, objetoConteoUpdate);
                                    ResponseEntity<String> respUpdateObject = integracionRepository.updateApi(dataTokenApiRead, bodyUpdateObject, true);
                                    JsonNode nodoRespUpdate = mapper.readTree(respUpdateObject.getBody());
                                    if (nodoRespUpdate.get("statusCode") != null && !nodoRespUpdate.get("statusCode").asText().equals("200")) {
                                        errList.addError("412", "UPDATE API WRITE", "No fue posible actualizar contador de objeto: " + nombreObjeto);
                                        throw errList;
                                    }
                                }
                            }
                        } else if (nodoAContar.get("nodo").asText().contains("oportunidad")) {
                            if (!verificarElementoEnListado("oportunidades", contadores)) {
                                contadores.add("oportunidades");
                            }
                            // contar oportunidades
                            Integer valorContadorOportunidadesDia = 1;
                            Integer valorContadorOportunidadesMes = 1;
                            try {
                                if (objetoBase.get("oportunidades_dia") != null && !objetoBase.get("oportunidades_dia").isNull()) {
                                    valorContadorOportunidadesDia += objetoBase.get("oportunidades_dia").asInt();
                                    valorContadorOportunidadesMes += objetoBase.get("oportunidades_mes").asInt();
                                }
                            } catch (Exception e) {
                            }
                            JsonNode objetoConteoUpdate = mapper.createObjectNode();
                            ((ObjectNode) objetoConteoUpdate).put("id", idObjeto);
                            ((ObjectNode) objetoConteoUpdate).put("oportunidades_dia", valorContadorOportunidadesDia);
                            ((ObjectNode) objetoConteoUpdate).put("oportunidades_mes", valorContadorOportunidadesMes);

                            String bodyUpdateObject = armarPeticionUpdateApiWrite(nombreObjeto, objetoConteoUpdate);
                            ResponseEntity<String> respUpdateObject = integracionRepository.updateApi(dataTokenApiRead, bodyUpdateObject, true);
                            JsonNode nodoRespUpdate = mapper.readTree(respUpdateObject.getBody());
                            if (nodoRespUpdate.get("statusCode") != null && !nodoRespUpdate.get("statusCode").asText().equals("200")) {
                                errList.addError("412", "UPDATE API WRITE", "No fue posible actualizar contador de objeto: " + nombreObjeto);
                                throw errList;
                            }
                        }
                        objetoConteo.setContadores(contadores);
                        objetoConteoService.saveUpdateObjetoConteo(objetoConteo);
                    }
                }
            }
        } catch (MyErrorListException e) {
            if (e.getErrors() != null) {
                errList.addErrorList(e.getErrors());
            }
            if (errList.getErrors() == null || (errList.getErrors() != null && errList.getErrors().size() == 0)) {
                errList.addError("412", "Error conteo de actividades", e.getMessage());
            }
            logger.info("error al contar actividades: " + errList.getErrors().get(0).getMessage());
            throw errList;
        } catch (Exception e) {
            errList.addError("412", "Error conteo de actividades", e.getMessage());
            logger.info("Error conteo de actividades: " + e.getMessage());
        }

        if (errList.getErrors().size() > 0) {
            logger.info("error aconteo de actividades: " + errList.getErrors().get(0).getMessage());
            throw errList;
        } else {
            logger.info("****EXITO. CONTEO DE ACTIVIDADES ***");
            RespuestaVO response = new RespuestaVO();
            response.setCode("200");
            response.setMessage("success");
            return response;
        }
    }

}
