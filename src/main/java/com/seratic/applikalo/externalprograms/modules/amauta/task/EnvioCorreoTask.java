package com.seratic.applikalo.externalprograms.modules.amauta.task;

import com.seratic.applikalo.externalprograms.modules.amauta.service.EnvioCrreoServiceImp;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class EnvioCorreoTask {

    private static final Log LOG = LogFactory.getLog(EnvioCorreoTask.class);
    @Autowired
    private EnvioCrreoServiceImp correoService;

    //@Scheduled(cron = "${amauta.cron.enviocorreo-recordatorio}")
    public void envioCorreoAumauta(){
        correoService.envioCorreo();
    }
}
