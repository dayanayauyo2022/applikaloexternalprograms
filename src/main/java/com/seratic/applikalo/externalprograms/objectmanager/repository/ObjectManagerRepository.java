/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.objectmanager.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import com.seratic.applikalo.externalprograms.objectmanager.dto.ObjectManagerGetRequestDTO;
import com.seratic.applikalo.externalprograms.objectmanager.dto.ObjectManagerInsertRequest;
import java.util.List;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author ivang
 */
@Repository
public class ObjectManagerRepository {

    private static final String URL_GET = "funcion/getObjeto";
    private static final String URL_INSERT = "insertregisterpost";
    private static final String URL_UPDATE = "update";
    private static final String USER_AGENT = "user-agent";
    private static final String USER_AGENT_VALUE = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36";
    private static final String AUTHORIZATION = "Authorization";
    private static final String ID_PUBLIC_USER = "idpublicuser";

    public String get(String urlReader, String tokenEcript, ObjectManagerGetRequestDTO requestBody) {
        String retorno = null;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(USER_AGENT, USER_AGENT_VALUE);
        headers.add("token", tokenEcript);
        HttpEntity<ObjectManagerGetRequestDTO> requestEntity = new HttpEntity<>(requestBody, headers);
        ResponseEntity<String> responseEntity = new RestTemplate().exchange(urlReader + URL_GET, HttpMethod.POST, requestEntity, String.class);
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            retorno = responseEntity.getBody();
        }
        return retorno;
    }

    public String getGeneric(String url, DataToken dataToken, ObjectManagerGetRequestDTO requestBody) {
        String retorno = null;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(USER_AGENT, USER_AGENT_VALUE);
        headers.add(AUTHORIZATION, dataToken.getPrefijo() + "_" + dataToken.getVersionAplicacion());
        HttpEntity<ObjectManagerGetRequestDTO> requestEntity = new HttpEntity<>(requestBody, headers);
        ResponseEntity<String> responseEntity = new RestTemplate().exchange(url, HttpMethod.POST, requestEntity, String.class);
        if (responseEntity.getStatusCode() == HttpStatus.OK) {
            retorno = responseEntity.getBody();
        }
        return retorno;
    }

    public <T> List<T> insert(DataToken dataToken, ObjectManagerInsertRequest<T> body, boolean notificar, T type) throws Exception {
        List<T> retorno = null;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(USER_AGENT, USER_AGENT_VALUE);
        headers.add(AUTHORIZATION, dataToken.getPrefijo() + "_" + dataToken.getVersionAplicacion());
        headers.add(ID_PUBLIC_USER, dataToken.getIdPublico());
        headers.add("notificar", String.valueOf(notificar));
        HttpEntity<ObjectManagerInsertRequest> requestEntity = new HttpEntity<>(body, headers);
        ResponseEntity<String> response = new RestTemplate().exchange(dataToken.getUrlApiWrite() + URL_INSERT, HttpMethod.POST, requestEntity, String.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            ObjectMapper objectMapper = new ObjectMapper();
            retorno = objectMapper.readValue(response.getBody(), objectMapper.getTypeFactory().constructCollectionType(List.class, (Class) type));
        }
        return retorno;
    }

    public String update(DataToken dataToken, Object body, boolean sendUserNullNotificationMovil, boolean ignorarTipoFlujo, boolean notificar, boolean automatization) {
        String retorno = null;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(USER_AGENT, USER_AGENT_VALUE);
        headers.add(AUTHORIZATION, dataToken.getPrefijo() + "_" + dataToken.getVersionAplicacion());
        headers.add(ID_PUBLIC_USER, dataToken.getIdPublico());
        headers.add("sendUserNullNotificationMovil", String.valueOf(sendUserNullNotificationMovil));
        headers.add("ignorarTipoFlujo", String.valueOf(ignorarTipoFlujo));
        headers.add("notificar", String.valueOf(notificar));
        headers.add("automatization", String.valueOf(automatization));
        HttpEntity<Object> requestEntity = new HttpEntity<>(body, headers);
        ResponseEntity<String> response = new RestTemplate().exchange(dataToken.getUrlApiWrite() + URL_UPDATE, HttpMethod.PUT, requestEntity, String.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            retorno = response.getBody();
        }
        return retorno;
    }
}
