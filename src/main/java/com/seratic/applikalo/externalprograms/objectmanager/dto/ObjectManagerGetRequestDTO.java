/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.objectmanager.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;

/**
 *
 * @author ivang
 */
public class ObjectManagerGetRequestDTO {

    @JsonInclude(Include.NON_NULL)
    private List<OrdenDTO> orden;
    @JsonInclude(Include.NON_NULL)
    private List<FiltroDTO> filtros;
    @JsonInclude(Include.NON_NULL)
    private Integer page;
    @JsonInclude(Include.NON_NULL)
    private Integer start;
    @JsonInclude(Include.NON_NULL)
    private Integer pageSize;
    private String objeto;
    private List<CampoDTO> campos;

    public ObjectManagerGetRequestDTO() {
    }

    public ObjectManagerGetRequestDTO(String objeto, List<CampoDTO> campos) {
        this.objeto = objeto;
        this.campos = campos;
    }

    public List<OrdenDTO> getOrden() {
        return orden;
    }

    public void setOrden(List<OrdenDTO> orden) {
        this.orden = orden;
    }

    public List<FiltroDTO> getFiltros() {
        return filtros;
    }

    public void setFiltros(List<FiltroDTO> filtros) {
        this.filtros = filtros;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getObjeto() {
        return objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }

    public List<CampoDTO> getCampos() {
        return campos;
    }

    public void setCampos(List<CampoDTO> campos) {
        this.campos = campos;
    }

}
