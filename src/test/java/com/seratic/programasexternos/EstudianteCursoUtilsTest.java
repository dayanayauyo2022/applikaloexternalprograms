package com.seratic.applikalo.externalprograms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.seratic.applikalo.externalprograms.modules.amauta.utils.CrearBodyUtils;
import com.seratic.applikalo.externalprograms.modules.amauta.utils.EstudianteCursoUtils;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EstudianteCursoUtilsTest {
    
    @Test
	void contextLoads() {
	}

	@Test
	public void testCrearFiltrosEstudianteCurso() {
		Integer idEstudiante = 1;
		List<JsonNode> expectedResult = new ArrayList<>();
        expectedResult.add(CrearBodyUtils.obtenerFiltro("id", "=", "AND", "estudiante", "1"));
        expectedResult.add(CrearBodyUtils.obtenerFiltro("activo", "=", "AND", "estudiante_curso", "'true'"));
        List<JsonNode> result = EstudianteCursoUtils.crearFiltrosEstudianteCurso(idEstudiante);
		assertEquals(expectedResult,result);
	}

    @Test
	public void testCrearCamposEstudianteCurso() {
		List<JsonNode> expectedResult = new ArrayList<>();
        expectedResult.add(CrearBodyUtils.obtenerCampo("estudiante.id"));
        expectedResult.add(CrearBodyUtils.obtenerCampo("avancetotal"));
        expectedResult.add(CrearBodyUtils.obtenerCampo("activo"));
        expectedResult.add(CrearBodyUtils.obtenerCampo("periodo.id"));
        List<JsonNode> result = EstudianteCursoUtils.crearCamposEstudianteCurso();
		assertEquals(expectedResult,result);
	}
    
}
