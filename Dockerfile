FROM tomcat:9.0.31-jdk8-openjdk
COPY target/externalprograms.war /usr/local/tomcat/webapps/ROOT.war
EXPOSE 8080
CMD sh /usr/local/tomcat/bin/catalina.sh run